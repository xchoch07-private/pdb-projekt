package com.pdb.pdbprojekt.quieries.services;

import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class RegalQueryService {

    @Autowired
    private RegalQueryRepository regalQueryRepository;

    public ResponseHolder getRegals() {
        List<RegalCollection> regalCollections = regalQueryRepository.findAll();
        if (regalCollections.isEmpty()) {
            ResponseHolder<String> responseHolder = new ResponseHolder<>();
            responseHolder.setReturnCode(HttpServletResponse.SC_OK);
            responseHolder.setResponseBody("There are no regals.");
            return responseHolder;
        }

        ResponseHolder<List<RegalCollection>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(regalCollections);
        return responseHolder;
    }
}
