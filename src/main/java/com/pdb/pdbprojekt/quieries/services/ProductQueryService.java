package com.pdb.pdbprojekt.quieries.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.*;
import com.pdb.pdbprojekt.models.nosql.FinanceCollection;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.sql.jsonObjects.SalesForPeriodJackson;
import com.pdb.pdbprojekt.repositories.nosql.FinanceQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.ProductQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProductQueryService {

    @Autowired
    private RegalQueryRepository regalQueryRepository;

    @Autowired
    private ProductQueryRepository productQueryRepository;

    @Autowired
    private FinanceQueryRepository financeQueryRepository;

    @Autowired
    private GoodsQueryRepository goodsQueryRepository;

    public SalesForPeriodJackson getSalesForPeriodJackson(String json) {
        SalesForPeriodJackson salesForPeriodJackson;
        try {
            return (JsonToObjectParser.parseJsonToObject(json, SalesForPeriodJackson.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResponseHolder getSellProductInPeriod(int productId, String requestBody) {
        SalesForPeriodJackson salesForPeriodJackson = getSalesForPeriodJackson(requestBody);

        Date from = ConvertDate.convertStringToDate(salesForPeriodJackson.getFrom());
        Date to = ConvertDate.convertStringToDate(salesForPeriodJackson.getTo());

        List<FinanceCollection> financeCollectionList = financeQueryRepository.findByExposeDateBetween(from, to);

        int count = 0;
        for (FinanceCollection actualFinanceCollection : financeCollectionList) {
            List<GoodsDocument> goodsDocumentList = actualFinanceCollection.getGoods();
            for (GoodsDocument goodsDocument : goodsDocumentList) {
                GoodsCollection goodsCollection = goodsQueryRepository.findGoodsCollectionById(goodsDocument.getSerialNumber());
                if (goodsCollection.getProductReference().getId() == productId) {
                    count++;
                }
            }
        }

        ResponseHolder<Integer> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(count);
        return responseHolder;
    }


    public ResponseHolder getProducts() {

        List<ProductCollection> productCollections = productQueryRepository.findAll();
        if (productCollections.isEmpty()) {
            ResponseHolder<String> responseHolder = new ResponseHolder<>();
            responseHolder.setReturnCode(HttpServletResponse.SC_OK);
            responseHolder.setResponseBody("There are no products.");
            return responseHolder;
        }

        ResponseHolder<List<ProductCollection>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(productCollections);
        return responseHolder;
    }

    public ResponseHolder getPositionOfProductInStorage(int idProduct) {

        List<RegalCollection> regalCollectionList = regalQueryRepository.findAll();
        List<RegalCollection> regalCollectionList1 = new ArrayList<>();
        for (RegalCollection actualRegalCollection : regalCollectionList) {
            if (actualRegalCollection.getProductReference() != null) {
                if (actualRegalCollection.getProductReference().getIdProduct() == idProduct) {
                    regalCollectionList1.add(actualRegalCollection);
                }
            }
        }

        List<Integer> storageIds = new ArrayList<>();
        for (RegalCollection actualRegalCollection : regalCollectionList1) {
            if (!storageIds.contains(actualRegalCollection.getStorageReference().getId())) {
                storageIds.add(actualRegalCollection.getStorageReference().getId());
            }
        }

        List<LocationOfProductResponse> locations = new ArrayList<>();
        for (int idStorage : storageIds) {
            LocationOfProductResponse locationOfProductResponse = new LocationOfProductResponse();
            locationOfProductResponse.setIdStorage(idStorage);
            locations.add(locationOfProductResponse);
        }

        List<LocationOfProductResponse> finalLocations = new ArrayList<>();
        for (LocationOfProductResponse locationOfProductResponse : locations) {
            List<RegalResponse> regalResponses = new ArrayList<>();
            for (RegalCollection actualRegalCollection : regalCollectionList1) {
                if (actualRegalCollection.getStorageReference().getId() == locationOfProductResponse.getIdStorage()) {
                    regalResponses.add(new RegalResponse(actualRegalCollection.getId(), actualRegalCollection.getSection(),
                            actualRegalCollection.getSector(), actualRegalCollection.getPosition()));
                }
            }
            locationOfProductResponse.setRegalResponseList(regalResponses);
            finalLocations.add(locationOfProductResponse);
        }


        ResponseHolder<List<LocationOfProductResponse>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(finalLocations);
        return responseHolder;
    }
}
