package com.pdb.pdbprojekt.quieries.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.ConvertDate;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.FinanceCollection;
import com.pdb.pdbprojekt.models.sql.jsonObjects.SalesForPeriodJackson;
import com.pdb.pdbprojekt.repositories.nosql.FinanceQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

@Service
public class FinanceQueryService {

    @Autowired
    FinanceQueryRepository financeQueryRepository;

    public SalesForPeriodJackson getSalesForPeriodJackson(String json) {
        SalesForPeriodJackson salesForPeriodJackson;
        try {
            return (JsonToObjectParser.parseJsonToObject(json, SalesForPeriodJackson.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }


    public ResponseHolder getSalesForPeriod(String requestBody) {
        SalesForPeriodJackson salesForPeriodJackson = getSalesForPeriodJackson(requestBody);

        Date from = ConvertDate.convertStringToDate(salesForPeriodJackson.getFrom());
        Date to = ConvertDate.convertStringToDate(salesForPeriodJackson.getTo());

        List<FinanceCollection> financeCollectionList = financeQueryRepository.findByExposeDateBetween(from, to);

        double price = 0;
        for (FinanceCollection actualFinanceCollection : financeCollectionList) {
            price += actualFinanceCollection.getPrice();
        }

        ResponseHolder<Double> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(price);
        return responseHolder;
    }

    public ResponseHolder getFinances() {
        List<FinanceCollection> financeCollections = financeQueryRepository.findAll();
        if (financeCollections.isEmpty()) {
            ResponseHolder<String> responseHolder = new ResponseHolder<>();
            responseHolder.setReturnCode(HttpServletResponse.SC_OK);
            responseHolder.setResponseBody("There are no finances.");
            return responseHolder;
        }

        ResponseHolder<List<FinanceCollection>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(financeCollections);
        return responseHolder;
    }
}
