package com.pdb.pdbprojekt.quieries.services;

import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.StorageCollection;
import com.pdb.pdbprojekt.repositories.nosql.StorageQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class StorageQueryService {

    @Autowired
    StorageQueryRepository storageQueryRepository;

    public ResponseHolder getStorages() {

        List<StorageCollection> storageCollections = storageQueryRepository.findAll();
        if (storageCollections.isEmpty()) {
            ResponseHolder<String> responseHolder = new ResponseHolder<>();
            responseHolder.setReturnCode(HttpServletResponse.SC_OK);
            responseHolder.setResponseBody("There are no storages.");
            return responseHolder;
        }

        ResponseHolder<List<StorageCollection>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(storageCollections);
        return responseHolder;
    }
}
