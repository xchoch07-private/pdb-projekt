package com.pdb.pdbprojekt.quieries.services;

import com.pdb.pdbprojekt.common.LocationOfProductResponse;
import com.pdb.pdbprojekt.common.RegalResponse;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsQueryService {

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    GoodsQueryRepository goodsQueryRepository;

    @Autowired
    RegalQueryRepository regalQueryRepository;

    public Object getGoods() {

        List<GoodsCollection> goodsCollections = goodsQueryRepository.findAll();
        if (goodsCollections.isEmpty()) {
            ResponseHolder<String> responseHolder = new ResponseHolder<>();
            responseHolder.setReturnCode(HttpServletResponse.SC_OK);
            responseHolder.setResponseBody("There are no goods.");
            return responseHolder;
        }

        ResponseHolder<List<GoodsCollection>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(goodsCollections);
        return responseHolder;
    }


    public ResponseHolder getGoodsWithoutRequisition() {
        List<RequisitionSystemCollection> requisitionSystemCollectionList = requisitionSystemQueryRepository.findAll();
        List<RequisitionDocument> requisitionDocumentList = new ArrayList<>();
        for (RequisitionSystemCollection actualRequisitionSystemCollection : requisitionSystemCollectionList) {
            if (actualRequisitionSystemCollection.getRequisitions() != null) {
                requisitionDocumentList.addAll(actualRequisitionSystemCollection.getRequisitions());
            }
        }
        List<GoodsDocument> goodsDocumentListRequisition = new ArrayList<>();
        List<Integer> goodsDocumentListRequisitionIds = new ArrayList<>();
        for (RequisitionDocument actualRequisitionDocument : requisitionDocumentList) {
            goodsDocumentListRequisition.addAll(actualRequisitionDocument.getGoods());
        }

        for (GoodsDocument actualGoodsDocument : goodsDocumentListRequisition) {
            goodsDocumentListRequisitionIds.add(actualGoodsDocument.getSerialNumber());
        }

        List<GoodsCollection> goodsCollectionList = goodsQueryRepository.findAll();
        List<GoodsCollection> goodsWithoutRequisitionList = new ArrayList<>();

        for (GoodsCollection actualGoodsCollection : goodsCollectionList) {
            if (!goodsDocumentListRequisitionIds.contains(actualGoodsCollection.getId())) {
                goodsWithoutRequisitionList.add(actualGoodsCollection);
            }
        }

        ResponseHolder<List<GoodsCollection>> responseHolder = new ResponseHolder();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(goodsWithoutRequisitionList);
        return responseHolder;
    }

    public ResponseHolder getPositionOfGoods(int goodsID) {
        List<RegalCollection> regalCollectionList = regalQueryRepository.findAll();
        LocationOfProductResponse locationOfProductResponse = new LocationOfProductResponse();
        for (RegalCollection actualRegalCollection : regalCollectionList) {
            List<GoodsDocument> goods = actualRegalCollection.getGoods();
            if (goods != null) {
                for (GoodsDocument goodsDocument : goods) {
                    if (goodsDocument.getSerialNumber() == goodsID) {
                        if (!goodsDocument.getState().equals("in-regal")) {
                            return ResponseHolder.returnBadRequestAndMessage("Product isnt in regal.");
                        }
                        List<RegalResponse> regalResponses = new ArrayList<>();
                        RegalResponse regalResponse = new RegalResponse(actualRegalCollection.getId(), actualRegalCollection.getSection(),
                                actualRegalCollection.getSector(), actualRegalCollection.getPosition());
                        regalResponses.add(regalResponse);
                        locationOfProductResponse.setIdStorage(actualRegalCollection.getStorageReference().getId());
                        locationOfProductResponse.setRegalResponseList(regalResponses);
                    }
                }
            }
        }

        ResponseHolder<LocationOfProductResponse> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(locationOfProductResponse);
        return responseHolder;
    }

}
