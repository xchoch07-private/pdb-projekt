package com.pdb.pdbprojekt.quieries.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.models.sql.jsonObjects.RequisitionsForTruckCapacity;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Service
public class RequisitionSystemQueryService {

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    GoodsQueryRepository goodsQueryRepository;

    public ResponseHolder getRequisitions() {
        List<RequisitionSystemCollection> requisitionSystemCollections = requisitionSystemQueryRepository.findAll();
        if (requisitionSystemCollections.isEmpty()) {
            ResponseHolder<String> responseHolder = new ResponseHolder<>();
            responseHolder.setReturnCode(HttpServletResponse.SC_OK);
            responseHolder.setResponseBody("There are no requisitions.");
            return responseHolder;
        }

        ResponseHolder<List<RequisitionSystemCollection>> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(requisitionSystemCollections);
        return responseHolder;
    }


    public ResponseHolder getNonShippedRequisitions() {
        List<RequisitionSystemCollection> requisitionSystemCollectionList = requisitionSystemQueryRepository.findAll();
        List<RequisitionDocument> requisitionDocumentList = new ArrayList<>();
        for (RequisitionSystemCollection actualRequisitionSystemCollection : requisitionSystemCollectionList) {
            List<RequisitionDocument> requisitionsList = actualRequisitionSystemCollection.getRequisitions();
            for (RequisitionDocument actualRequisitionDocument : requisitionsList) {
                if (!actualRequisitionDocument.getRequisitionState().equals("dispatched")) {
                    requisitionDocumentList.add(actualRequisitionDocument);
                }
            }
        }

        ResponseHolder<List<RequisitionDocument>> responseHolder = new ResponseHolder();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(requisitionDocumentList);
        return responseHolder;
    }


    public RequisitionsForTruckCapacity getRequisitionsForTruckCapacityJackson(String json) {
        RequisitionsForTruckCapacity requisitionsForTruckCapacity;
        try {
            return (JsonToObjectParser.parseJsonToObject(json, RequisitionsForTruckCapacity.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResponseHolder getRequisitionsForTruckCapacity(String requestBody) {
        RequisitionsForTruckCapacity requisitionsForTruckCapacity = getRequisitionsForTruckCapacityJackson(requestBody);

        List<RequisitionDocument> requisitionDocumentList = requisitionSystemQueryRepository.findRequisitionSystemCollectionById(requisitionsForTruckCapacity.getSubscriberId()).getRequisitions();
        List<RequisitionDocument> requisitionDocumentListInRegal = new ArrayList<>();
        for (RequisitionDocument actualRequisitionDocuments : requisitionDocumentList) {
            if (actualRequisitionDocuments.getRequisitionState().equals("in-regal")) {
                requisitionDocumentListInRegal.add(actualRequisitionDocuments);
            }
        }

        List<RequisitionDocument> finalRequisitionDocument = new ArrayList<>();

        for (RequisitionDocument actualRequisitionDocument : requisitionDocumentListInRegal) {
            int dimension = 0;
            List<GoodsDocument> goodsDocumentList = actualRequisitionDocument.getGoods();
            for (GoodsDocument actualGoodsDocument : goodsDocumentList) {
                ProductCollection productCollection = goodsQueryRepository.findGoodsCollectionById(actualGoodsDocument.getSerialNumber()).getProductReference();
                dimension += productCollection.getDimension();
            }

            if (dimension < requisitionsForTruckCapacity.getRemainingCapacity()) {
                finalRequisitionDocument.add(actualRequisitionDocument);
            }
        }

        ResponseHolder<List<RequisitionDocument>> responseHolder = new ResponseHolder();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(finalRequisitionDocument);
        return responseHolder;

    }

}
