package com.pdb.pdbprojekt.quieries.controllers;

import com.pdb.pdbprojekt.quieries.services.RequisitionSystemQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/queries/requisition")
public class RequisitionSystemQueryController {

    @Autowired
    RequisitionSystemQueryService requisitionSystemQueryService;

    //not dispatched requisitions
    @GetMapping("/getNonShippedRequisitions")
    public Object getNonShippedRequisitions() {
        return  requisitionSystemQueryService.getNonShippedRequisitions();
    }


    @GetMapping("/getRequisitionsForTruckCapacity")
    public Object getRequisitionsForTruckCapacity(@RequestBody String requestBody) {
        return requisitionSystemQueryService.getRequisitionsForTruckCapacity(requestBody);
    }

    @GetMapping("/getRequisitions")
    public Object getRequisitions() {
        return requisitionSystemQueryService.getRequisitions();
    }
}
