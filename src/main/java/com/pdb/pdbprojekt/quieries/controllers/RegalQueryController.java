package com.pdb.pdbprojekt.quieries.controllers;

import com.pdb.pdbprojekt.quieries.services.RegalQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/queries/regal")
public class RegalQueryController {

    @Autowired
    protected RegalQueryService regalQueryService;

    @GetMapping("/getRegals")
    public Object getRegals() {
        return regalQueryService.getRegals();
    }
}
