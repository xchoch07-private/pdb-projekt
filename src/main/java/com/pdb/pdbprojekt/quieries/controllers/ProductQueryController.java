package com.pdb.pdbprojekt.quieries.controllers;

import com.pdb.pdbprojekt.quieries.services.ProductQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/queries/product")
public class ProductQueryController {

    @Autowired
    private ProductQueryService productQueryService;

    @GetMapping("/getProductPositions/{idProduct}")
    public Object getPositionOfProductInStorage(@PathVariable int idProduct) {
        return productQueryService.getPositionOfProductInStorage(idProduct);
    }

    @GetMapping("/getProducts")
    public Object getProducts() {
        return productQueryService.getProducts();
    }

    @GetMapping("getSellProductInPeriod/{productId}")
    public Object getSellProductInPeriod(@PathVariable int productId, @RequestBody String requestBody) {
        return productQueryService.getSellProductInPeriod(productId, requestBody);
    }

}
