package com.pdb.pdbprojekt.quieries.controllers;

import com.pdb.pdbprojekt.quieries.services.FinanceQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/queries/finance")
public class FinanceQueryController {

    @Autowired
    FinanceQueryService financeQueryService;

    @GetMapping("salesForPeriod")
    public Object salesForPeriod(@RequestBody String requestBody) {
        return financeQueryService.getSalesForPeriod(requestBody);
    }

    @GetMapping("/getFinances")
    public Object getFinances() {
        return financeQueryService.getFinances();
    }
}
