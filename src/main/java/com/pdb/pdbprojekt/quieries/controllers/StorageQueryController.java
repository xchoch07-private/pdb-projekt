package com.pdb.pdbprojekt.quieries.controllers;

import com.pdb.pdbprojekt.quieries.services.StorageQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/queries/storage")
public class StorageQueryController {

    @Autowired
    private StorageQueryService storageQueryService;

    @GetMapping("/getStorages")
    public Object getStorages() {
        return storageQueryService.getStorages();
    }
}
