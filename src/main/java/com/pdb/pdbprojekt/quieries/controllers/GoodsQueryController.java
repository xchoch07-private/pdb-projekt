package com.pdb.pdbprojekt.quieries.controllers;

import com.pdb.pdbprojekt.quieries.services.GoodsQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/queries/goods")
public class GoodsQueryController {

    @Autowired
    GoodsQueryService goodsQueryService;

    @GetMapping("/getGoodsWithoutRequisition")
    public Object getGoodsWithoutRequisition(){
        return goodsQueryService.getGoodsWithoutRequisition();
    }

    @GetMapping("/getPositionOfGoods/{goodsId}")
    public Object getPositionOfGoods(@PathVariable int goodsId) {
        return goodsQueryService.getPositionOfGoods(goodsId);
    }

    @GetMapping("/getGoods")
    public Object getGoods() {
        return goodsQueryService.getGoods();
    }
}
