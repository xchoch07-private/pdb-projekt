package com.pdb.pdbprojekt.models.sql.jsonObjects;

public class ProductJackson {
    private String name;
    private double price;
    private int dimension;
    private int productCode;

    public ProductJackson() {
    }

    public ProductJackson(String name, double price, int dimension, int productCode) {
        this.name = name;
        this.price = price;
        this.dimension = dimension;
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }
}
