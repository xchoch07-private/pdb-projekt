package com.pdb.pdbprojekt.models.sql;

import javax.persistence.*;

@Entity
public class Storage {

    @Id
    @SequenceGenerator(name = "StorageIdGenerator", sequenceName = "STORAGE_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "StorageIdGenerator")
    private int idStorage;

    private int capacity;
    
    private String name;

    public Storage() {

    }

    public Storage(int capacity, String name) {
        this.capacity = capacity;
        this.name = name;
    }

    public Storage(int idStorage, int capacity, String name) {
        this.idStorage = idStorage;
        this.capacity = capacity;
        this.name = name;
    }

    public int getIdStorage() {
        return idStorage;
    }

    public void setIdStorage(int idStorage) {
        this.idStorage = idStorage;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
