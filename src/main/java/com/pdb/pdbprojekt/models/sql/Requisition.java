package com.pdb.pdbprojekt.models.sql;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Requisition {

    @Id
    @SequenceGenerator(name = "RequisitionIdGenerator", sequenceName = "REQUISITION_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RequisitionIdGenerator")
    private int idRequisition;

    private String requisitionState;

    private String note;

    private Date orderCreationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idSubscriber", referencedColumnName = "idSubscriber")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Subscriber subscriberReference;

    public Requisition() {
    }

    public Requisition(String requisitionState, String note, Date orderCreationDate) {
        this.requisitionState = requisitionState;
        this.note = note;
        this.orderCreationDate = orderCreationDate;
    }

    public int getIdRequisition() {
        return idRequisition;
    }

    public void setIdRequisition(int idRequisition) {
        this.idRequisition = idRequisition;
    }

    public String getRequisitionState() {
        return requisitionState;
    }

    public void setRequisitionState(String requisitionState) {
        this.requisitionState = requisitionState;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getOrderCreationDate() {
        return orderCreationDate;
    }

    public void setOrderCreationDate(Date orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

    public Subscriber getSubscriberReference() {
        return subscriberReference;
    }

    public void setSubscriberReference(Subscriber subscriberReference) {
        this.subscriberReference = subscriberReference;
    }
}
