package com.pdb.pdbprojekt.models.sql.jsonObjects;

import java.util.List;

public class InvoiceJackson {
    private String exposeDate;
    private String maturityDate;
    private int variableSymbol;
    List<Integer> goods;

    public InvoiceJackson() {
    }

    public InvoiceJackson(String exposeDate, String maturityDate, int variableSymbol, List<Integer> goods) {
        this.exposeDate = exposeDate;
        this.maturityDate = maturityDate;
        this.variableSymbol = variableSymbol;
        this.goods = goods;
    }

    public String getExposeDate() {
        return exposeDate;
    }

    public void setExposeDate(String exposeDate) {
        this.exposeDate = exposeDate;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public int getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(int variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public List<Integer> getGoods() {
        return goods;
    }

    public void setGoods(List<Integer> goods) {
        this.goods = goods;
    }
}
