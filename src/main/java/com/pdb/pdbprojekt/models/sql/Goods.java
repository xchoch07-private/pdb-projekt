package com.pdb.pdbprojekt.models.sql;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Goods {

    @Id
    @SequenceGenerator(name = "GoodsIdGenerator", sequenceName = "GOODS_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GoodsIdGenerator")
    private int serialNumber;

    //inProgress, in-regal, loading, dispatched, invoiced
    private String state;

    private Date fabricationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idInvoice", referencedColumnName = "idInvoice")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Invoice invoiceReference;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idProduct", referencedColumnName = "idProduct")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Product productReference;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idRequisition", referencedColumnName = "idRequisition")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Requisition requisitionReference;

    public Goods() {

    }

    public Goods(String state, Date fabricationDate) {
        this.state = state;
        this.fabricationDate = fabricationDate;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getFabricationDate() {
        return fabricationDate;
    }

    public void setFabricationDate(Date fabricationDate) {
        this.fabricationDate = fabricationDate;
    }

    public Invoice getInvoiceReference() {
        return invoiceReference;
    }

    public void setInvoiceReference(Invoice invoiceReference) {
        this.invoiceReference = invoiceReference;
    }

    public Product getProductReference() {
        return productReference;
    }

    public void setProductReference(Product productReference) {
        this.productReference = productReference;
    }

    public Requisition getRequisitionReference() {
        return requisitionReference;
    }

    public void setRequisitionReference(Requisition requisitionReference) {
        this.requisitionReference = requisitionReference;
    }
}
