package com.pdb.pdbprojekt.models.sql.jsonObjects;

import java.util.List;

public class GoodsStateJackson {
    private List<Integer> goodsIds;
    private String state;

    public GoodsStateJackson() {
    }

    public GoodsStateJackson(List<Integer> goodsIds, String state) {
        this.goodsIds = goodsIds;
        this.state = state;
    }

    public List<Integer> getGoodsIds() {
        return goodsIds;
    }

    public void setGoodsIds(List<Integer> goodsIds) {
        this.goodsIds = goodsIds;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
