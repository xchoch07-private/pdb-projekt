package com.pdb.pdbprojekt.models.sql.jsonObjects;

public class TruckJackson {

    private int capacity;

    public TruckJackson() {

    }

    public TruckJackson(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
