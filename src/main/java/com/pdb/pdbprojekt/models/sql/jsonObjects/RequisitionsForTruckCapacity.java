package com.pdb.pdbprojekt.models.sql.jsonObjects;

public class RequisitionsForTruckCapacity {
    private int truckId;
    private int subscriberId;
    private int remainingCapacity;

    public RequisitionsForTruckCapacity() {
    }

    public RequisitionsForTruckCapacity(int truckId, int subscriberId, int remainingCapacity) {
        this.truckId = truckId;
        this.subscriberId = subscriberId;
        this.remainingCapacity = remainingCapacity;
    }

    public int getTruckId() {
        return truckId;
    }

    public void setTruckId(int truckId) {
        this.truckId = truckId;
    }

    public int getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(int subscriberId) {
        this.subscriberId = subscriberId;
    }

    public int getRemainingCapacity() {
        return remainingCapacity;
    }

    public void setRemainingCapacity(int remainingCapacity) {
        this.remainingCapacity = remainingCapacity;
    }
}

