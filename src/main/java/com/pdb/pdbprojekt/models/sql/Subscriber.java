package com.pdb.pdbprojekt.models.sql;

import javax.persistence.*;

@Entity
public class Subscriber {

    @Id
    @SequenceGenerator(name = "SubscriberIdGenerator", sequenceName = "SUBSCRIBER_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SubscriberIdGenerator")
    private int idSubscriber;

    //can be null
    private String name;

    //can be null
    private String firstName;

    //can be null
    private String lastName;

    private String address;

    private int ico;

    private String accountNumber;


    public Subscriber() {}

    public Subscriber(String name, String firstName, String lastName, String address, int ico, String accountNumber) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
    }

    public Subscriber(int idSubscriber, String name, String firstName, String lastName, String address, int ico, String accountNumber) {
        this.idSubscriber = idSubscriber;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
    }

    public int getIdSubscriber() {
        return idSubscriber;
    }

    public void setIdSubscriber(int idSubscriber) {
        this.idSubscriber = idSubscriber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIco() {
        return ico;
    }

    public void setIco(int ico) {
        this.ico = ico;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
