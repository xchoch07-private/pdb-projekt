package com.pdb.pdbprojekt.models.sql;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Invoice {

    @Id
    @SequenceGenerator(name = "InvoiceIdGenerator", sequenceName = "INVOICE_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "InvoiceIdGenerator")
    private int idInvoice;

    private String supplier;

    private Date exposeDate;

    private Date maturityDate;

    private int variableSymbol;

    private double price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idSubscriber", referencedColumnName = "idSubscriber")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Subscriber subscriberReference;

    public Invoice() {

    }

    public Invoice(String supplier, Date exposeDate, Date maturityDate, int variableSymbol, double price) {
        this.supplier = supplier;
        this.exposeDate = exposeDate;
        this.maturityDate = maturityDate;
        this.variableSymbol = variableSymbol;
        this.price = price;
    }

    public Invoice(String supplier, Date exposeDate, Date maturityDate, int variableSymbol, double price, Subscriber subscriberReference) {
        this.supplier = supplier;
        this.exposeDate = exposeDate;
        this.maturityDate = maturityDate;
        this.variableSymbol = variableSymbol;
        this.price = price;
        this.subscriberReference = subscriberReference;
    }

    public int getIdInvoice() {
        return idInvoice;
    }

    public void setIdInvoice(int idInvoice) {
        this.idInvoice = idInvoice;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Date getExposeDate() {
        return exposeDate;
    }

    public void setExposeDate(Date exposeDate) {
        this.exposeDate = exposeDate;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    public int getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(int variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Subscriber getSubscriberReference() {
        return subscriberReference;
    }

    public void setSubscriberReference(Subscriber subscriberReference) {
        this.subscriberReference = subscriberReference;
    }
}
