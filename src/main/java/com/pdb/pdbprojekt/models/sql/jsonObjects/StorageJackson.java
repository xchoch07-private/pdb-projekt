package com.pdb.pdbprojekt.models.sql.jsonObjects;

public class StorageJackson {

    private int capacity;

    private String name;

    public StorageJackson() {

    }

    public StorageJackson(int capacity, String name) {
        this.capacity = capacity;
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
