package com.pdb.pdbprojekt.models.sql.jsonObjects;

import java.util.Date;
import java.util.List;

public class RequisitionJackson {
    private String requisitionState;
    private String note;
    private String orderCreationDate;
    private List<GoodsForRequisitionJackson> goods;

    public RequisitionJackson() {
    }

    public RequisitionJackson(String requisitionState, String note, String orderCreationDate, List<GoodsForRequisitionJackson> goods) {
        this.requisitionState = requisitionState;
        this.note = note;
        this.orderCreationDate = orderCreationDate;
        this.goods = goods;
    }

    public String getRequisitionState() {
        return requisitionState;
    }

    public void setRequisitionState(String requisitionState) {
        this.requisitionState = requisitionState;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOrderCreationDate() {
        return orderCreationDate;
    }

    public void setOrderCreationDate(String orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

    public List<GoodsForRequisitionJackson> getGoods() {
        return goods;
    }

    public void setGoods(List<GoodsForRequisitionJackson> goods) {
        this.goods = goods;
    }
}
