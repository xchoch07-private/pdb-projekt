package com.pdb.pdbprojekt.models.sql.jsonObjects;

import java.util.List;

public class GoodsIdForRegalJackson {

    List<Integer> goodsIds;

    public GoodsIdForRegalJackson() {

    }

    public GoodsIdForRegalJackson(List<Integer> goodsIds) {
        this.goodsIds = goodsIds;
    }

    public List<Integer> getGoodsIds() {
        return goodsIds;
    }

    public void setGoodsIds(List<Integer> goodsIds) {
        this.goodsIds = goodsIds;
    }
}
