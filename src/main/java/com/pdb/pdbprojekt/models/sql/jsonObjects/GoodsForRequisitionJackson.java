package com.pdb.pdbprojekt.models.sql.jsonObjects;

public class GoodsForRequisitionJackson {
    private int idProduct;
    private int amount;

    public GoodsForRequisitionJackson() {
    }

    public GoodsForRequisitionJackson(int idProduct, int amount) {
        this.idProduct = idProduct;
        this.amount = amount;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
