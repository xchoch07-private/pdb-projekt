package com.pdb.pdbprojekt.models.sql.jsonObjects;

public class ProductIdJackson {

    private int product;

    public ProductIdJackson() {

    }

    public ProductIdJackson(int product) {
        this.product = product;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }
}
