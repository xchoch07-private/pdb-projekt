package com.pdb.pdbprojekt.models.sql;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Regal {

    @Id
    @SequenceGenerator(name = "RegalIdGenerator", sequenceName = "REGAL_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RegalIdGenerator")
    private int idRegal;

    private int section;

    private int sector;

    private int position;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idProduct", referencedColumnName = "idProduct")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Product productReference;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idStorage", referencedColumnName = "idStorage")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Storage storageReference;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinTable(name = "Goods_Regal", joinColumns = @JoinColumn(name = "idRegal"), inverseJoinColumns = @JoinColumn(name = "serialNumber"))
    private List<Goods> goods;

    public Regal() {

    }

    public Regal(int section, int sector, int position) {
        this.section = section;
        this.sector = sector;
        this.position = position;
    }

    public Regal(int section, int sector, int position, Product productReference, Storage storageReference, List<Goods> goods) {
        this.section = section;
        this.sector = sector;
        this.position = position;
        this.productReference = productReference;
        this.storageReference = storageReference;
        this.goods = goods;
    }

    public int getIdRegal() {
        return idRegal;
    }

    public void setIdRegal(int idRegal) {
        this.idRegal = idRegal;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Product getProductReference() {
        return productReference;
    }

    public void setProductReference(Product productReference) {
        this.productReference = productReference;
    }

    public Storage getStorageReference() {
        return storageReference;
    }

    public void setStorageReference(Storage storageReference) {
        this.storageReference = storageReference;
    }

    public List<Goods> getGoods() {
        return goods;
    }

    public void setGoods(List<Goods> goods) {
        this.goods = goods;
    }
}
