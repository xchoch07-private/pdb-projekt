package com.pdb.pdbprojekt.models.sql;

import javax.persistence.*;
import java.util.List;

@Entity
public class Truck {

    @Id
    @SequenceGenerator(name = "TruckIdGenerator", sequenceName = "TRUCK_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TruckIdGenerator")
    private int idTruck;

    //can be null
    private int capacity;

    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    @JoinTable(name = "Requisition_Truck", joinColumns = @JoinColumn(name = "idTruck"), inverseJoinColumns = @JoinColumn(name = "idRequisition"))
    private List<Requisition> requisitions;

    public Truck() {

    }

    public Truck(int capacity) {
        this.capacity = capacity;
    }

    public int getIdTruck() {
        return idTruck;
    }

    public void setIdTruck(int idTruck) {
        this.idTruck = idTruck;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<Requisition> getRequisitions() {
        return requisitions;
    }

    public void setRequisitions(List<Requisition> requisitions) {
        this.requisitions = requisitions;
    }
}
