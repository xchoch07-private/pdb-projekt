package com.pdb.pdbprojekt.models.sql.jsonObjects;

public class RegalJackson {

    private int section;

    private int sector;

    private int position;

    public RegalJackson() {

    }

    public RegalJackson(int section, int sector, int position) {
        this.section = section;
        this.sector = sector;
        this.position = position;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
