package com.pdb.pdbprojekt.models.sql.jsonObjects;

import java.util.List;

public class RequisitionsToTruckJackson {

    List<Integer> requisitionsIds;

    public RequisitionsToTruckJackson() {

    }

    public RequisitionsToTruckJackson(List<Integer> requisitionsIds) {
        this.requisitionsIds = requisitionsIds;
    }

    public List<Integer> getRequisitionsIds() {
        return requisitionsIds;
    }

    public void setRequisitionsIds(List<Integer> requisitionsIds) {
        this.requisitionsIds = requisitionsIds;
    }
}
