package com.pdb.pdbprojekt.models.nosql;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.persistence.Transient;

@Document
@Data
public class ProductCollection {

    @Transient
    public static final String SEQUENCE_NAME = "product_sequence";

    @Id
    private int id;

    private String name;

    private double price;

    private int dimension;

    private int productCode;

    Integer version;

    public ProductCollection() {
    }

    public ProductCollection(String name, double price, int dimension, int productCode) {
        this.name = name;
        this.price = price;
        this.dimension = dimension;
        this.productCode = productCode;
    }

    public ProductCollection(int id, String name, double price, int dimension, int productCode) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.dimension = dimension;
        this.productCode = productCode;
    }

    public ProductCollection(int id, String name, double price, int dimension, int productCode, Integer version) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.dimension = dimension;
        this.productCode = productCode;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
