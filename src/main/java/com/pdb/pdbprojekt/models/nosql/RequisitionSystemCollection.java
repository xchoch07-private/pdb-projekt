package com.pdb.pdbprojekt.models.nosql;

import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Document
@Data
public class RequisitionSystemCollection {

    @Transient
    public static final String SEQUENCE_NAME = "requisition_sequence";

    @Id
    private int id;

    private String name;

    private String firstName;

    private String lastName;

    private String address;

    private int ico;

    private String accountNumber;

    private List<RequisitionDocument> requisitions;

    Integer version;

    public RequisitionSystemCollection() {
    }

    public RequisitionSystemCollection(String name, String firstName, String lastName, String address, int ico, String accountNumber) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
    }

    public RequisitionSystemCollection(int id, String name, String firstName, String lastName, String address, int ico, String accountNumber) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
    }

    public RequisitionSystemCollection(int id, String name, String firstName, String lastName, String address, int ico, String accountNumber, List<RequisitionDocument> requisitions) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
        this.requisitions = requisitions;
    }

    public RequisitionSystemCollection(int id, String name, String firstName, String lastName, String address, int ico, String accountNumber, Integer version) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIco() {
        return ico;
    }

    public void setIco(int ico) {
        this.ico = ico;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<RequisitionDocument> getRequisitions() {
        return requisitions;
    }

    public void setRequisitions(List<RequisitionDocument> requisitions) {
        this.requisitions = requisitions;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
