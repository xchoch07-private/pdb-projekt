package com.pdb.pdbprojekt.models.nosql;

import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.SubscriberDocument;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Document
@Data
public class FinanceCollection {

    @Transient
    public static final String SEQUENCE_NAME = "finance_sequence";

    @Id
    private int id;

    private String supplier;

    private Date exposeDate;

    private Date maturityDate;

    private int variableSymbol;

    private double price;

    private List<GoodsDocument> goods;

    private SubscriberDocument subscriberDocument;

    Integer version;

    public FinanceCollection() {
    }

    public FinanceCollection(String supplier, Date exposeDate, Date maturityDate, int variableSymbol, double price) {
        this.supplier = supplier;
        this.exposeDate = exposeDate;
        this.maturityDate = maturityDate;
        this.variableSymbol = variableSymbol;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Date getExposeDate() {
        return exposeDate;
    }

    public void setExposeDate(Date exposeDate) {
        this.exposeDate = exposeDate;
    }

    public Date getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    public int getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(int variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<GoodsDocument> getGoods() {
        return goods;
    }

    public void setGoods(List<GoodsDocument> goods) {
        this.goods = goods;
    }

    public SubscriberDocument getSubscriberDocument() {
        return subscriberDocument;
    }

    public void setSubscriberDocument(SubscriberDocument subscriberDocument) {
        this.subscriberDocument = subscriberDocument;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
