package com.pdb.pdbprojekt.models.nosql.sequences;

import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.SequenceQueryRepository;
import org.hibernate.sql.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class SequenceGeneratorService {

    private final MongoOperations mongoOperations;

    @Autowired
    SequenceQueryRepository sequenceQueryRepository;

    @Autowired
    public SequenceGeneratorService(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public long generateSequence(String seqName) {
        CollectionSequences counter = mongoOperations.findAndModify(query(where("_id").is(seqName)),
                new Update().inc("seq",1), options().returnNew(true).upsert(true),
                CollectionSequences.class);
        return !Objects.isNull(counter) ? counter.getSeq() : 1;
    }

//    public void deleteSequence(String seqName){
//        CollectionSequences counter = mongoOperations.findAndModify(query(where("_id").is(seqName)),
//                new Update().set("seq", 0), options().returnNew(true).upsert(true),
//                CollectionSequences.class);
//
//    }


}
