package com.pdb.pdbprojekt.models.nosql.documents;

import lombok.Data;

import javax.persistence.Id;
import java.util.Date;


@Data
public class GoodsDocument {

    @Id
    private int serialNumber;

    private String state;

    private Date fabricationDate;

    public GoodsDocument() {
    }

    public GoodsDocument(String state, Date dateOfManufacture) {
        this.state = state;
        this.fabricationDate = dateOfManufacture;
    }

    public GoodsDocument(int serialNumber, String state, Date fabricationDate) {
        this.serialNumber = serialNumber;
        this.state = state;
        this.fabricationDate = fabricationDate;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getFabricationDate() {
        return fabricationDate;
    }

    public void setFabricationDate(Date fabricationDate) {
        this.fabricationDate = fabricationDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
