package com.pdb.pdbprojekt.models.nosql.documents;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Id;

@Data
public class SubscriberDocument {

    @Id
    private int idSubscriber;

    private String name;

    private String firstName;

    private String lastName;

    private String address;

    private int ico;

    private String accountNumber;

    public SubscriberDocument() {
    }

    public SubscriberDocument(String name, String firstName, String lastName, String address, int ico, String accountNumber) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
    }

    public SubscriberDocument(int idSubscriber, String name, String firstName, String lastName, String address, int ico, String accountNumber) {
        this.idSubscriber = idSubscriber;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ico = ico;
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIco() {
        return ico;
    }

    public void setIco(int ico) {
        this.ico = ico;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
