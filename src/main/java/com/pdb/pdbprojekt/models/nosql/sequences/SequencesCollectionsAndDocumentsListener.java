package com.pdb.pdbprojekt.models.nosql.sequences;

import com.pdb.pdbprojekt.models.nosql.*;
import com.pdb.pdbprojekt.models.nosql.documents.TruckDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

////////////////////////////////////////////////////
/////               Collections                /////
////////////////////////////////////////////////////


@Component
class RequisitionSystemCollectionListener extends AbstractMongoEventListener<RequisitionSystemCollection> {

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    RequisitionSystemCollectionListener(SequenceGeneratorService sequenceGeneratorService) {
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<RequisitionSystemCollection> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId((int) sequenceGeneratorService.generateSequence(RequisitionSystemCollection.SEQUENCE_NAME));
        }
    }
}


@Component
class FinanceCollectionListener extends AbstractMongoEventListener<FinanceCollection> {

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    FinanceCollectionListener(SequenceGeneratorService sequenceGeneratorService) {
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<FinanceCollection> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId((int) sequenceGeneratorService.generateSequence(FinanceCollection.SEQUENCE_NAME));
        }
    }
}


@Component
class GoodsCollectionListener extends AbstractMongoEventListener<GoodsCollection> {

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public GoodsCollectionListener(SequenceGeneratorService sequenceGeneratorService) {
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<GoodsCollection> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId((int) sequenceGeneratorService.generateSequence(GoodsCollection.SEQUENCE_NAME));
        }
    }
}


@Component
class ProductCollectionListener extends AbstractMongoEventListener<ProductCollection> {

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public ProductCollectionListener(SequenceGeneratorService sequenceGeneratorService) {
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<ProductCollection> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId((int) sequenceGeneratorService.generateSequence(ProductCollection.SEQUENCE_NAME));
        }
    }
}


@Component
class RegalCollectionListener extends AbstractMongoEventListener<RegalCollection> {

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public RegalCollectionListener(SequenceGeneratorService sequenceGeneratorService) {
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<RegalCollection> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId((int) sequenceGeneratorService.generateSequence(RegalCollection.SEQUENCE_NAME));
        }
    }
}


@Component
class StorageCollectionListener extends AbstractMongoEventListener<StorageCollection> {

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public StorageCollectionListener(SequenceGeneratorService sequenceGeneratorService) {
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<StorageCollection> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId((int) sequenceGeneratorService.generateSequence(StorageCollection.SEQUENCE_NAME));
        }
    }
}


////////////////////////////////////////////////////
/////                Documents                 /////
////////////////////////////////////////////////////

@Component
class TruckDocumentListener extends AbstractMongoEventListener<TruckDocument> {

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public TruckDocumentListener(SequenceGeneratorService sequenceGeneratorService) {
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public void onBeforeConvert(BeforeConvertEvent<TruckDocument> event) {
        if (event.getSource().getId() < 1) {
            event.getSource().setId((int) sequenceGeneratorService.generateSequence(TruckDocument.SEQUENCE_NAME));
        }
    }
}
