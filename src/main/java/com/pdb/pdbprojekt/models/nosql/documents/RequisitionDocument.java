package com.pdb.pdbprojekt.models.nosql.documents;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class RequisitionDocument {

    @Id
    private int id;

    private String requisitionState;

    private String note;

    private Date orderCreationDate;

    private List<GoodsDocument> goods;

    private List<TruckDocument> trucks;

    public RequisitionDocument() {
    }

    public RequisitionDocument(String requisitionState, String note, Date orderCreationDate) {
        this.requisitionState = requisitionState;
        this.note = note;
        this.orderCreationDate = orderCreationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequisitionState() {
        return requisitionState;
    }

    public void setRequisitionState(String requisitionState) {
        this.requisitionState = requisitionState;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getOrderCreationDate() {
        return orderCreationDate;
    }

    public void setOrderCreationDate(Date orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

    public List<GoodsDocument> getGoods() {
        return goods;
    }

    public void setGoods(List<GoodsDocument> goods) {
        this.goods = goods;
    }

    public List<TruckDocument> getTrucks() {
        return trucks;
    }

    public void setTrucks(List<TruckDocument> trucks) {
        this.trucks = trucks;
    }
}
