package com.pdb.pdbprojekt.models.nosql.documents;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Id;

@Data
public class ProductDocument {

    @Id
    private int idProduct;

    private String name;

    private double price;

    private int dimension;

    private int productCode;

    public ProductDocument() {
    }

    public ProductDocument(String name, double price, int dimension, int productCode) {
        this.name = name;
        this.price = price;
        this.dimension = dimension;
        this.productCode = productCode;
    }

    public ProductDocument(int idProduct, String name, double price, int dimension, int productCode) {
        this.idProduct = idProduct;
        this.name = name;
        this.price = price;
        this.dimension = dimension;
        this.productCode = productCode;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }
}
