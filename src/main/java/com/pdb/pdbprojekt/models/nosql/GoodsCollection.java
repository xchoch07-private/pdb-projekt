package com.pdb.pdbprojekt.models.nosql;

import lombok.Data;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

@Document
@Data
public class GoodsCollection {

    @Transient
    public static final String SEQUENCE_NAME = "goods_sequence";

    @Id
    private int id;

    private String state;

    private Date fabricationDate;

    @DBRef
    private ProductCollection productReference;

    Integer version;

    public GoodsCollection() {
    }

    public GoodsCollection(String state, Date fabricationDate) {
        this.state = state;
        this.fabricationDate = fabricationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFabricationDate() {
        return fabricationDate;
    }

    public void setFabricationDate(Date fabricationDate) {
        this.fabricationDate = fabricationDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ProductCollection getProductReference() {
        return productReference;
    }

    public void setProductReference(ProductCollection productReference) {
        this.productReference = productReference;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
