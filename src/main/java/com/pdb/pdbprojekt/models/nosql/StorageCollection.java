package com.pdb.pdbprojekt.models.nosql;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.persistence.Transient;

@Document
@Data
public class StorageCollection {

    @Transient
    public static final String SEQUENCE_NAME = "storage_sequence";

    @Id
    private int id;

    private int size;

    private String name;

    Integer version;

    public StorageCollection() {
    }

    public StorageCollection(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public StorageCollection(int id, int size, String name) {
        this.id = id;
        this.size = size;
        this.name = name;
    }

    public StorageCollection(int id, int size, String name, Integer version) {
        this.id = id;
        this.size = size;
        this.name = name;
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
