package com.pdb.pdbprojekt.models.nosql.documents;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Transient;

@Data
public class TruckDocument {

    @Transient
    public static final String SEQUENCE_NAME = "truck_sequence";

    @Id
    private int id;

    private int capacity;

    public TruckDocument() {

    }

    public TruckDocument(int id, int capacity) {
        this.id = id;
        this.capacity = capacity;
    }

    public TruckDocument(int capacity) {
        this.capacity = capacity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
