package com.pdb.pdbprojekt.models.nosql;

import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.ProductDocument;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

@Document
@Data
public class RegalCollection {

    @Transient
    public static final String SEQUENCE_NAME = "regal_sequence";

    @Id
    private int id;

    private int section;

    private int sector;

    private int position;

    Integer version;

    @DBRef
    private StorageCollection storageReference;

    private ProductDocument productReference;

    private List<GoodsDocument> goods;

    public RegalCollection() {
    }

    public RegalCollection(int section, int sector, int position) {
        this.section = section;
        this.sector = sector;
        this.position = position;
    }

    public RegalCollection(int section, int sector, int position, ProductDocument productReference, List<GoodsDocument> goods) {
        this.section = section;
        this.sector = sector;
        this.position = position;
        this.productReference = productReference;
        this.goods = goods;
    }

    public RegalCollection(int id, int section, int sector, int position) {
        this.id = id;
        this.section = section;
        this.sector = sector;
        this.position = position;
    }

    public RegalCollection(int id, int section, int sector, int position, StorageCollection storageReference, ProductDocument productReference, List<GoodsDocument> goods) {
        this.id = id;
        this.section = section;
        this.sector = sector;
        this.position = position;
        this.storageReference = storageReference;
        this.productReference = productReference;
        this.goods = goods;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public StorageCollection getStorageReference() {
        return storageReference;
    }

    public void setStorageReference(StorageCollection storageReference) {
        this.storageReference = storageReference;
    }

    public ProductDocument getProductReference() {
        return productReference;
    }

    public void setProductReference(ProductDocument productReference) {
        this.productReference = productReference;
    }

    public List<GoodsDocument> getGoods() {
        return goods;
    }

    public void setGoods(List<GoodsDocument> goods) {
        this.goods = goods;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
