package com.pdb.pdbprojekt.common;

public class RegalResponse {

    int idRegal;

    int section;

    int sector;

    int position;

    public RegalResponse() {

    }

    public RegalResponse(int idRegal, int section, int sector, int position) {
        this.idRegal = idRegal;
        this.section = section;
        this.sector = sector;
        this.position = position;
    }

    public int getIdRegal() {
        return idRegal;
    }

    public void setIdRegal(int idRegal) {
        this.idRegal = idRegal;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public int getSector() {
        return sector;
    }

    public void setSector(int sector) {
        this.sector = sector;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
