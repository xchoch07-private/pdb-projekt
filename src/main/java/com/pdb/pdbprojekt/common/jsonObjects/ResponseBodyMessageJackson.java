package com.pdb.pdbprojekt.common.jsonObjects;

import java.util.Set;

public class ResponseBodyMessageJackson {
    private String message;

    public ResponseBodyMessageJackson() {
    }

    public ResponseBodyMessageJackson(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
