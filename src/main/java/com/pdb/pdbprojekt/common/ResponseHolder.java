package com.pdb.pdbprojekt.common;

import com.pdb.pdbprojekt.common.jsonObjects.EmptyJsonResponse;
import com.pdb.pdbprojekt.common.jsonObjects.ResponseBodyMessageJackson;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

public class ResponseHolder<T> {
    private int returnCode;
    private T responseBody;

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public void setResponseBody(T responseBody) {
        this.responseBody = responseBody;
    }

    public T getResponseBody() {
        return responseBody;
    }

    public T setup(HttpServletResponse response) {
        response.setStatus(returnCode);
        return responseBody;
    }

    public static ResponseHolder returnEmptyBody(int code){
        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setResponseBody(new EmptyJsonResponse());
        responseHolder.setReturnCode(code);
        return responseHolder;
    }

    public static ResponseHolder returnEmptyArray(int code){
        ResponseHolder responseHolder = new ResponseHolder();
        responseHolder.setReturnCode(code);
        ArrayList<Object> emptyArray = new ArrayList<>();
        responseHolder.setResponseBody(emptyArray);
        return responseHolder;
    }

    public static ResponseHolder returnOkAndMessage(String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_OK);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }

    public static ResponseHolder returnBadRequestAndMessage(String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_BAD_REQUEST);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }

    public static ResponseHolder returnReturnForbiddenAndMessage(String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(HttpServletResponse.SC_FORBIDDEN);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }

    public static ResponseHolder returnReturnCodeAndMessage(int returnCode, String message) {
        ResponseHolder<ResponseBodyMessageJackson> responseHolder = new ResponseHolder<>();
        responseHolder.setReturnCode(returnCode);
        responseHolder.setResponseBody(new ResponseBodyMessageJackson(message));
        return responseHolder;
    }
}
