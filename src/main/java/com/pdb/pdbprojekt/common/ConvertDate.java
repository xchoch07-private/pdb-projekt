package com.pdb.pdbprojekt.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ConvertDate {

    public static Date convertStringToDate(String dateString) {
        DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        try {
            return (format.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
