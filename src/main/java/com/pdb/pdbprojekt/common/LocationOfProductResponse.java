package com.pdb.pdbprojekt.common;

import java.util.List;

public class LocationOfProductResponse {

    int idStorage;

    List<RegalResponse> regalResponseList;

    public LocationOfProductResponse() {

    }

    public LocationOfProductResponse(int idStorage, List<RegalResponse> regalResponseList) {
        this.idStorage = idStorage;
        this.regalResponseList = regalResponseList;
    }

    public int getIdStorage() {
        return idStorage;
    }

    public void setIdStorage(int idStorage) {
        this.idStorage = idStorage;
    }

    public List<RegalResponse> getRegalResponseList() {
        return regalResponseList;
    }

    public void setRegalResponseList(List<RegalResponse> regalResponseList) {
        this.regalResponseList = regalResponseList;
    }
}
