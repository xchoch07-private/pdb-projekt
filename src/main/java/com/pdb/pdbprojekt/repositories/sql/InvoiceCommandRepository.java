package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceCommandRepository extends JpaRepository<Invoice, Integer> {
}
