package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.models.sql.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsCommandRepository extends JpaRepository<Goods, Integer> {

   Goods findGoodsBySerialNumber(int id);


   @Query("SELECT u FROM Goods u JOIN Requisition r JOIN Subscriber WHERE u.serialNumber = ?1")
   Goods findGoodsBySerialNumberSql(int id);

   @Query("SELECT u FROM Goods u WHERE u.requisitionReference = null AND u.productReference = ?1")
   List<Goods> findGoodsWithNoneRequisition(Product productId);



}
