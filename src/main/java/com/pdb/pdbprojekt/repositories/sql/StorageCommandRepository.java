package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Storage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageCommandRepository extends JpaRepository<Storage, Integer> {

    Storage findStorageByIdStorage(int id);
}
