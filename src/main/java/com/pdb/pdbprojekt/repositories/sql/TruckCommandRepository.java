package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Truck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TruckCommandRepository extends JpaRepository<Truck, Integer> {

    Truck findTruckByIdTruck(int id);
}
