package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Requisition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequisitionCommandRepository extends JpaRepository<Requisition, Integer> {

    Requisition findRequisitionByIdRequisition(int id);
}
