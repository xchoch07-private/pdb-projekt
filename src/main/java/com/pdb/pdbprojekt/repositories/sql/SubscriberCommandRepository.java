package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriberCommandRepository extends JpaRepository<Subscriber, Integer> {

   Subscriber findSubscriberByIdSubscriber(int id);
}
