package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Regal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RegalCommandRepository extends JpaRepository<Regal, Integer> {

    Regal findRegalByIdRegal(int id);

}
