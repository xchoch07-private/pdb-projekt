package com.pdb.pdbprojekt.repositories.sql;

import com.pdb.pdbprojekt.models.sql.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductCommandRepository extends JpaRepository<Product, Integer> {

    Product findByIdProduct(int id);

}
