package com.pdb.pdbprojekt.repositories.nosql;

import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RequisitionSystemQueryRepository extends MongoRepository<RequisitionSystemCollection, Integer> {

    RequisitionSystemCollection findById(int id);

    RequisitionSystemCollection findRequisitionSystemCollectionById(int id);
}
