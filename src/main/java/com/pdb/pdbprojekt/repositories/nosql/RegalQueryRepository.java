package com.pdb.pdbprojekt.repositories.nosql;

import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegalQueryRepository extends MongoRepository<RegalCollection, Integer> {

    RegalCollection findRegalCollectionById(int id);


}
