package com.pdb.pdbprojekt.repositories.nosql;

import com.pdb.pdbprojekt.models.nosql.FinanceCollection;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface FinanceQueryRepository extends MongoRepository<FinanceCollection, Integer> {

    FinanceCollection findFinanceCollectionsById(int id);


//    @Query("$and : [{exposeDate : {$gt:?}}, {exposeDate : {$lt:?}}]")
//    List<FinanceCollection> getFinanceInPeriod(String first, String second);


    List<FinanceCollection> findByExposeDateBetween(Date first, Date second);

//    @Query("$id: ?1")
//    List<FinanceCollection> getFinanceInPeriod(int id);
}
