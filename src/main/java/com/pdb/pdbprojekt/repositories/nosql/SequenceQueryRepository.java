package com.pdb.pdbprojekt.repositories.nosql;

import com.pdb.pdbprojekt.models.nosql.sequences.CollectionSequences;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface SequenceQueryRepository extends MongoRepository<CollectionSequences, String> {

}
