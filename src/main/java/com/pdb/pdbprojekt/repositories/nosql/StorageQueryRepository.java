package com.pdb.pdbprojekt.repositories.nosql;

import com.pdb.pdbprojekt.models.nosql.StorageCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StorageQueryRepository extends MongoRepository<StorageCollection, Integer> {

    StorageCollection findStorageCollectionById(int id);
}
