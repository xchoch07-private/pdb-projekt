package com.pdb.pdbprojekt.repositories.nosql;

import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoodsQueryRepository extends MongoRepository<GoodsCollection, Integer> {

    GoodsCollection findGoodsCollectionById(int id);
}
