package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.InvoiceCommandService;
import com.pdb.pdbprojekt.models.nosql.FinanceCollection;

public class FinanceCreatedEventNoSql {

    private final InvoiceCommandService service;

    public FinanceCreatedEventNoSql(InvoiceCommandService service) {
        this.service = service;
    }

    public FinanceCollection apply(FinanceCollection financeCollection) {
        return service.saveFinanceNoSql(financeCollection);
    }


}
