package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.RequisitionCommandService;
import com.pdb.pdbprojekt.models.sql.Requisition;

public class RequisitionRemovedEventSql {
    private final RequisitionCommandService service;

    public RequisitionRemovedEventSql(RequisitionCommandService service) {
        this.service = service;
    }

    public void apply(Requisition requisition) {
        service.deleteRequisitionSql(requisition);
    }
}
