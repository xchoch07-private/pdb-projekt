package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.GoodsCommandService;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;

public class GoodsCreatedEventNoSql {

    private final GoodsCommandService service;

    public GoodsCreatedEventNoSql(GoodsCommandService service) {
        this.service = service;
    }


    public GoodsCollection apply(GoodsCollection goodsCollection) {
        return service.saveGoodsCollectionNoSql(goodsCollection);
    }
}
