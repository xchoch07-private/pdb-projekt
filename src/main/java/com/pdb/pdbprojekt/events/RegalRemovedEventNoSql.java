package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.RegalCommandService;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;

public class RegalRemovedEventNoSql {

    private final RegalCommandService regalCommandService;

    public RegalRemovedEventNoSql(RegalCommandService regalCommandService) {
        this.regalCommandService = regalCommandService;
    }

    public void apply(RegalCollection regalCollection) {
        regalCommandService.removeRegalNoSql(regalCollection);
    }
}
