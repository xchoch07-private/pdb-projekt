package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.SubscriberCommandService;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;

public class SubscriberCreatedEventNoSql {

    private final SubscriberCommandService service;

    public SubscriberCreatedEventNoSql(SubscriberCommandService service) {
        this.service = service;
    }

    public RequisitionSystemCollection apply(RequisitionSystemCollection requisitionSystemCollection) {
        return service.saveSubscriberNoSql(requisitionSystemCollection);
    }
}
