package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.SubscriberCommandService;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;

public class SubscriberRemovedEventNoSql {

    private final SubscriberCommandService service;

    public SubscriberRemovedEventNoSql(SubscriberCommandService service) {
        this.service = service;
    }

    public void apply(RequisitionSystemCollection requisitionSystemCollection) {
        service.removeSubscriberNoSql(requisitionSystemCollection);
    }
}
