package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.StorageCommandService;
import com.pdb.pdbprojekt.models.nosql.StorageCollection;

public class StorageRemovedEventNoSql {

    private final StorageCommandService storageCommandService;

    public StorageRemovedEventNoSql(StorageCommandService storageCommandService) {
        this.storageCommandService = storageCommandService;
    }

    public void apply(StorageCollection storageCollection) {
        storageCommandService.removeStorageNoSql(storageCollection);
    }
}
