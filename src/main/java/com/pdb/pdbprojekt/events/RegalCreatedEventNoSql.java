package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.RegalCommandService;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;

public class RegalCreatedEventNoSql {

    protected final RegalCommandService regalCommandService;

    public RegalCreatedEventNoSql(RegalCommandService regalCommandService) {
        this.regalCommandService = regalCommandService;
    }

    public RegalCollection apply(RegalCollection regalCollection) {
        return regalCommandService.saveRegalNoSql(regalCollection);
    }
}
