package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.RegalCommandService;
import com.pdb.pdbprojekt.models.sql.Regal;

public class RegalCreatedEventSql {

    private final RegalCommandService regalCommandService;

    public RegalCreatedEventSql(RegalCommandService regalCommandService) {
        this.regalCommandService = regalCommandService;
    }

    public Regal apply(Regal regal) {
        return regalCommandService.saveRegal(regal);
    }
}
