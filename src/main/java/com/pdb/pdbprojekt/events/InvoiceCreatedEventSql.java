package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.InvoiceCommandService;
import com.pdb.pdbprojekt.models.sql.Invoice;

public class InvoiceCreatedEventSql {

    private final InvoiceCommandService service;

    public InvoiceCreatedEventSql(InvoiceCommandService service) {
        this.service = service;
    }

    public Invoice apply(Invoice invoice) {
        return service.saveInvoice(invoice);
    }
}
