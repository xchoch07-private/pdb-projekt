package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.StorageCommandService;
import com.pdb.pdbprojekt.models.sql.Storage;

public class StorageCreatedEventSql {

    private final StorageCommandService storageCommandService;

    public StorageCreatedEventSql(StorageCommandService storageCommandService) {
        this.storageCommandService = storageCommandService;
    }

    public Storage apply(Storage storage) {
        return storageCommandService.saveStorage(storage);
    }
}
