package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.RegalCommandService;
import com.pdb.pdbprojekt.models.sql.Regal;

public class RegalRemovedEventSql {

    private final RegalCommandService regalCommandService;

    public RegalRemovedEventSql(RegalCommandService regalCommandService) {
        this.regalCommandService = regalCommandService;
    }

    public void apply(Regal regal) {
        regalCommandService.removeRegal(regal);
    }
}
