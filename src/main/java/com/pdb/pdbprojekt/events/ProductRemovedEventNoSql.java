package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.ProductCommandService;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;

public class ProductRemovedEventNoSql {

    private final ProductCommandService productCommandService;

    public ProductRemovedEventNoSql(ProductCommandService productCommandService) {
        this.productCommandService = productCommandService;
    }

    public void apply(ProductCollection productCollection) {
        productCommandService.removeProductNoSql(productCollection);
    }
}
