package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.ProductCommandService;
import com.pdb.pdbprojekt.models.sql.Product;

public class ProductRemovedEventSql {

    private final ProductCommandService productCommandService;

    public ProductRemovedEventSql(ProductCommandService productCommandService) {
        this.productCommandService = productCommandService;
    }

    public void apply(Product product) {
        productCommandService.removeProduct(product);
    }
}
