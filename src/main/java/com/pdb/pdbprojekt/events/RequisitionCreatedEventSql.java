package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.RequisitionCommandService;
import com.pdb.pdbprojekt.models.sql.Requisition;

public class RequisitionCreatedEventSql {
    private final RequisitionCommandService service;

    public RequisitionCreatedEventSql(RequisitionCommandService service) {
        this.service = service;
    }

    public Requisition apply(Requisition requisition) {
        return service.saveRequisitionSql(requisition);
    }
}
