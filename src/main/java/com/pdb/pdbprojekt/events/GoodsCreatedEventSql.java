package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.GoodsCommandService;
import com.pdb.pdbprojekt.models.sql.Goods;

public class GoodsCreatedEventSql {

    private final GoodsCommandService service;

    public GoodsCreatedEventSql(GoodsCommandService service) {
        this.service = service;
    }

    public Goods apply(Goods goods) {
        return service.saveGoods(goods);
    }
}
