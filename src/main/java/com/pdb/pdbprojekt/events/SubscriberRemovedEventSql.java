package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.SubscriberCommandService;
import com.pdb.pdbprojekt.models.sql.Subscriber;

public class SubscriberRemovedEventSql {
    private final SubscriberCommandService service;

    public SubscriberRemovedEventSql(SubscriberCommandService service) {
        this.service = service;
    }

    public void apply(Subscriber subscriber) {
        service.removeSubscriber(subscriber);
    }

}
