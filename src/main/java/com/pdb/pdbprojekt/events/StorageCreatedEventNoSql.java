package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.StorageCommandService;
import com.pdb.pdbprojekt.models.nosql.StorageCollection;

public class StorageCreatedEventNoSql {

    private final StorageCommandService storageCommandService;

    public StorageCreatedEventNoSql(StorageCommandService storageCommandService) {
        this.storageCommandService = storageCommandService;
    }

    public StorageCollection apply(StorageCollection storageCollection) {
        return storageCommandService.saveStorageNoSql(storageCollection);
    }
}
