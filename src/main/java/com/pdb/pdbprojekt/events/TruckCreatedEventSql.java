package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.TruckCommandService;
import com.pdb.pdbprojekt.models.sql.Truck;

public class TruckCreatedEventSql {

    private final TruckCommandService truckCommandService;

    public TruckCreatedEventSql(TruckCommandService truckCommandService) {
        this.truckCommandService = truckCommandService;
    }

    public Truck apply(Truck truck) {
        return truckCommandService.saveTruck(truck);
    }
}
