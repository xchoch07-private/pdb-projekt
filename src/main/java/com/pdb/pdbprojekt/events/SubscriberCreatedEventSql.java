package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.models.sql.Subscriber;
import com.pdb.pdbprojekt.commands.services.SubscriberCommandService;

public class SubscriberCreatedEventSql {

    private final SubscriberCommandService service;

    public SubscriberCreatedEventSql(SubscriberCommandService service) {
        this.service = service;
    }

    public Subscriber apply(Subscriber subscriber) {
        return service.saveSubscriber(subscriber);
    }

}
