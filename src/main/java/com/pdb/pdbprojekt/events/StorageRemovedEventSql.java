package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.StorageCommandService;
import com.pdb.pdbprojekt.models.sql.Storage;

public class StorageRemovedEventSql {

    private final StorageCommandService storageCommandService;

    public StorageRemovedEventSql(StorageCommandService storageCommandService) {
        this.storageCommandService = storageCommandService;
    }

    public void apply(Storage storage) {
        storageCommandService.removeStorage(storage);
    }
}
