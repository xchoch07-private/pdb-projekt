package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.ProductCommandService;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;

public class ProductCreatedEventNoSql {
    private final ProductCommandService service;

    public ProductCreatedEventNoSql(ProductCommandService service) {
        this.service = service;
    }

    public ProductCollection apply(ProductCollection productCollection) {
        return service.saveProductNoSql(productCollection);
    }

}
