package com.pdb.pdbprojekt.events;

import com.pdb.pdbprojekt.commands.services.ProductCommandService;
import com.pdb.pdbprojekt.models.sql.Product;

public class ProductCreatedEventSql {
    private final ProductCommandService service;

    public ProductCreatedEventSql(ProductCommandService service) {
        this.service = service;
    }

    public Product apply(Product product) {
        return service.saveProductSql(product);
    }

}
