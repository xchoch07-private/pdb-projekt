package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.TruckCreatedEventSql;
import com.pdb.pdbprojekt.models.sql.Truck;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class TruckOracleSourceOfTruthSubscriber extends AbstractSubscriber {

    Truck truck;

    public TruckOracleSourceOfTruthSubscriber(Truck truck, EventBus... eventBuses) {
        super(eventBuses);
        this.truck = truck;
    }

    @Subscribe
    public Truck onConfirmedTruckCreatedEvent(TruckCreatedEventSql confirmedTruckCreatedEvent) {
        return confirmedTruckCreatedEvent.apply(truck);
    }
}
