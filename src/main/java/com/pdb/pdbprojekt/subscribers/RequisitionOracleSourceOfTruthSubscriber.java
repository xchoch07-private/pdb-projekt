package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.RequisitionCreatedEventSql;
import com.pdb.pdbprojekt.events.RequisitionRemovedEventSql;
import com.pdb.pdbprojekt.models.sql.Requisition;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class RequisitionOracleSourceOfTruthSubscriber extends AbstractSubscriber{


    Requisition requisition;

    public RequisitionOracleSourceOfTruthSubscriber(Requisition requisition, EventBus... eventBuses) {
        super(eventBuses);
        this.requisition = requisition;
    }

    @Subscribe
    public Requisition onConformedRequisitionCreatedEvent(RequisitionCreatedEventSql confirmedRequistionCreatedEvent) {
        return confirmedRequistionCreatedEvent.apply(requisition);
    }

    @Subscribe
    public void onConformedRequisitionRemovedEvent(RequisitionRemovedEventSql confirmedRequisitionRemovedEvent){
        confirmedRequisitionRemovedEvent.apply(requisition);
    }


}
