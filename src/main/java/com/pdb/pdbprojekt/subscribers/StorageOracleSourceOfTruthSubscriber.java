package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.StorageCreatedEventSql;
import com.pdb.pdbprojekt.events.StorageRemovedEventSql;
import com.pdb.pdbprojekt.models.sql.Storage;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class StorageOracleSourceOfTruthSubscriber extends AbstractSubscriber {

    Storage storage;

    public StorageOracleSourceOfTruthSubscriber(Storage storage, EventBus... eventBuses) {
        super(eventBuses);
        this.storage = storage;
    }

    @Subscribe
    public Storage onConfirmedStorageCreatedEvent(StorageCreatedEventSql confirmedStorageCreatedEvent) {
        return confirmedStorageCreatedEvent.apply(storage);
    }

    @Subscribe
    public void onConformedStorageRemovedEvent(StorageRemovedEventSql confirmedStorageRemovedEvent) {
        confirmedStorageRemovedEvent.apply(storage);
    }
}
