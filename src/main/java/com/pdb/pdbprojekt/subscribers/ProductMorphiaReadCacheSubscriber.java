package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.ProductCreatedEventNoSql;
import com.pdb.pdbprojekt.events.ProductRemovedEventNoSql;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ProductMorphiaReadCacheSubscriber extends AbstractSubscriber {

    ProductCollection productCollection;

    public ProductMorphiaReadCacheSubscriber(ProductCollection productCollection, EventBus... eventBuses) {
        super(eventBuses);
        this.productCollection = productCollection;
    }

    @Subscribe
    public ProductCollection onConfirmedProductCollectionCreatedEvent(ProductCreatedEventNoSql confirmedProductCollectionCreatedEvent) {
        return confirmedProductCollectionCreatedEvent.apply(productCollection);
    }

    @Subscribe
    public void onConfirmedProductCollectionRemovedEvent(ProductRemovedEventNoSql confirmedProductCollectionRemovedEvent) {
        confirmedProductCollectionRemovedEvent.apply(productCollection);
    }
}
