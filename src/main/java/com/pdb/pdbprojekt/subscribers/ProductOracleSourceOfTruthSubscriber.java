package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.ProductCreatedEventSql;
import com.pdb.pdbprojekt.events.ProductRemovedEventSql;
import com.pdb.pdbprojekt.models.sql.Product;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ProductOracleSourceOfTruthSubscriber extends AbstractSubscriber {
    Product product;

    public ProductOracleSourceOfTruthSubscriber(Product product, EventBus... eventBuses) {
        super(eventBuses);
        this.product = product;
    }

    @Subscribe
    public Product onConfirmedProductCreatedEvent(ProductCreatedEventSql confirmedProductCreatedEvent) {
        return confirmedProductCreatedEvent.apply(product);
    }

    @Subscribe
    public void onConfirmedProductRemovedEvent(ProductRemovedEventSql confirmedProductRemovedEvent) {
        confirmedProductRemovedEvent.apply(product);
    }
}
