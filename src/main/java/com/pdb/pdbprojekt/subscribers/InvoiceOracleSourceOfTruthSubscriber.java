package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.InvoiceCreatedEventSql;
import com.pdb.pdbprojekt.models.sql.Invoice;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class InvoiceOracleSourceOfTruthSubscriber extends AbstractSubscriber {

    Invoice invoice;

    public InvoiceOracleSourceOfTruthSubscriber(Invoice invoice, EventBus... eventBuses) {
        super(eventBuses);
        this.invoice = invoice;
    }

    @Subscribe
    public Invoice onConfirmedInvoiceCreatedEvent(InvoiceCreatedEventSql confirmedInvoiceCreatedEvent) {
        return confirmedInvoiceCreatedEvent.apply(invoice);
    }

}
