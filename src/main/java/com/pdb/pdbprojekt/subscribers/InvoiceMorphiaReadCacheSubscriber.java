package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.FinanceCreatedEventNoSql;
import com.pdb.pdbprojekt.models.nosql.FinanceCollection;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class InvoiceMorphiaReadCacheSubscriber extends AbstractSubscriber {

    FinanceCollection financeCollection;

    public InvoiceMorphiaReadCacheSubscriber(FinanceCollection financeCollection, EventBus... eventBuses) {
        super(eventBuses);
        this.financeCollection = financeCollection;
    }

    @Subscribe
    public FinanceCollection onConfirmedFinanceSystemCollectionCreatedEvent(FinanceCreatedEventNoSql confirmedFinanceSystemCollectionCreatedEvent) {
        return confirmedFinanceSystemCollectionCreatedEvent.apply(financeCollection);
    }
}
