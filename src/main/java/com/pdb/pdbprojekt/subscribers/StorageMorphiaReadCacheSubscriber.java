package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.StorageCreatedEventNoSql;
import com.pdb.pdbprojekt.events.StorageRemovedEventNoSql;
import com.pdb.pdbprojekt.models.nosql.StorageCollection;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class StorageMorphiaReadCacheSubscriber extends AbstractSubscriber {

    StorageCollection storageCollection;

    public StorageMorphiaReadCacheSubscriber(StorageCollection storageCollection, EventBus... eventBuses) {
        super(eventBuses);
        this.storageCollection = storageCollection;
    }

    @Subscribe
    public StorageCollection onConfirmedStorageCollectionCreatedEvent(StorageCreatedEventNoSql confirmedStorageCollectionCreatedEvent) {
        return confirmedStorageCollectionCreatedEvent.apply(storageCollection);
    }

    @Subscribe
    public void onConfirmedStorageCollectionRemovedEvent(StorageRemovedEventNoSql confirmedStorageCollectionRemovedEvent) {
        confirmedStorageCollectionRemovedEvent.apply(storageCollection);
    }
}
