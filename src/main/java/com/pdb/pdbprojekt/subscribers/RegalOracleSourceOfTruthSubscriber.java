package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.RegalCreatedEventSql;
import com.pdb.pdbprojekt.events.RegalRemovedEventSql;
import com.pdb.pdbprojekt.models.sql.Regal;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class RegalOracleSourceOfTruthSubscriber extends AbstractSubscriber {

    Regal regal;

    public RegalOracleSourceOfTruthSubscriber(Regal regal, EventBus... eventBuses) {
        super(eventBuses);
        this.regal = regal;
    }

    @Subscribe
    public Regal onConfirmedRegalCreatedEvent(RegalCreatedEventSql confirmedRegalCreatedEvent) {
        return confirmedRegalCreatedEvent.apply(regal);
    }

    @Subscribe
    public void onConfirmedRegalRemovedEvent(RegalRemovedEventSql confirmedRegalRemovedEvent) {
        confirmedRegalRemovedEvent.apply(regal);
    }
}
