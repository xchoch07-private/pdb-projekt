package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.GoodsCreatedEventSql;
import com.pdb.pdbprojekt.models.sql.Goods;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class GoodsOracleSourceOfTruthSubscriber extends AbstractSubscriber {
    Goods goods;

    public GoodsOracleSourceOfTruthSubscriber(Goods goods, EventBus... eventBuses) {
        super(eventBuses);
        this.goods = goods;
    }

    @Subscribe
    public Goods onConformedGoodsCreatedEvent(GoodsCreatedEventSql confirmedGoodsCreatedEvent) {
        return confirmedGoodsCreatedEvent.apply(goods);
    }
}
