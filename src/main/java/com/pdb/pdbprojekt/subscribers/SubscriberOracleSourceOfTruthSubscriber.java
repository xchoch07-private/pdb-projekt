package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.SubscriberRemovedEventSql;
import com.pdb.pdbprojekt.models.sql.Subscriber;
import com.pdb.pdbprojekt.events.SubscriberCreatedEventSql;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class SubscriberOracleSourceOfTruthSubscriber extends AbstractSubscriber {

    Subscriber subscriber;

    public SubscriberOracleSourceOfTruthSubscriber(Subscriber subscriber, EventBus... eventBuses) {
        super(eventBuses);
        this.subscriber = subscriber;
    }

    @Subscribe
    public Subscriber onConfirmedSubscriberCreatedEvent(SubscriberCreatedEventSql confirmedSubscriberCreatedEvent) {
        return confirmedSubscriberCreatedEvent.apply(subscriber);
    }

    @Subscribe
    public void onConfirmedSubscriberRemovedEvent(SubscriberRemovedEventSql confirmedSubscriberRemovedEvent) {
        confirmedSubscriberRemovedEvent.apply(subscriber);
    }
}
