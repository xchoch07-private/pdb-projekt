package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.RegalCreatedEventNoSql;
import com.pdb.pdbprojekt.events.RegalRemovedEventNoSql;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class RegalMorphiaReadCacheSubscriber extends AbstractSubscriber {

    RegalCollection regalCollection;

    public RegalMorphiaReadCacheSubscriber(RegalCollection regalCollection, EventBus... eventBuses) {
        super(eventBuses);
        this.regalCollection = regalCollection;
    }

    @Subscribe
    public RegalCollection onConfirmedRegalCollectionCreatedEvent(RegalCreatedEventNoSql confirmedRegalCollectionCreatedEvent) {
        return confirmedRegalCollectionCreatedEvent.apply(regalCollection);
    }

    @Subscribe
    public void onConfirmedRegalCollectionRemovedEvent(RegalRemovedEventNoSql confirmedRegalCollectionRemovedEvent) {
        confirmedRegalCollectionRemovedEvent.apply(regalCollection);
    }
}
