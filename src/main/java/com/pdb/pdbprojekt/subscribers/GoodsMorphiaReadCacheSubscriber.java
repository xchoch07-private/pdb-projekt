package com.pdb.pdbprojekt.subscribers;

import com.pdb.pdbprojekt.events.GoodsCreatedEventNoSql;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class GoodsMorphiaReadCacheSubscriber extends AbstractSubscriber{
    GoodsCollection goodsCollection;
    GoodsDocument goodsDocument;

    public GoodsMorphiaReadCacheSubscriber(GoodsCollection goodsCollection, EventBus... eventBuses) {
        super(eventBuses);
        this.goodsCollection = goodsCollection;
    }

    @Subscribe
    public GoodsCollection onConformedGoodsCollectionCreatedEvent(GoodsCreatedEventNoSql confirmedGoodsCollectionCreatedEvent) {
        return confirmedGoodsCollectionCreatedEvent.apply(goodsCollection);
    }


}
