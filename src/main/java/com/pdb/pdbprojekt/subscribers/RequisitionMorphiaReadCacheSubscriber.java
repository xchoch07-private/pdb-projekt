package com.pdb.pdbprojekt.subscribers;


import com.pdb.pdbprojekt.events.SubscriberCreatedEventNoSql;
import com.pdb.pdbprojekt.events.SubscriberRemovedEventNoSql;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class RequisitionMorphiaReadCacheSubscriber extends AbstractSubscriber {

    RequisitionSystemCollection requisitionSystemCollection;

    public RequisitionMorphiaReadCacheSubscriber(RequisitionSystemCollection requisitionSystemCollection, EventBus... eventBuses) {
        super(eventBuses);
        this.requisitionSystemCollection = requisitionSystemCollection;
    }

    @Subscribe
    public RequisitionSystemCollection onConfirmedRequisitionSystemCollectionCreatedEvent(SubscriberCreatedEventNoSql confirmedRequisitionSystemCollectionCreatedEvent) {
        return confirmedRequisitionSystemCollectionCreatedEvent.apply(requisitionSystemCollection);
    }

    @Subscribe
    public void onConfirmedRequisitionSystemCollectionRemovedEvent(SubscriberRemovedEventNoSql confirmedRequisitionSystemCollectionRemovedEvent) {
        confirmedRequisitionSystemCollectionRemovedEvent.apply(requisitionSystemCollection);
    }
}
