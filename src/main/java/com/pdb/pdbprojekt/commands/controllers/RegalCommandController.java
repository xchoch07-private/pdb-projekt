package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.RegalCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/commands/regal")
public class RegalCommandController {

    @Autowired
    private RegalCommandService regalCommandService;

    @PostMapping("/add/{idStorage}")
    public Object addRegal(@PathVariable int idStorage, @RequestBody String requestBody) {
        return regalCommandService.addNewRegal(idStorage, requestBody);
    }

    @PostMapping("/add/product/{idRegal}")
    public Object addProductToRegal(@PathVariable int idRegal, @RequestBody String requestBody) {
        return regalCommandService.addNewProductsToRegal(idRegal, requestBody);
    }

    @PostMapping("/add/goods/{idRegal}")
    public Object addGoodsToRegal(@PathVariable int idRegal, @RequestBody String requestBody) {
        return regalCommandService.addNewGoodsToRegal(idRegal, requestBody);
    }

    @PutMapping("/update/{idRegal}")
    public Object updateRegal(@PathVariable int idRegal, @RequestBody String requestBody) {
        return regalCommandService.updateRegal(idRegal, requestBody);
    }

    @DeleteMapping("/remove/{id}")
    public Object removeRegal(@PathVariable int id) {
        return regalCommandService.deleteRegal(id);
    }
}
