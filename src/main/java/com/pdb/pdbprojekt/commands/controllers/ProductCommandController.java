package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.ProductCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commands/product")
public class ProductCommandController {

    @Autowired
    ProductCommandService productCommandService;

    @PostMapping("/add")
    public Object addProduct(@RequestBody String requestBody) {
        return productCommandService.addProduct(requestBody);
    }

    @PutMapping("/update/{idProduct}")
    public Object updateStorage(@PathVariable int idProduct, @RequestBody String requestBody) {
        return productCommandService.updateProduct(idProduct, requestBody);
    }

    @DeleteMapping("/remove/{id}")
    public Object removeProduct(@PathVariable int id) {
        return productCommandService.deleteProduct(id);
    }
}
