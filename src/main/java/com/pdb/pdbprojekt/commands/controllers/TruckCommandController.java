package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.TruckCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commands/truck")
public class TruckCommandController {

    @Autowired
    private TruckCommandService truckCommandService;

    @PostMapping("/add")
    public Object addTruck(@RequestBody String requestBody) {
        return truckCommandService.addNewTruck(requestBody);
    }

    @PostMapping("/addRequisition/{idTruck}")
    public Object addRequisitionToTruck(@PathVariable int idTruck, @RequestBody String requestBody) {
        return truckCommandService.addRequisitionToTruck(idTruck, requestBody);
    }

    @PutMapping("/update/{idTruck}")
    public Object updateTruck(@PathVariable int idTruck, @RequestBody String requestBody) {
        return truckCommandService.updateTruck(idTruck, requestBody);
    }

}
