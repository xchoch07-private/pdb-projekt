package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.GoodsCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commands/goods")
public class GoodsCommandController {

    @Autowired
    private GoodsCommandService goodsCommandService;

    @PutMapping("/changeState")
    public Object changeStateForGoods(@RequestBody String requestBody) {
        return goodsCommandService.changeStateForGoods(requestBody);
    }

}
