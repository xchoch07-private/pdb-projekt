package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.StorageCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commands/storage")
public class StorageCommandController {

    @Autowired
    private StorageCommandService storageCommandService;

    @PostMapping("/add")
    public Object addStorage(@RequestBody String requestBody) {
        return storageCommandService.addNewStorage(requestBody);
    }

    @PutMapping("/update/{idStorage}")
    public Object updateStorage(@PathVariable int idStorage, @RequestBody String requestBody) {
        return storageCommandService.updateStorage(idStorage, requestBody);
    }

    @DeleteMapping("/remove/{id}")
    public Object removeStorage(@PathVariable int id) {
        return storageCommandService.deleteStorage(id);
    }
}
