package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.SubscriberCommandService;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.SubscriberCommandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commands/subscriber")
public class SubscriberCommandController {

    @Autowired
    private SubscriberCommandService subscriberCommandService;

    @Autowired
    private RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    private SubscriberCommandRepository subscriberCommandRepository;

    @PostMapping("/add")
    public Object addSubscriber(@RequestBody String requestBody) {
        return subscriberCommandService.addNewSubscriber(requestBody);
    }

    @PutMapping("/update/{idSubscriber}")
    public Object updateSubscriber(@PathVariable int idSubscriber, @RequestBody String requestBody) {
        return subscriberCommandService.updateSubscriber(idSubscriber, requestBody);
    }

    @DeleteMapping("/remove/{id}")
    public Object removeSubscriber(@PathVariable int id) {
        return subscriberCommandService.deleteSubscriber(id);
    }

}
