package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.RequisitionCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commands/requisition")
public class RequisitionCommandController {

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @PostMapping("/add/{idSubscriber}")
    public Object addRequisition(@PathVariable int idSubscriber, @RequestBody String requestBody) {
        return requisitionCommandService.addRequisition(idSubscriber, requestBody);
    }

    @DeleteMapping("/remove/{requisitionId}")
    public Object removeRequisition(@PathVariable int requisitionId) {
        return requisitionCommandService.removeRequisition(requisitionId);
    }
}
