package com.pdb.pdbprojekt.commands.controllers;

import com.pdb.pdbprojekt.commands.services.InvoiceCommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/commands/invoice")
public class InvoiceCommandController {

    @Autowired
    private InvoiceCommandService invoiceCommandService;

    @PostMapping("/add/{id}")
    public Object addInvoice(@PathVariable int id, @RequestBody String requestBody) {
        return invoiceCommandService.addNewInvoice(id, requestBody);
    }
}
