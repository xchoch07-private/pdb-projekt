package com.pdb.pdbprojekt.commands.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.StorageCreatedEventNoSql;
import com.pdb.pdbprojekt.events.StorageCreatedEventSql;
import com.pdb.pdbprojekt.events.StorageRemovedEventNoSql;
import com.pdb.pdbprojekt.events.StorageRemovedEventSql;
import com.pdb.pdbprojekt.models.nosql.StorageCollection;
import com.pdb.pdbprojekt.models.sql.Regal;
import com.pdb.pdbprojekt.models.sql.Storage;
import com.pdb.pdbprojekt.models.sql.jsonObjects.StorageJackson;
import com.pdb.pdbprojekt.repositories.nosql.StorageQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.RegalCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.StorageCommandRepository;
import com.pdb.pdbprojekt.subscribers.StorageMorphiaReadCacheSubscriber;
import com.pdb.pdbprojekt.subscribers.StorageOracleSourceOfTruthSubscriber;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StorageCommandService {

    @Autowired
    private StorageCommandRepository storageCommandRepository;

    @Autowired
    private StorageQueryRepository storageQueryRepository;

    @Autowired
    private RegalCommandRepository regalCommandRepository;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();

    //////////////////////////////////////////////////////
    // CREATE NEW Storage
    //////////////////////////////////////////////////////
    public Storage createStorage(String json) {
        StorageJackson storageJackson;
        try {
            storageJackson = JsonToObjectParser.parseJsonToObject(json, StorageJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return new Storage(storageJackson.getCapacity(), storageJackson.getName());
    }

    public StorageCollection createStorageCollection(String json) {
        StorageJackson storageJackson;
        try {
            storageJackson = JsonToObjectParser.parseJsonToObject(json, StorageJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return new StorageCollection(storageJackson.getCapacity(), storageJackson.getName());
    }

    public ResponseHolder addNewStorage(String requestBody) {

        Storage storage = createStorage(requestBody);
        StorageCollection storageCollection = createStorageCollection(requestBody);

        sendStorageToDB(storage, storageCollection);

        return ResponseHolder.returnOkAndMessage("The storage has been successfully created.");
    }

    //////////////////////////////////////////////////////
    // UPDATE STORAGE
    //////////////////////////////////////////////////////
    public ResponseHolder updateStorage(int idStorage, String json) {

        Storage tmp = storageCommandRepository.findStorageByIdStorage(idStorage);
        if (tmp == null) {
            return ResponseHolder.returnBadRequestAndMessage("This storage isn't in system.");
        }

        Storage storage = createStorage(json);
        storage.setIdStorage(idStorage);

        StorageCollection storageCollection = createStorageCollection(json);
        storageCollection.setId(idStorage);

        sendStorageToDB(storage, storageCollection);

        return ResponseHolder.returnOkAndMessage("Storage has been updated successfully.");
    }

    //////////////////////////////////////////////////////
    // DELETE STORAGE
    //////////////////////////////////////////////////////
    public ResponseHolder deleteStorage(int id) {

        Storage storage = storageCommandRepository.findStorageByIdStorage(id);
        StorageCollection storageCollection = storageQueryRepository.findStorageCollectionById(id);
        List<Regal> regals = regalCommandRepository.findAll();

        for (Regal regal : regals) {
            if (regal.getStorageReference().getIdStorage() == id) {
                return ResponseHolder.returnBadRequestAndMessage("Cannot delete storage with regals.");
            }
        }

        StorageOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new StorageOracleSourceOfTruthSubscriber(storage, eventBusSql);
        StorageMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new StorageMorphiaReadCacheSubscriber(storageCollection, eventBusNoSql);

        eventBusSql.post(new StorageRemovedEventSql(this));
        eventBusSql.unregister(sourceOfTruthSubscriber);
        eventBusNoSql.post(new StorageRemovedEventNoSql(this));
        eventBusNoSql.unregister(morphiaReadCacheSubscriber);

        return ResponseHolder.returnOkAndMessage("Storage has been successfully deleted.");
    }


    //////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////
    public Storage saveStorage(Storage storage) {
        return storageCommandRepository.save(storage);
    }

    public StorageCollection saveStorageNoSql(StorageCollection storageCollection) {
        if (storageQueryRepository.findStorageCollectionById(storageCollection.getId()) != null) {
            if (storageQueryRepository.findStorageCollectionById(storageCollection.getId()).getVersion() == null) {
                storageCollection.setVersion(2);
            } else {
                storageCollection.setVersion(storageQueryRepository.findStorageCollectionById(storageCollection.getId()).getVersion() + 1);
            }
        }
        return storageQueryRepository.save(storageCollection);
    }

    public void removeStorage(Storage storage) {
        storageCommandRepository.delete(storage);
    }

    public void removeStorageNoSql(StorageCollection storageCollection) {
        storageQueryRepository.delete(storageCollection);
    }

    public Storage getStorage(int id) {
        return storageCommandRepository.findStorageByIdStorage(id);
    }

    public StorageCollection getStorageCollectionNoSql(int id) {
        return storageQueryRepository.findStorageCollectionById(id);
    }

    //////////////////////////////////////////////////////
    // HELPFUL METHODS
    //////////////////////////////////////////////////////
    private void sendStorageToDB(Storage storage, StorageCollection storageCollection) {
        StorageOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new StorageOracleSourceOfTruthSubscriber(storage, eventBusSql);
        StorageMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new StorageMorphiaReadCacheSubscriber(storageCollection, eventBusNoSql);

        final StorageCreatedEventSql createdEventSql = new StorageCreatedEventSql(this);
        eventBusSql.post(createdEventSql);
        eventBusSql.unregister(sourceOfTruthSubscriber);
        final StorageCreatedEventNoSql createdEventNoSql = new StorageCreatedEventNoSql(this);
        eventBusNoSql.post(createdEventNoSql);
        eventBusNoSql.unregister(morphiaReadCacheSubscriber);
    }
}
