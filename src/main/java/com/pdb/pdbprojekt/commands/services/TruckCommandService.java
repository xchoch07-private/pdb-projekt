package com.pdb.pdbprojekt.commands.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.SubscriberCreatedEventNoSql;
import com.pdb.pdbprojekt.events.TruckCreatedEventSql;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.models.nosql.documents.TruckDocument;
import com.pdb.pdbprojekt.models.sql.Requisition;
import com.pdb.pdbprojekt.models.sql.Truck;
import com.pdb.pdbprojekt.models.sql.jsonObjects.RequisitionsToTruckJackson;
import com.pdb.pdbprojekt.models.sql.jsonObjects.TruckJackson;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.RequisitionCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.TruckCommandRepository;
import com.pdb.pdbprojekt.subscribers.RequisitionMorphiaReadCacheSubscriber;
import com.pdb.pdbprojekt.subscribers.TruckOracleSourceOfTruthSubscriber;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TruckCommandService {

    @Autowired
    private TruckCommandRepository truckCommandRepository;

    @Autowired
    private RequisitionCommandRepository requisitionCommandRepository;

    @Autowired
    private RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    private SubscriberCommandService subscriberCommandService;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();

    //////////////////////////////////////////////////////
    // CREATE NEW Truck
    //////////////////////////////////////////////////////
    public Truck createTruck(String json) {
        TruckJackson truckJackson;
        try {
            truckJackson = JsonToObjectParser.parseJsonToObject(json, TruckJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return new Truck(truckJackson.getCapacity());
    }

    public ResponseHolder addNewTruck(String requestBody) {
        Truck truck = createTruck(requestBody);

        TruckOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new TruckOracleSourceOfTruthSubscriber(truck, eventBusSql);

        final TruckCreatedEventSql createdEventSql = new TruckCreatedEventSql(this);
        eventBusSql.post(createdEventSql);
        eventBusSql.unregister(sourceOfTruthSubscriber);

        return ResponseHolder.returnOkAndMessage("The truck has been successfully created.");
    }

    //////////////////////////////////////////////////////
    // ADD Requisition to Truck
    //////////////////////////////////////////////////////
    public RequisitionsToTruckJackson createRequisitionsToTruckIds(String json) {
        RequisitionsToTruckJackson requisitionsToTruckJackson;
        try {
            requisitionsToTruckJackson = JsonToObjectParser.parseJsonToObject(json, RequisitionsToTruckJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        return requisitionsToTruckJackson;
    }

    public ResponseHolder addRequisitionToTruck(int idTruck, String json) {
        RequisitionsToTruckJackson requisitionsToTruckJackson = createRequisitionsToTruckIds(json);
        List<Integer> requisitionIds = requisitionsToTruckJackson.getRequisitionsIds();

        Truck truck = truckCommandRepository.findTruckByIdTruck(idTruck);
        if (truck == null) {
            return ResponseHolder.returnBadRequestAndMessage("Truck does not exists. ");
        }

        ArrayList<Requisition> requisitions = new ArrayList<>();
        for (int id : requisitionIds) {
            requisitions.add(requisitionCommandRepository.findRequisitionByIdRequisition(id));
        }

        truck.setRequisitions(requisitions);
        TruckOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new TruckOracleSourceOfTruthSubscriber(truck, eventBusSql);
        eventBusSql.post(new TruckCreatedEventSql(this));
        eventBusSql.unregister(sourceOfTruthSubscriber);

        TruckDocument truckDocument = new TruckDocument(truck.getIdTruck(), truck.getCapacity());

        boolean sent = false;
        List<RequisitionSystemCollection> requisitionSystemCollections = requisitionSystemQueryRepository.findAll();
        for (RequisitionSystemCollection requisitionSystemCollection : requisitionSystemCollections) {
            List<RequisitionDocument> requisitionDocuments = requisitionSystemCollection.getRequisitions();
            List<RequisitionDocument> newRequisitionDocuments = new ArrayList<>();
            for (RequisitionDocument requisitionDocument : requisitionDocuments) {
                if (requisitionIds.contains(requisitionDocument.getId())) {
                    sent = true;
                    List<TruckDocument> truckDocuments = requisitionDocument.getTrucks();
                    if (truckDocuments == null) {
                        truckDocuments = new ArrayList<>();
                    }
                    truckDocuments.add(truckDocument);
                    requisitionDocument.setTrucks(truckDocuments);
                    newRequisitionDocuments.add(requisitionDocument);
                }
            }
            requisitionSystemCollection.setRequisitions(newRequisitionDocuments);

            if (sent) {
                RequisitionMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
                eventBusNoSql.post(new SubscriberCreatedEventNoSql(subscriberCommandService));
                eventBusNoSql.unregister(morphiaReadCacheSubscriber);
            }
        }

        return ResponseHolder.returnOkAndMessage("Requisition has been added to truck successfully");
    }


    //////////////////////////////////////////////////////
    // UPDATE Truck
    //////////////////////////////////////////////////////
    public ResponseHolder updateTruck(int idTruck, String requestBody) {
        Truck newTruck = createTruck(requestBody);

        if (!truckCommandRepository.existsById(idTruck)) {
            return ResponseHolder.returnBadRequestAndMessage("This Subscriber doesnt exist.");
        }

        Truck truck = truckCommandRepository.findTruckByIdTruck(idTruck);
        truck.setCapacity(newTruck.getCapacity());

        TruckOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new TruckOracleSourceOfTruthSubscriber(truck, eventBusSql);
        final TruckCreatedEventSql createdEventSql = new TruckCreatedEventSql(this);
        eventBusSql.post(createdEventSql);
        eventBusSql.unregister(sourceOfTruthSubscriber);

        List<RequisitionSystemCollection> requisitionSystemCollectionsList = requisitionSystemQueryRepository.findAll();
        for (RequisitionSystemCollection actualRequisitionSystemCollection : requisitionSystemCollectionsList) {
            List<RequisitionDocument> requisitionDocumentList = actualRequisitionSystemCollection.getRequisitions();
            List<RequisitionDocument> newRequisitionDocumentList = new ArrayList<>();
            for (RequisitionDocument actualRequisitionDocument : requisitionDocumentList) {
                List<TruckDocument> truckDocumentsList = actualRequisitionDocument.getTrucks();
                List<TruckDocument> newTruckDocumentsList = new ArrayList<>();
                for (TruckDocument actualTruckDocument : truckDocumentsList) {
                    if (actualTruckDocument.getId() == idTruck) {
                        actualTruckDocument.setCapacity(newTruck.getCapacity());
                    }
                    newTruckDocumentsList.add(actualTruckDocument);
                }
                actualRequisitionDocument.setTrucks(newTruckDocumentsList);
                newRequisitionDocumentList.add(actualRequisitionDocument);
            }
            actualRequisitionSystemCollection.setRequisitions(newRequisitionDocumentList);

            RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(actualRequisitionSystemCollection, eventBusNoSql);
            final SubscriberCreatedEventNoSql createdEventNoSql = new SubscriberCreatedEventNoSql(subscriberCommandService);
            eventBusNoSql.post(createdEventNoSql);
            eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);
        }
        return ResponseHolder.returnOkAndMessage("The truck has been successfully updated.");

    }


    //////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////
    public Truck saveTruck(Truck truck) {
        return truckCommandRepository.save(truck);
    }
}
