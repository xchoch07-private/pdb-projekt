package com.pdb.pdbprojekt.commands.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.ConvertDate;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.FinanceCreatedEventNoSql;
import com.pdb.pdbprojekt.events.GoodsCreatedEventSql;
import com.pdb.pdbprojekt.events.InvoiceCreatedEventSql;
import com.pdb.pdbprojekt.models.nosql.FinanceCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.SubscriberDocument;
import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.models.sql.Invoice;
import com.pdb.pdbprojekt.models.sql.Subscriber;
import com.pdb.pdbprojekt.models.sql.jsonObjects.InvoiceJackson;
import com.pdb.pdbprojekt.repositories.nosql.FinanceQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.InvoiceCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.SubscriberCommandRepository;
import com.pdb.pdbprojekt.subscribers.GoodsOracleSourceOfTruthSubscriber;
import com.pdb.pdbprojekt.subscribers.InvoiceMorphiaReadCacheSubscriber;
import com.pdb.pdbprojekt.subscribers.InvoiceOracleSourceOfTruthSubscriber;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;

@Service
public class InvoiceCommandService {

    @Autowired
    private InvoiceCommandRepository invoiceCommandRepository;

    @Autowired
    private FinanceQueryRepository financeQueryRepository;

    @Autowired
    private SubscriberCommandRepository subscriberCommandRepository;

    @Autowired
    private GoodsCommandRepository goodsCommandRepository;

    @Autowired
    private GoodsCommandService goodsCommandService;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();

    //////////////////////////////////////////////////////
    // CREATE NEW INVOICE
    //////////////////////////////////////////////////////
    public InvoiceJackson createInvoiceJackson(String json) {
        try {
            return (JsonToObjectParser.parseJsonToObject(json, InvoiceJackson.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }


    public double countTotalPrice(List<Integer> goods) {
        double totalPrice = 0;

        for (int actualGoodsId : goods) {
            Goods goods1 = goodsCommandRepository.findGoodsBySerialNumber(actualGoodsId);

            totalPrice += goods1.getProductReference().getPrice();
        }
        return totalPrice;
    }


    public ResponseHolder addNewInvoice(int subscriberId, String requestBody) {
        InvoiceJackson invoiceJackson = createInvoiceJackson(requestBody);
        Subscriber subscriber = subscriberCommandRepository.findSubscriberByIdSubscriber(subscriberId);


        for (int actualGoodsId : invoiceJackson.getGoods()) {
            Goods goods = goodsCommandRepository.findGoodsBySerialNumber(actualGoodsId);
            if (!goods.getState().equals("dispatched")) {
                return ResponseHolder.returnBadRequestAndMessage("Goods must have to state dispatched for invoice.");
            }
        }

        String supplier;
        if (subscriber.getName() == null) {
            supplier = subscriber.getFirstName() + " " + subscriber.getLastName();
        } else {
            supplier = subscriber.getName();
        }

        Date exposeDate = ConvertDate.convertStringToDate(invoiceJackson.getExposeDate());
        Date maturityDate = ConvertDate.convertStringToDate(invoiceJackson.getMaturityDate());

        double totalPrice = countTotalPrice(invoiceJackson.getGoods());

        Invoice invoice = new Invoice(
                supplier,
                exposeDate,
                maturityDate,
                invoiceJackson.getVariableSymbol(),
                totalPrice
        );

        invoice.setSubscriberReference(subscriber);

        List<Goods> goodsList = new ArrayList<>();
        for (int actualGoodsId : invoiceJackson.getGoods()) {
            Goods newGoods = goodsCommandRepository.findGoodsBySerialNumber(actualGoodsId);
            newGoods.setInvoiceReference(invoice);
        }

        //ulozeni faktury
        InvoiceOracleSourceOfTruthSubscriber invoiceOracleSourceOfTruthSubscriber = new InvoiceOracleSourceOfTruthSubscriber(invoice, eventBusSql);
        final InvoiceCreatedEventSql createdEventSql = new InvoiceCreatedEventSql(this);
        eventBusSql.post(createdEventSql);
        eventBusSql.unregister(invoiceOracleSourceOfTruthSubscriber);


        //preulozeni vsech zbozi

        for (Goods actualGoods : goodsList) {
            GoodsOracleSourceOfTruthSubscriber goodsOracleSourceOfTruthSubscriber = new GoodsOracleSourceOfTruthSubscriber(actualGoods, eventBusSql);
            final GoodsCreatedEventSql createdGoodsEventSql = new GoodsCreatedEventSql(goodsCommandService);
            eventBusSql.post(createdGoodsEventSql);
            eventBusSql.unregister(goodsOracleSourceOfTruthSubscriber);
        }

        FinanceCollection financeCollection = new FinanceCollection(
                supplier,
                exposeDate,
                maturityDate,
                invoiceJackson.getVariableSymbol(),
                totalPrice
        );

        SubscriberDocument subscriberDocument = new SubscriberDocument(
                subscriber.getIdSubscriber(),
                subscriber.getName(),
                subscriber.getFirstName(),
                subscriber.getLastName(),
                subscriber.getAddress(),
                subscriber.getIco(),
                subscriber.getAccountNumber()
        );

        financeCollection.setSubscriberDocument(subscriberDocument);

        List<GoodsDocument> goodsDocumentList = new ArrayList<>();
        for (int actualGoodsId : invoiceJackson.getGoods()) {
            Goods tempGoods = goodsCommandRepository.findGoodsBySerialNumber(actualGoodsId);
            goodsDocumentList.add(new GoodsDocument(
                    tempGoods.getSerialNumber(),
                    tempGoods.getState(),
                    tempGoods.getFabricationDate()
            ));
        }

        financeCollection.setGoods(goodsDocumentList);

        for (int actualGoodsId : invoiceJackson.getGoods()) {
            JSONObject obj = new JSONObject();
            List<Integer> goodsIds = new ArrayList<>();
            goodsIds.add(actualGoodsId);
            obj.put("goodsIds", goodsIds);
            obj.put("state", "invoiced");
            goodsCommandService.changeStateForGoods(obj.toJSONString());
        }

        //ulozeni finance
        InvoiceMorphiaReadCacheSubscriber invoiceMorphiaReadCacheSubscriber = new InvoiceMorphiaReadCacheSubscriber(financeCollection, eventBusNoSql);
        final FinanceCreatedEventNoSql createdEventNoSql = new FinanceCreatedEventNoSql(this);
        eventBusNoSql.post(createdEventNoSql);
        eventBusNoSql.unregister(invoiceMorphiaReadCacheSubscriber);

        return ResponseHolder.returnOkAndMessage("The invoice has been successfully created.");
    }


    //////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////

    public Invoice saveInvoice(Invoice invoice) {
        return invoiceCommandRepository.save(invoice);
    }

    public FinanceCollection saveFinanceNoSql(FinanceCollection financeCollection) {
        if (financeQueryRepository.findFinanceCollectionsById(financeCollection.getId()) != null) {
            if (financeQueryRepository.findFinanceCollectionsById(financeCollection.getId()).getVersion() == null) {
                financeCollection.setVersion(2);
            } else {
                financeCollection.setVersion(financeQueryRepository.findFinanceCollectionsById(financeCollection.getId()).getVersion() + 1);
            }
        }
        return financeQueryRepository.save(financeCollection);
    }

}
