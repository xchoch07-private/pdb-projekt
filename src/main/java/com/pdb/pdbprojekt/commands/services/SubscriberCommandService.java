package com.pdb.pdbprojekt.commands.services;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.SubscriberCreatedEventNoSql;
import com.pdb.pdbprojekt.events.SubscriberCreatedEventSql;
import com.pdb.pdbprojekt.events.SubscriberRemovedEventNoSql;
import com.pdb.pdbprojekt.events.SubscriberRemovedEventSql;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.sql.Subscriber;
import com.pdb.pdbprojekt.models.sql.jsonObjects.SubscriberJackson;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.SubscriberCommandRepository;
import com.pdb.pdbprojekt.subscribers.RequisitionMorphiaReadCacheSubscriber;
import com.pdb.pdbprojekt.subscribers.SubscriberOracleSourceOfTruthSubscriber;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriberCommandService {

    @Autowired
    private SubscriberCommandRepository subscriberCommandRepository;

    @Autowired
    private RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();


    public void saveOrUpdateSubscriber(Subscriber subscriber, RequisitionSystemCollection requisitionSystemCollection) {
        SubscriberOracleSourceOfTruthSubscriber subscriberOracleSourceOfTruthSubscriber = new SubscriberOracleSourceOfTruthSubscriber(subscriber, eventBusSql);
        RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
        final SubscriberCreatedEventSql createdEventSql = new SubscriberCreatedEventSql(this);
        eventBusSql.post(createdEventSql);
        eventBusSql.unregister(subscriberOracleSourceOfTruthSubscriber);
        final SubscriberCreatedEventNoSql createdEventNoSql = new SubscriberCreatedEventNoSql(this);
        eventBusNoSql.post(createdEventNoSql);
        eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);
    }

    //////////////////////////////////////////////////////
    // CREATE NEW SUBSCRIBER
    //////////////////////////////////////////////////////
    public Subscriber createSubscriber(String json) {
        SubscriberJackson subscriberJackson;
        try {
            subscriberJackson = JsonToObjectParser.parseJsonToObject(json, SubscriberJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return (new Subscriber(
                subscriberJackson.getName(),
                subscriberJackson.getFirstName(),
                subscriberJackson.getLastName(),
                subscriberJackson.getAddress(),
                subscriberJackson.getIco(),
                subscriberJackson.getAccountNumber()
        ));
    }

    public RequisitionSystemCollection createRequisitionSystemCollection(String json) {
        SubscriberJackson subscriberJackson;
        try {
            subscriberJackson = JsonToObjectParser.parseJsonToObject(json, SubscriberJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return (new RequisitionSystemCollection(
                subscriberJackson.getName(),
                subscriberJackson.getFirstName(),
                subscriberJackson.getLastName(),
                subscriberJackson.getAddress(),
                subscriberJackson.getIco(),
                subscriberJackson.getAccountNumber()
        ));
    }

    public ResponseHolder addNewSubscriber(String requestBody) {
        Subscriber subscriber = createSubscriber(requestBody);
        RequisitionSystemCollection requisitionSystemCollection = createRequisitionSystemCollection(requestBody);

        saveOrUpdateSubscriber(subscriber, requisitionSystemCollection);
        return ResponseHolder.returnOkAndMessage("The customer has been successfully created.");
    }


    //////////////////////////////////////////////////////
    // UPDATE SUBSCRIBER
    //////////////////////////////////////////////////////
    public ResponseHolder updateSubscriber(int idSubscriber, String requestBody) {
        Subscriber newSubscriber = createSubscriber(requestBody);

        if (!subscriberCommandRepository.existsById(idSubscriber)) {
            return ResponseHolder.returnBadRequestAndMessage("This Subscriber doesnt exist.");
        }
        Subscriber subscriber = subscriberCommandRepository.findSubscriberByIdSubscriber(idSubscriber);
        RequisitionSystemCollection requisitionSystemCollection = requisitionSystemQueryRepository.findById(idSubscriber);


        subscriber.setName(newSubscriber.getName());
        subscriber.setFirstName(newSubscriber.getFirstName());
        subscriber.setLastName(newSubscriber.getLastName());
        subscriber.setAddress(newSubscriber.getAddress());
        subscriber.setIco(newSubscriber.getIco());
        subscriber.setAccountNumber(newSubscriber.getAccountNumber());

        requisitionSystemCollection.setName(newSubscriber.getName());
        requisitionSystemCollection.setFirstName(newSubscriber.getFirstName());
        requisitionSystemCollection.setLastName(newSubscriber.getLastName());
        requisitionSystemCollection.setAddress(newSubscriber.getAddress());
        requisitionSystemCollection.setIco(newSubscriber.getIco());
        requisitionSystemCollection.setAccountNumber(newSubscriber.getAccountNumber());

        saveOrUpdateSubscriber(subscriber, requisitionSystemCollection);
        return ResponseHolder.returnOkAndMessage("The customer has been successfully created.");
    }


    //////////////////////////////////////////////////////
    // DELETE SUBSCRIBER
    //////////////////////////////////////////////////////
    public ResponseHolder deleteSubscriber(int id) {
        Subscriber subscriber = subscriberCommandRepository.findSubscriberByIdSubscriber(id);

        RequisitionSystemCollection requisitionSystemCollection = requisitionSystemQueryRepository.findById(id);

        SubscriberOracleSourceOfTruthSubscriber subscriberOracleSourceOfTruthSubscriber = new SubscriberOracleSourceOfTruthSubscriber(subscriber, eventBusSql);
        RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
        final SubscriberRemovedEventSql removedEventSql = new SubscriberRemovedEventSql(this);
        eventBusSql.post(removedEventSql);
        eventBusSql.unregister(subscriberOracleSourceOfTruthSubscriber);
        final SubscriberRemovedEventNoSql removedEventNoSql = new SubscriberRemovedEventNoSql(this);
        eventBusNoSql.post(removedEventNoSql);
        eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);
        return ResponseHolder.returnOkAndMessage("The customer has been successfully deleted.");
    }

    ///////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////
    public Subscriber saveSubscriber(Subscriber subscriber) {
        return subscriberCommandRepository.save(subscriber);
    }

    public RequisitionSystemCollection saveSubscriberNoSql(RequisitionSystemCollection requisitionSystemCollection) {
        if (requisitionSystemQueryRepository.findById(requisitionSystemCollection.getId()) != null) {
            if (requisitionSystemQueryRepository.findById(requisitionSystemCollection.getId()).getVersion() == null) {
                requisitionSystemCollection.setVersion(2);
            } else {
                requisitionSystemCollection.setVersion(requisitionSystemQueryRepository.findById(requisitionSystemCollection.getId()).getVersion() + 1);
            }
        }
        return requisitionSystemQueryRepository.save(requisitionSystemCollection);
    }


    public void removeSubscriber(Subscriber subscriber) {
        subscriberCommandRepository.delete(subscriber);
    }

    public void removeSubscriberNoSql(RequisitionSystemCollection requisitionSystemCollection) {
        requisitionSystemQueryRepository.delete(requisitionSystemCollection);
    }
}
