package com.pdb.pdbprojekt.commands.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.*;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.models.sql.Regal;
import com.pdb.pdbprojekt.models.sql.Requisition;
import com.pdb.pdbprojekt.models.sql.jsonObjects.GoodsStateJackson;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.RegalCommandRepository;
import com.pdb.pdbprojekt.subscribers.*;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class GoodsCommandService {

    @Autowired
    GoodsCommandRepository goodsCommandRepository;

    @Autowired
    GoodsQueryRepository goodsQueryRepository;

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RegalCommandRepository regalCommandRepository;

    @Autowired
    RegalCommandService regalCommandService;

    @Autowired
    RegalQueryRepository regalQueryRepository;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();

    ///////////////////////////////////////////////////////
    // CHANGE GOODS STATE
    //////////////////////////////////////////////////////


    public void setStateForRequisition(Goods tmp, String state) {
        Requisition requisition = tmp.getRequisitionReference();
        List<Goods> allGoods = goodsCommandRepository.findAll();
        for (Goods actualGoods : allGoods) {
            if (actualGoods.getRequisitionReference() == requisition && !(actualGoods.getState().equals(state))) {
                requisition.setRequisitionState(state);
                RequisitionOracleSourceOfTruthSubscriber requisitionOracleSourceOfTruthSubscriber = new RequisitionOracleSourceOfTruthSubscriber(requisition, eventBusSql);
                final RequisitionCreatedEventSql createdEventSql = new RequisitionCreatedEventSql(requisitionCommandService);
                eventBusSql.post(createdEventSql);
                eventBusSql.unregister(requisitionOracleSourceOfTruthSubscriber);

                int subscriber = requisition.getSubscriberReference().getIdSubscriber();

                RequisitionSystemCollection requisitionSystemCollection = requisitionSystemQueryRepository.findRequisitionSystemCollectionById(subscriber);
                List<RequisitionDocument> requisitionDocuments = requisitionSystemCollection.getRequisitions();
                List<RequisitionDocument> newRequisitionDocuments = new ArrayList<>();
                for (RequisitionDocument tmp2 : requisitionDocuments) {
                    if (tmp2.getId() == requisition.getIdRequisition()) {
                        tmp2.setRequisitionState(state);
                    }
                    newRequisitionDocuments.add(tmp2);
                }
                requisitionSystemCollection.setRequisitions(newRequisitionDocuments);

                RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
                final SubscriberCreatedEventNoSql createdEventNoSql = new SubscriberCreatedEventNoSql(subscriberCommandService);
                eventBusNoSql.post(createdEventNoSql);
                eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);


            }
        }
    }

    public ResponseHolder changeStateForGoods(String requestBody) {
        GoodsStateJackson goodsStateJackson;
        try {
            goodsStateJackson = JsonToObjectParser.parseJsonToObject(requestBody, GoodsStateJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseHolder.returnBadRequestAndMessage("Request body error");
        }

        for (int goodsId : goodsStateJackson.getGoodsIds()) {

            Goods goods = goodsCommandRepository.findGoodsBySerialNumber(goodsId);
            GoodsCollection goodsCollection = goodsQueryRepository.findGoodsCollectionById(goodsId);

            goods.setState(goodsStateJackson.getState());
            goodsCollection.setState(goodsStateJackson.getState());

            GoodsOracleSourceOfTruthSubscriber goodsOracleSourceOfTruthSubscriber = new GoodsOracleSourceOfTruthSubscriber(goods, eventBusSql);
            final GoodsCreatedEventSql createdEventSql = new GoodsCreatedEventSql(this);
            eventBusSql.post(createdEventSql);
            eventBusSql.unregister(goodsOracleSourceOfTruthSubscriber);

            GoodsMorphiaReadCacheSubscriber goodsMorphiaReadCacheSubscriber = new GoodsMorphiaReadCacheSubscriber(goodsCollection, eventBusNoSql);
            final GoodsCreatedEventNoSql createdEventNoSql = new GoodsCreatedEventNoSql(this);
            eventBusNoSql.post(createdEventNoSql);
            eventBusNoSql.unregister(goodsMorphiaReadCacheSubscriber);

            //Change in RequisitionSystemCollection
            boolean sent = false;
            List<RequisitionSystemCollection> requisitionSystemCollections = requisitionSystemQueryRepository.findAll();
            for (RequisitionSystemCollection requisitionSystemCollection : requisitionSystemCollections) {
                List<RequisitionDocument> requisitionDocumentList = requisitionSystemCollection.getRequisitions();
                List<RequisitionDocument> newRequisitionDocumentList = new ArrayList<>();
                if (requisitionDocumentList != null) {
                    for (RequisitionDocument requisitionDocument : requisitionDocumentList) {
                        List<GoodsDocument> goodsList = requisitionDocument.getGoods();
                        List<GoodsDocument> newGoodsList = new ArrayList<>();
                        for (GoodsDocument actualGoodsDocument : goodsList) {
                            if (goodsId == actualGoodsDocument.getSerialNumber()) {
                                actualGoodsDocument.setFabricationDate(new Date());
                                actualGoodsDocument.setState(goodsStateJackson.getState());
                                sent = true;
                            }
                            newGoodsList.add(actualGoodsDocument);
                        }
                        requisitionDocument.setGoods(newGoodsList);
                        newRequisitionDocumentList.add(requisitionDocument);
                    }
                    requisitionSystemCollection.setRequisitions(newRequisitionDocumentList);
                }

                if (sent) {
                    RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
                    final SubscriberCreatedEventNoSql createdEventNoSql2 = new SubscriberCreatedEventNoSql(subscriberCommandService);
                    eventBusNoSql.post(createdEventNoSql2);
                    eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);
                }
            }

            if (goodsStateJackson.getState().equals("loading")) {
                Goods goods1 = goodsCommandRepository.findGoodsBySerialNumber(goodsId);
                List<Regal> regalList = regalCommandRepository.findAll();
                if (regalList.size() > 0) {
                    for (Regal actualRegal : regalList) {
                        List<Goods> goodsList = actualRegal.getGoods();
                        if (!actualRegal.getGoods().contains(goods1)) {
                            goodsList.remove(goods1);
                        }
                        actualRegal.setGoods(goodsList);
                        RegalOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new RegalOracleSourceOfTruthSubscriber(actualRegal, eventBusSql);
                        final RegalCreatedEventSql createdEventSql2 = new RegalCreatedEventSql(regalCommandService);
                        eventBusSql.post(createdEventSql2);
                        eventBusSql.unregister(sourceOfTruthSubscriber);
                    }
                }

                List<RegalCollection> regalCollectionList = regalQueryRepository.findAll();
                for (RegalCollection actualRegalCollection : regalCollectionList) {
                    List<GoodsDocument> goodsDocumentsList = actualRegalCollection.getGoods();
                    if (goodsDocumentsList != null) {
                        List<GoodsDocument> newGoodsDocumentsList = new ArrayList<>();
                        for (GoodsDocument actualGoodsDocument : goodsDocumentsList) {
                            if (actualGoodsDocument.getSerialNumber() != goodsId) {
                                newGoodsDocumentsList.add(actualGoodsDocument);
                            }
                        }
                        actualRegalCollection.setGoods(newGoodsDocumentsList);
                        RegalMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new RegalMorphiaReadCacheSubscriber(actualRegalCollection, eventBusNoSql);
                        final RegalCreatedEventNoSql createdEventNoSql2 = new RegalCreatedEventNoSql(regalCommandService);
                        eventBusNoSql.post(createdEventNoSql2);
                        eventBusNoSql.unregister(morphiaReadCacheSubscriber);
                    }
                }
            }

            if (goodsStateJackson.getState().equals("dispatched")) {
                setStateForRequisition(goods, "dispatched");
            }
        }
        return ResponseHolder.returnOkAndMessage("State was successfully changed.");
    }


    ///////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////
    public Goods saveGoods(Goods goods) {
        return goodsCommandRepository.save(goods);
    }

    public GoodsCollection saveGoodsCollectionNoSql(GoodsCollection goodsCollection) {
        if (goodsQueryRepository.findGoodsCollectionById(goodsCollection.getId()) != null) {
            if (goodsQueryRepository.findGoodsCollectionById(goodsCollection.getId()).getVersion() == null) {
                goodsCollection.setVersion(2);
            } else {
                goodsCollection.setVersion(goodsQueryRepository.findGoodsCollectionById(goodsCollection.getId()).getVersion() + 1);
            }
        }
        return goodsQueryRepository.save(goodsCollection);
    }

}
