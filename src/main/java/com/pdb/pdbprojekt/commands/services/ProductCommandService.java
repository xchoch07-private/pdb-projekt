package com.pdb.pdbprojekt.commands.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.*;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import com.pdb.pdbprojekt.models.nosql.documents.ProductDocument;
import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.models.sql.Product;
import com.pdb.pdbprojekt.models.sql.jsonObjects.ProductJackson;
import com.pdb.pdbprojekt.repositories.nosql.ProductQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.ProductCommandRepository;
import com.pdb.pdbprojekt.subscribers.ProductMorphiaReadCacheSubscriber;
import com.pdb.pdbprojekt.subscribers.ProductOracleSourceOfTruthSubscriber;
import com.pdb.pdbprojekt.subscribers.RegalMorphiaReadCacheSubscriber;
import com.pdb.pdbprojekt.subscribers.RegalOracleSourceOfTruthSubscriber;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductCommandService {

    @Autowired
    ProductCommandRepository productCommandRepository;

    @Autowired
    ProductQueryRepository productQueryRepository;

    @Autowired
    RegalQueryRepository regalQueryRepository;

    @Autowired
    GoodsCommandRepository goodsCommandRepository;

    @Autowired
    RegalCommandService regalCommandService;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();

    //////////////////////////////////////////////////////
    // CREATE NEW SUBSCRIBER
    //////////////////////////////////////////////////////
    public Product createProduct(String json) {
        ProductJackson productJackson;
        try {
            productJackson = JsonToObjectParser.parseJsonToObject(json, ProductJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return new Product(productJackson.getName(), productJackson.getPrice(), productJackson.getDimension(), productJackson.getProductCode());
    }

    public ProductCollection createProductCollection(String json) {
        ProductJackson productJackson;
        try {
            productJackson = JsonToObjectParser.parseJsonToObject(json, ProductJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return new ProductCollection(productJackson.getName(), productJackson.getPrice(), productJackson.getDimension(), productJackson.getProductCode());
    }

    public ResponseHolder addProduct(String requestBody) {

        Product product = createProduct(requestBody);
        ProductCollection productCollection = createProductCollection(requestBody);

        sendProductToDB(product, productCollection);
        return ResponseHolder.returnOkAndMessage("The product has been successfully created.");
    }

    //////////////////////////////////////////////////////
    // UPDATE PRODUCT
    //////////////////////////////////////////////////////
    public ResponseHolder updateProduct(int idProduct, String json) {
        Product tmp = productCommandRepository.findByIdProduct(idProduct);
        if (tmp == null) {
            return ResponseHolder.returnBadRequestAndMessage("This product isn't in system.");
        }

        Product product = createProduct(json);
        product.setIdProduct(idProduct);

        ProductCollection productCollection = createProductCollection(json);
        productCollection.setId(idProduct);

        sendProductToDB(product, productCollection);

        List<RegalCollection> regalCollectionList = regalQueryRepository.findAll();
        for (RegalCollection regalCollection : regalCollectionList) {
            if (regalCollection.getProductReference().getIdProduct() == idProduct) {
                ProductDocument productDocument = new ProductDocument(
                        productCollection.getId(),
                        productCollection.getName(),
                        productCollection.getPrice(),
                        productCollection.getDimension(),
                        productCollection.getProductCode()
                );
                regalCollection.setProductReference(productDocument);
                RegalMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new RegalMorphiaReadCacheSubscriber(regalCollection, eventBusNoSql);
                final RegalCreatedEventNoSql createdEventNoSql = new RegalCreatedEventNoSql(regalCommandService);
                eventBusNoSql.post(createdEventNoSql);
                eventBusNoSql.unregister(morphiaReadCacheSubscriber);
            }
        }

        return ResponseHolder.returnOkAndMessage("Product has been updated successfully.");
    }

    //////////////////////////////////////////////////////
    // DELETE PRODUCT
    //////////////////////////////////////////////////////
    public ResponseHolder deleteProduct(int idProduct) {
        Product product = productCommandRepository.findByIdProduct(idProduct);
        if (product == null) {
            return ResponseHolder.returnBadRequestAndMessage("Product isn't in system.");
        }

        List<Goods> goodsList = goodsCommandRepository.findAll();
        for (Goods goods : goodsList) {
            if (goods.getProductReference().getIdProduct() == idProduct) {
                return ResponseHolder.returnBadRequestAndMessage("Cannot delete product if goods this product exist.");
            }
        }

        ProductCollection productCollection = productQueryRepository.findById(idProduct);

        ProductOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new ProductOracleSourceOfTruthSubscriber(product, eventBusSql);
        ProductMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new ProductMorphiaReadCacheSubscriber(productCollection, eventBusNoSql);

        eventBusSql.post(new ProductRemovedEventSql(this));
        eventBusSql.unregister(sourceOfTruthSubscriber);
        eventBusNoSql.post(new ProductRemovedEventNoSql(this));
        eventBusNoSql.unregister(morphiaReadCacheSubscriber);

        List<RegalCollection> regalCollectionList = regalQueryRepository.findAll();
        for (RegalCollection actualRegalCollection : regalCollectionList) {

            if (actualRegalCollection.getProductReference().getIdProduct() == idProduct) {
                RegalCollection newRegalCollection = new RegalCollection(
                        actualRegalCollection.getId(),
                        actualRegalCollection.getSection(),
                        actualRegalCollection.getSector(),
                        actualRegalCollection.getPosition()
                );
                actualRegalCollection = newRegalCollection;
            }

            RegalMorphiaReadCacheSubscriber morphiaReadCacheSubscriber2 = new RegalMorphiaReadCacheSubscriber(actualRegalCollection, eventBusNoSql);
            final RegalCreatedEventNoSql createdEventNoSql = new RegalCreatedEventNoSql(regalCommandService);
            eventBusNoSql.post(createdEventNoSql);
            eventBusNoSql.unregister(morphiaReadCacheSubscriber2);
        }
        return ResponseHolder.returnOkAndMessage("Product has been successfully deleted.");
    }

    //////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////

    public Product saveProductSql(Product product) {
        return productCommandRepository.save(product);
    }

    public ProductCollection saveProductNoSql(ProductCollection productCollection) {
        if (productQueryRepository.findById(productCollection.getId()) != null) {
            if (productQueryRepository.findById(productCollection.getId()).getVersion() == null) {
                productCollection.setVersion(2);
            } else {
                productCollection.setVersion(productQueryRepository.findById(productCollection.getId()).getVersion() + 1);
            }
        }

        return productQueryRepository.save(productCollection);
    }

    public void removeProduct(Product product) {
        productCommandRepository.delete(product);
    }

    public void removeProductNoSql(ProductCollection productCollection) {
        productQueryRepository.delete(productCollection);
    }

    //////////////////////////////////////////////////////
    // HELPFUL METHODS
    //////////////////////////////////////////////////////
    private void sendProductToDB(Product product, ProductCollection productCollection) {
        ProductOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new ProductOracleSourceOfTruthSubscriber(product, eventBusSql);
        ProductMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new ProductMorphiaReadCacheSubscriber(productCollection, eventBusNoSql);
        final ProductCreatedEventSql createdEventSql = new ProductCreatedEventSql(this);
        eventBusSql.post(createdEventSql);
        eventBusNoSql.unregister(sourceOfTruthSubscriber);
        final ProductCreatedEventNoSql createdEventNoSql = new ProductCreatedEventNoSql(this);
        eventBusNoSql.post(createdEventNoSql);
        eventBusNoSql.unregister(morphiaReadCacheSubscriber);
    }
}
