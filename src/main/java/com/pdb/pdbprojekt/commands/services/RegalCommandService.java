package com.pdb.pdbprojekt.commands.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.*;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.ProductDocument;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.models.sql.*;
import com.pdb.pdbprojekt.models.sql.jsonObjects.GoodsIdForRegalJackson;
import com.pdb.pdbprojekt.models.sql.jsonObjects.ProductIdJackson;
import com.pdb.pdbprojekt.models.sql.jsonObjects.RegalJackson;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.ProductCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.RegalCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.RequisitionCommandRepository;
import com.pdb.pdbprojekt.subscribers.*;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RegalCommandService {

    @Autowired
    private StorageCommandService storageCommandService;

    @Autowired
    private GoodsCommandService goodsCommandService;

    @Autowired
    private RegalCommandRepository regalCommandRepository;

    @Autowired
    private RegalQueryRepository regalQueryRepository;

    @Autowired
    private ProductCommandRepository productCommandRepository;

    @Autowired
    private GoodsCommandRepository goodsCommandRepository;

    @Autowired
    private GoodsQueryRepository goodsQueryRepository;

    @Autowired
    private RequisitionCommandRepository requisitionCommandRepository;

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();

    //////////////////////////////////////////////////////
    // CREATE NEW Regal
    //////////////////////////////////////////////////////
    public Regal createRegal(String json) {
        RegalJackson regalJackson;

        try {
            regalJackson = JsonToObjectParser.parseJsonToObject(json, RegalJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        return new Regal(regalJackson.getSection(), regalJackson.getSector(), regalJackson.getPosition());
    }

    public RegalCollection createRegalCollection(String json) {
        RegalJackson regalJackson;

        try {
            regalJackson = JsonToObjectParser.parseJsonToObject(json, RegalJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
        return new RegalCollection(regalJackson.getSection(), regalJackson.getSector(), regalJackson.getPosition());
    }

    public ResponseHolder addNewRegal(int idStorage, String json) {
        Regal regal = createRegal(json);
        regal.setStorageReference(storageCommandService.getStorage(idStorage));

        RegalCollection regalCollection = createRegalCollection(json);
        regalCollection.setStorageReference(storageCommandService.getStorageCollectionNoSql(idStorage));

        eventsForCreatingOrUpdatingRegal(regalCollection, regal);

        return ResponseHolder.returnOkAndMessage("The regal has been successfully created in storage: " + idStorage);
    }

    //////////////////////////////////////////////////////
    // ADD PRODUCTS TO REGAL
    //////////////////////////////////////////////////////

    public ProductIdJackson createProductIdJackson(String json) {
        try {
            return JsonToObjectParser.parseJsonToObject(json, ProductIdJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResponseHolder addNewProductsToRegal(int idRegal, String json) {
        Regal regal = regalCommandRepository.findRegalByIdRegal(idRegal);
        if (regal == null) {
            return ResponseHolder.returnBadRequestAndMessage("Regal does not exists.");
        }
        RegalCollection regalCollection = regalQueryRepository.findRegalCollectionById(idRegal);
        ProductIdJackson productIdJackson = createProductIdJackson(json);

        Product product = productCommandRepository.findByIdProduct(productIdJackson.getProduct());
        regal.setProductReference(product);

        ProductDocument productDocument = new ProductDocument(
                product.getIdProduct(),
                product.getName(),
                product.getPrice(),
                product.getDimension(),
                product.getProductCode()
        );
        regalCollection.setProductReference(productDocument);

        eventsForCreatingOrUpdatingRegal(regalCollection, regal);

        return ResponseHolder.returnOkAndMessage("Product have been successfully added to regal " + idRegal);
    }

    //////////////////////////////////////////////////////
    // ADD GOODS TO REGAL
    //////////////////////////////////////////////////////
    public GoodsIdForRegalJackson createGoodsIds(String json) {
        GoodsIdForRegalJackson goodsIdForRegalJackson;
        try {
            goodsIdForRegalJackson = JsonToObjectParser.parseJsonToObject(json, GoodsIdForRegalJackson.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

        return goodsIdForRegalJackson;
    }


    public void setStateForRequisition(Goods tmp) {
        Requisition requisition = tmp.getRequisitionReference();
        List<Goods> allGoods = goodsCommandRepository.findAll();
        for (Goods actualGoods : allGoods) {
            if (actualGoods.getRequisitionReference() == requisition && !(actualGoods.getState().equals("in-regal"))) {
                requisition.setRequisitionState("in-regal");
                RequisitionOracleSourceOfTruthSubscriber requisitionOracleSourceOfTruthSubscriber = new RequisitionOracleSourceOfTruthSubscriber(requisition, eventBusSql);
                final RequisitionCreatedEventSql createdEventSql = new RequisitionCreatedEventSql(requisitionCommandService);
                eventBusSql.post(createdEventSql);
                eventBusSql.unregister(requisitionOracleSourceOfTruthSubscriber);

                int subscriber = requisition.getSubscriberReference().getIdSubscriber();

                RequisitionSystemCollection requisitionSystemCollection = requisitionSystemQueryRepository.findRequisitionSystemCollectionById(subscriber);
                List<RequisitionDocument> requisitionDocuments = requisitionSystemCollection.getRequisitions();
                List<RequisitionDocument> newRequisitionDocuments = new ArrayList<>();
                for (RequisitionDocument tmp2 : requisitionDocuments) {
                    if (tmp2.getId() == requisition.getIdRequisition()) {
                        tmp2.setRequisitionState("in-regal");
                    }
                    newRequisitionDocuments.add(tmp2);
                }
                requisitionSystemCollection.setRequisitions(newRequisitionDocuments);

                RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
                final SubscriberCreatedEventNoSql createdEventNoSql = new SubscriberCreatedEventNoSql(subscriberCommandService);
                eventBusNoSql.post(createdEventNoSql);
                eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);


            }
        }
    }


    public ResponseHolder addNewGoodsToRegal(int idRegal, String json) {
        GoodsIdForRegalJackson goodsIdForRegalJackson = createGoodsIds(json);
        List<Integer> goodsIds = goodsIdForRegalJackson.getGoodsIds();
        Regal regal = regalCommandRepository.findRegalByIdRegal(idRegal);
        int idProduct = regal.getProductReference().getIdProduct();

        ArrayList<Goods> goods = new ArrayList<>();
        for (int id : goodsIds) {
            goods.add(goodsCommandRepository.findGoodsBySerialNumber(id));
        }

        //jeste zkontrolovat asi ze to je in-progress
        for (Goods tmp : goods) {
            if (tmp.getProductReference().getIdProduct() != idProduct) {
                return ResponseHolder.returnBadRequestAndMessage("Cannot insert goods into regal" + idRegal +
                        ". In this regal is another product.");
            }
        }

        //muzu vyrobky ulozit do regalu //in-regal
        boolean allGoodsFromRequisitionOnStore = true;
        ArrayList<Goods> goodsToSql = new ArrayList<>();
        for (Goods tmp : goods) {
            tmp.setState("in-regal");
            tmp.setFabricationDate(new Date());
            goodsToSql.add(tmp);
            setStateForRequisition(tmp);
        }

        List<GoodsCollection> goodsCollections = new ArrayList<>();
        for (int id : goodsIds) {
            goodsCollections.add(goodsQueryRepository.findGoodsCollectionById(id));
        }

        ArrayList<GoodsCollection> goodsToNoSql = new ArrayList<>();
        for (GoodsCollection tmp : goodsCollections) {
            tmp.setState("in-regal");
            tmp.setFabricationDate(new Date());
            goodsToNoSql.add(tmp);
        }

        //save to DBs
        for (Goods tmp : goodsToSql) {
            GoodsOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new GoodsOracleSourceOfTruthSubscriber(tmp, eventBusSql);
            eventBusSql.post(new GoodsCreatedEventSql(goodsCommandService));
            eventBusSql.unregister(sourceOfTruthSubscriber);
        }

        for (GoodsCollection tmp : goodsToNoSql) {
            GoodsMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new GoodsMorphiaReadCacheSubscriber(tmp, eventBusNoSql);
            eventBusNoSql.post(new GoodsCreatedEventNoSql(goodsCommandService));
            eventBusNoSql.unregister(morphiaReadCacheSubscriber);

        }

        regal.setGoods(goodsToSql);
        RegalOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new RegalOracleSourceOfTruthSubscriber(regal, eventBusSql);
        eventBusSql.post(new RegalCreatedEventSql(this));
        eventBusSql.unregister(sourceOfTruthSubscriber);

        ArrayList<GoodsDocument> goodsDocuments = new ArrayList<>();
        for (GoodsCollection tmp : goodsToNoSql) {
            goodsDocuments.add(new GoodsDocument(tmp.getId(), tmp.getState(), tmp.getFabricationDate()));
        }

        RegalCollection regalCollection = regalQueryRepository.findRegalCollectionById(idRegal);
        regalCollection.setGoods(goodsDocuments);
        RegalMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new RegalMorphiaReadCacheSubscriber(regalCollection, eventBusNoSql);
        eventBusNoSql.post(new RegalCreatedEventNoSql(this));
        eventBusNoSql.unregister(morphiaReadCacheSubscriber);

        boolean sent = false;
        List<RequisitionSystemCollection> requisitionSystemCollections = requisitionSystemQueryRepository.findAll();
        for (RequisitionSystemCollection requisitionSystemCollection : requisitionSystemCollections) {
            List<RequisitionDocument> requisitionDocumentList = requisitionSystemCollection.getRequisitions();
            List<RequisitionDocument> newRequisitionDocumentList = new ArrayList<>();
            if (requisitionDocumentList != null) {
                for (RequisitionDocument requisitionDocument : requisitionDocumentList) {
                    List<GoodsDocument> goodsList = requisitionDocument.getGoods();
                    List<GoodsDocument> newGoodsList = new ArrayList<>();
                    for (GoodsDocument actualGoodsDocument : goodsList) {
                        if (goodsIdForRegalJackson.getGoodsIds().contains(actualGoodsDocument.getSerialNumber())) {
                            actualGoodsDocument.setFabricationDate(new Date());
                            actualGoodsDocument.setState("in-regal");
                            sent = true;
                        }
                        newGoodsList.add(actualGoodsDocument);
                    }
                    requisitionDocument.setGoods(newGoodsList);
                    newRequisitionDocumentList.add(requisitionDocument);
                }
                requisitionSystemCollection.setRequisitions(newRequisitionDocumentList);
            }
            if (sent) {
                RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
                final SubscriberCreatedEventNoSql createdEventNoSql = new SubscriberCreatedEventNoSql(subscriberCommandService);
                eventBusNoSql.post(createdEventNoSql);
                eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);
            }
        }


        //TODO pridat kontrolu jestli jsou vyrobene vsechny vyrobky v objednavce a v tom pripade ji zmenit stav

        return ResponseHolder.returnOkAndMessage("Goods have been successfully added to regal " + idRegal);
    }

    //////////////////////////////////////////////////////
    // UPDATE Regal
    //////////////////////////////////////////////////////
    public ResponseHolder updateRegal(int idRegal, String requestBody) {
        Regal tmp = regalCommandRepository.findRegalByIdRegal(idRegal);
        if (tmp == null) {
            return ResponseHolder.returnBadRequestAndMessage("This regal isn't in system.");
        }

        Regal regal = createRegal(requestBody);
        regal.setIdRegal(idRegal);

        RegalCollection regalCollectionTmp = createRegalCollection(requestBody);
        RegalCollection regalCollection = regalQueryRepository.findRegalCollectionById(idRegal);
        regalCollection.setPosition(regalCollectionTmp.getPosition());
        regalCollection.setSection(regalCollectionTmp.getSection());
        regalCollection.setSector(regalCollectionTmp.getSector());

        eventsForCreatingOrUpdatingRegal(regalCollection, regal);

        return ResponseHolder.returnOkAndMessage("Regal has been successfully updated.");
    }

    //////////////////////////////////////////////////////
    // DELETE Regal
    //////////////////////////////////////////////////////
    public ResponseHolder deleteRegal(int id) {
        Regal regal = regalCommandRepository.findRegalByIdRegal(id);
        if (regal == null) {
            return ResponseHolder.returnBadRequestAndMessage("Regal isn't in system.");
        }

        RegalCollection regalCollection = regalQueryRepository.findRegalCollectionById(id);

        if (regal.getProductReference() != null) {
            return ResponseHolder.returnBadRequestAndMessage("Cannot delete regal with product.");
        }

        RegalOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new RegalOracleSourceOfTruthSubscriber(regal, eventBusSql);
        RegalMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new RegalMorphiaReadCacheSubscriber(regalCollection, eventBusNoSql);

        eventBusSql.post(new RegalRemovedEventSql(this));
        eventBusSql.unregister(sourceOfTruthSubscriber);
        eventBusNoSql.post(new RegalRemovedEventNoSql(this));
        eventBusNoSql.unregister(morphiaReadCacheSubscriber);

        return ResponseHolder.returnOkAndMessage("Regal has been successfully deleted.");
    }

    //////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////
    public Regal saveRegal(Regal regal) {
        return regalCommandRepository.save(regal);
    }

    public RegalCollection saveRegalNoSql(RegalCollection regalCollection) {
        if (regalQueryRepository.findRegalCollectionById(regalCollection.getId()) != null) {
            if (regalQueryRepository.findRegalCollectionById(regalCollection.getId()).getVersion() == null) {
                regalCollection.setVersion(2);
            } else {
                regalCollection.setVersion(regalQueryRepository.findRegalCollectionById(regalCollection.getId()).getVersion() + 1);
            }
        }
        return regalQueryRepository.save(regalCollection);
    }

    public void removeRegal(Regal regal) {
        regalCommandRepository.delete(regal);
    }

    public void removeRegalNoSql(RegalCollection regalCollection) {
        regalQueryRepository.delete(regalCollection);
    }


    //////////////////////////////////////////////////////
    // HELPFUL METHODS
    //////////////////////////////////////////////////////
    private void eventsForCreatingOrUpdatingRegal(RegalCollection regalCollection, Regal regal) {
        RegalOracleSourceOfTruthSubscriber sourceOfTruthSubscriber = new RegalOracleSourceOfTruthSubscriber(regal, eventBusSql);
        RegalMorphiaReadCacheSubscriber morphiaReadCacheSubscriber = new RegalMorphiaReadCacheSubscriber(regalCollection, eventBusNoSql);

        final RegalCreatedEventSql createdEventSql = new RegalCreatedEventSql(this);
        eventBusSql.post(createdEventSql);
        eventBusSql.unregister(sourceOfTruthSubscriber);
        final RegalCreatedEventNoSql createdEventNoSql = new RegalCreatedEventNoSql(this);
        eventBusNoSql.post(createdEventNoSql);
        eventBusNoSql.unregister(morphiaReadCacheSubscriber);
    }
}
