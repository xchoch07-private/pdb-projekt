package com.pdb.pdbprojekt.commands.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pdb.pdbprojekt.common.ConvertDate;
import com.pdb.pdbprojekt.common.JsonToObjectParser;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.events.*;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.models.nosql.sequences.CollectionSequences;
import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.models.sql.Product;
import com.pdb.pdbprojekt.models.sql.Requisition;
import com.pdb.pdbprojekt.models.sql.jsonObjects.GoodsForRequisitionJackson;
import com.pdb.pdbprojekt.models.sql.jsonObjects.RequisitionJackson;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.ProductQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.SequenceQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.ProductCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.RequisitionCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.SubscriberCommandRepository;
import com.pdb.pdbprojekt.subscribers.*;
import org.greenrobot.eventbus.EventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class RequisitionCommandService {

    @Autowired
    ProductCommandRepository productCommandRepository;

    @Autowired
    ProductQueryRepository productQueryRepository;

    @Autowired
    GoodsCommandService goodsCommandService;

    @Autowired
    GoodsCommandRepository goodsCommandRepository;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RequisitionCommandRepository requisitionCommandRepository;

    @Autowired
    SequenceQueryRepository sequenceQueryRepository;

    @Autowired
    SubscriberCommandRepository subscriberCommandRepository;

    @Autowired
    GoodsQueryRepository goodsQueryRepository;

    final EventBus eventBusSql = EventBus.getDefault();
    final EventBus eventBusNoSql = EventBus.getDefault();


    private int getSequence(String sequenceName) {
        CollectionSequences collectionSequences;
        try {
            collectionSequences = sequenceQueryRepository.findById(sequenceName).get();
        } catch (NoSuchElementException e) {
            collectionSequences = null;
        }
        int requisitionSequenceId;
        if (collectionSequences == null) {
            requisitionSequenceId = 1;
        } else {
            requisitionSequenceId = (int) collectionSequences.getSeq() + 1;
        }

        sequenceQueryRepository.save(new CollectionSequences(
                sequenceName,
                requisitionSequenceId
        ));

        return requisitionSequenceId;
    }


    //////////////////////////////////////////////////////
    // CREATE NEW SUBSCRIBER
    //////////////////////////////////////////////////////

    public RequisitionJackson createRequisitionJackson(String json) {
        try {
            return (JsonToObjectParser.parseJsonToObject(json, RequisitionJackson.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResponseHolder addRequisition(int idSubscriber, String requestBody) {
        RequisitionJackson requisitionJackson = createRequisitionJackson(requestBody);

        //vytvorit novou objednavku SQL
        Requisition requisition = new Requisition(
                requisitionJackson.getRequisitionState(),
                requisitionJackson.getNote(),
                ConvertDate.convertStringToDate(requisitionJackson.getOrderCreationDate())
        );

        //vytvorit novou objednavku NoSql
        RequisitionDocument requisitionDocument = new RequisitionDocument(
                requisitionJackson.getRequisitionState(),
                requisitionJackson.getNote(),
                ConvertDate.convertStringToDate(requisitionJackson.getOrderCreationDate())
        );

        int requisitionSequenceId = getSequence("requisition_document_sequences");
        requisitionDocument.setId(requisitionSequenceId);

        //vytvorit list zbozi pro SQL
        List<Goods> goodsList = new ArrayList<>();
        List<GoodsDocument> goodsDocumentList = new ArrayList<>();
        List<GoodsCollection> goodsCollectionsList = new ArrayList<>();
        List<GoodsForRequisitionJackson> goodsForRequisitionJacksonList = requisitionJackson.getGoods();

        for (GoodsForRequisitionJackson item : goodsForRequisitionJacksonList) {
            int productId = item.getIdProduct();
            int amount = item.getAmount();

            for (int i = 0; i < amount; i++) {
                //SQL
                Product product = productCommandRepository.findByIdProduct(productId);
                if (product == null) {
                    return ResponseHolder.returnBadRequestAndMessage("Product doesnt exist!");
                }

                //kontrola zda existuje nejaky zbozi tohoto produktu, ktere neni prirazeno k zadne objednavce
                List<Goods> goodsList1 = goodsCommandRepository.findGoodsWithNoneRequisition(product);

                Goods newGoods = null;
                if (goodsList1.size() > 0) {
                    newGoods = goodsList1.get(0);
                    newGoods.setRequisitionReference(requisition);
                    goodsCommandRepository.delete(newGoods);
                } else {
                    newGoods = new Goods(
                            "inProgress",
                            null
                    );
                    newGoods.setProductReference(product);
                }
                goodsList.add(newGoods);

                //NOSQL
                ProductCollection productCollection = productQueryRepository.findById(productId);
                if (productCollection == null) {
                    return ResponseHolder.returnBadRequestAndMessage("Product doesnt exist!");
                }

                GoodsDocument newGoodsDocument = null;
                if (goodsList1.size() > 0) {
                    newGoodsDocument = new GoodsDocument(
                            newGoods.getSerialNumber(),
                            newGoods.getState(),
                            newGoods.getFabricationDate()
                    );
                } else {
                    newGoodsDocument = new GoodsDocument(
                            "inProgress",
                            null
                    );
                    newGoodsDocument.setSerialNumber(getSequence("goods_document_sequence"));

                    GoodsCollection newGoodsCollection = new GoodsCollection(
                            "inProgress",
                            null
                    );
                    newGoodsCollection.setProductReference(productCollection);
                    goodsCollectionsList.add(newGoodsCollection);
                }

                goodsDocumentList.add(newGoodsDocument);
            }
        }

        //Ulozit objednavku pro SQL
        requisition.setSubscriberReference(subscriberCommandRepository.findSubscriberByIdSubscriber(idSubscriber));
        RequisitionOracleSourceOfTruthSubscriber requisitionOracleSourceOfTruthSubscriber = new RequisitionOracleSourceOfTruthSubscriber(requisition, eventBusSql);
        final RequisitionCreatedEventSql createdEventSql = new RequisitionCreatedEventSql(requisitionCommandService);
        eventBusSql.post(createdEventSql);
        eventBusSql.unregister(requisitionOracleSourceOfTruthSubscriber);

        //Ulozit zbozi pro Sql na vsechny mista
        for (Goods actualGoods : goodsList) {
            actualGoods.setRequisitionReference(requisition);
            GoodsOracleSourceOfTruthSubscriber goodsOracleSourceOfTruthSubscriber = new GoodsOracleSourceOfTruthSubscriber(actualGoods, eventBusSql);
            final GoodsCreatedEventSql createdGoodsEventSql = new GoodsCreatedEventSql(goodsCommandService);
            eventBusSql.post(createdGoodsEventSql);
            eventBusSql.unregister(goodsOracleSourceOfTruthSubscriber);
        }


        //Ulozit zbozi pro NoSql na vsechny mista
        for (GoodsCollection actualGoodsCollection : goodsCollectionsList) {
            GoodsMorphiaReadCacheSubscriber goodsMorphiaReadCacheSubscriber = new GoodsMorphiaReadCacheSubscriber(actualGoodsCollection, eventBusNoSql);
            final GoodsCreatedEventNoSql createdEventNoSql = new GoodsCreatedEventNoSql(goodsCommandService);
            eventBusNoSql.post(createdEventNoSql);
            eventBusNoSql.unregister(goodsMorphiaReadCacheSubscriber);
        }

        //Ulozit objednavku pro NoSql
        requisitionDocument.setId(getSequence("requisition_document_sequence"));
        requisitionDocument.setGoods(goodsDocumentList);
        RequisitionSystemCollection requisitionSystemCollection = requisitionSystemQueryRepository.findById(idSubscriber);

        List<RequisitionDocument> requisitionDocuments = requisitionSystemCollection.getRequisitions();
        if (requisitionDocuments == null) {
            requisitionDocuments = new ArrayList<>();
        }
        requisitionDocuments.add(requisitionDocument);
        requisitionSystemCollection.setRequisitions(requisitionDocuments);


        RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(requisitionSystemCollection, eventBusNoSql);
        final SubscriberCreatedEventNoSql createdEventNoSql = new SubscriberCreatedEventNoSql(subscriberCommandService);
        eventBusNoSql.post(createdEventNoSql);
        eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);


        return ResponseHolder.returnOkAndMessage("The requisition has been successfully created.");
    }


    //////////////////////////////////////////////////////
    // DELETE SUBSCRIBER
    //////////////////////////////////////////////////////

    public ResponseHolder removeRequisition(int requisitionId) {
        Requisition requisition = requisitionCommandRepository.findRequisitionByIdRequisition(requisitionId);

        List<Goods> goodsList = goodsCommandRepository.findAll();

        for (Goods actualGoods : goodsList) {
            if (actualGoods.getState().equals("dispatched") && actualGoods.getRequisitionReference() == requisition) {
                return ResponseHolder.returnBadRequestAndMessage("The order cannot be canceled, the goods have already been dispatched.");
            }
        }

        for (Goods actualGoods : goodsList) {
            if (actualGoods.getRequisitionReference() == requisition) {
                actualGoods.setRequisitionReference(null);

                GoodsOracleSourceOfTruthSubscriber goodsOracleSourceOfTruthSubscriber = new GoodsOracleSourceOfTruthSubscriber(actualGoods, eventBusSql);
                final GoodsCreatedEventSql createdGoodsEventSql = new GoodsCreatedEventSql(goodsCommandService);
                eventBusSql.post(createdGoodsEventSql);
                eventBusSql.unregister(goodsOracleSourceOfTruthSubscriber);
            }
        }

        RequisitionOracleSourceOfTruthSubscriber requisitionOracleSourceOfTruthSubscriber = new RequisitionOracleSourceOfTruthSubscriber(requisition, eventBusSql);
        final RequisitionRemovedEventSql removeEventSql = new RequisitionRemovedEventSql(requisitionCommandService);
        eventBusSql.post(removeEventSql);
        eventBusSql.unregister(requisitionOracleSourceOfTruthSubscriber);


        List<RequisitionSystemCollection> requisitionSystemCollectionList = requisitionSystemQueryRepository.findAll();

        for (RequisitionSystemCollection actualRequisitionSystemCollectione : requisitionSystemCollectionList) {
            RequisitionDocument requisitionDocumentForRemove = null;
            List<RequisitionDocument> requisitionDocumentList = actualRequisitionSystemCollectione.getRequisitions();
            if (requisitionDocumentList != null) {
                for (RequisitionDocument requisitionDocument : requisitionDocumentList) {
                    if (requisitionDocument.getId() == requisitionId) {
                        requisitionDocumentForRemove = requisitionDocument;
                    }
                }
                if (requisitionDocumentForRemove != null) {
                    List<RequisitionDocument> requisitionDocuments = actualRequisitionSystemCollectione.getRequisitions();
                    requisitionDocuments.remove(requisitionDocumentForRemove);
                    actualRequisitionSystemCollectione.setRequisitions(requisitionDocuments);


                    RequisitionMorphiaReadCacheSubscriber requisitionMorphiaReadCacheSubscriber = new RequisitionMorphiaReadCacheSubscriber(actualRequisitionSystemCollectione, eventBusNoSql);
                    final SubscriberCreatedEventNoSql createdEventNoSql = new SubscriberCreatedEventNoSql(subscriberCommandService);
                    eventBusNoSql.post(createdEventNoSql);
                    eventBusNoSql.unregister(requisitionMorphiaReadCacheSubscriber);
                }
            }
        }


        return ResponseHolder.returnOkAndMessage("The requisition has been successfully deleted.");

    }


    ///////////////////////////////////////////////////////
    // METHODS FOR REPOSITORIES
    //////////////////////////////////////////////////////
    public Requisition saveRequisitionSql(Requisition requisition) {
        return requisitionCommandRepository.save(requisition);
    }

    public void deleteRequisitionSql(Requisition requisition) {
        requisitionCommandRepository.delete(requisition);
    }

}
