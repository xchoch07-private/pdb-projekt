package com.pdb.pdbprojekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdbprojektApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdbprojektApplication.class, args);
	}

}
