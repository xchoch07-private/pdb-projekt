package com.pdb.pdbprojekt.tests;

import com.pdb.pdbprojekt.commands.services.*;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.models.nosql.documents.TruckDocument;
import com.pdb.pdbprojekt.models.sql.Truck;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.RequisitionCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.TruckCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class TruckServiceTest {

    @Autowired
    TruckCommandService truckCommandService;

    @Autowired
    StorageCommandService storageCommandService;

    @Autowired
    ProductCommandService productCommandService;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @Autowired
    TruckCommandRepository truckCommandRepository;

    @Autowired
    RequisitionCommandRepository requisitionCommandRepository;

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    String truck = "{\n" +
            "    \"capacity\": 199 \n" +
            "}";

    String truckUpdated = "{\n" +
            "    \"capacity\": 999 \n" +
            "}";

    String storage = "{\n" +
            "    \"capacity\": 200,\n" +
            "    \"name\": \"PrvniSklad\"\n" +
            "}";

    String product1 = "{\n" +
            "    \"name\": \"produkt1\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String product2 = "{\n" +
            "    \"name\": \"produkt2\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String subscriber = "{\n" +
            "    \"name\": \"firma\",\n" +
            "    \"firstName\": \"Tomas\",\n" +
            "    \"lastName\": \"Chocholaty\",\n" +
            "    \"address\": \"adresa\",\n" +
            "    \"ico\": 2000,\n" +
            "    \"accountNumber\": 2134325\n" +
            "}";

    String requisition = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"none\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 5\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 10\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    String requisition2 = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"NoteOne\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 8\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 9\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    String requisitionsOnTruck = "{\n" +
            "    \"requisitionsIds\": [1, 2]\n" +
            "}";

    @Test
    public void test_createTruck() {
        truckCommandService.addNewTruck(truck);

        int createdTruckId = truckCommandRepository.findAll().get(0).getIdTruck();
        Optional<Truck> truckFromSql = truckCommandRepository.findById(createdTruckId);
        assertTrue(truckFromSql.isPresent());
        assertEquals(createdTruckId, truckFromSql.get().getIdTruck());
        assertEquals(199, truckFromSql.get().getCapacity());
    }

    @Test
    public void test_updateTruck() {
        truckCommandService.addNewTruck(truck);
        int createdTruckId = truckCommandRepository.findAll().get(0).getIdTruck();
        truckCommandService.updateTruck(createdTruckId, truckUpdated);

        Optional<Truck> truckFromSql = truckCommandRepository.findById(createdTruckId);

        assertTrue(truckFromSql.isPresent());
        assertEquals(createdTruckId, truckFromSql.get().getIdTruck());
        assertEquals(999, truckFromSql.get().getCapacity());
    }

    @Test
    public void test_addRequisitionsToTruck() {
        truckCommandService.addNewTruck(truck);
        int createdTruckId = truckCommandRepository.findAll().get(0).getIdTruck();
        storageCommandService.addNewStorage(storage);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        subscriberCommandService.addNewSubscriber(subscriber);
        requisitionCommandService.addRequisition(1, requisition);
        requisitionCommandService.addRequisition(1, requisition2);
        int createdRequisitionId = requisitionCommandRepository.findAll().get(0).getIdRequisition();
        truckCommandService.addRequisitionToTruck(1, requisitionsOnTruck);

        Optional<Truck> truck = truckCommandRepository.findById(createdTruckId);
        Optional<RequisitionSystemCollection> requisitionSystemCollection = Optional.ofNullable(requisitionSystemQueryRepository.findById(createdRequisitionId));

        assertTrue(truck.isPresent());
        assertTrue(requisitionSystemCollection.isPresent());

        assertEquals(2, truck.get().getRequisitions().size());

        for(RequisitionDocument tmp : requisitionSystemCollection.get().getRequisitions()) {
            for (TruckDocument truckDocument : tmp.getTrucks()) {
                assertEquals(createdTruckId, truckDocument.getId());
            }
        }
    }
}
