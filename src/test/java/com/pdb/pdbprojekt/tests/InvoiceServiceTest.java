package com.pdb.pdbprojekt.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pdb.pdbprojekt.commands.services.*;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.FinanceCollection;
import com.pdb.pdbprojekt.models.sql.Invoice;
import com.pdb.pdbprojekt.quieries.services.FinanceQueryService;
import com.pdb.pdbprojekt.repositories.nosql.FinanceQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.InvoiceCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class InvoiceServiceTest {

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    ProductCommandService productCommandService;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @Autowired
    GoodsCommandService goodsCommandService;

    @Autowired
    InvoiceCommandService invoiceCommandService;

    @Autowired
    FinanceQueryService financeQueryService;

    @Autowired
    InvoiceCommandRepository invoiceCommandRepository;

    @Autowired
    FinanceQueryRepository financeQueryRepository;

    String product = "{\n" +
            "    \"name\": \"Product1\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String product2 = "{\n" +
            "    \"name\": \"Product2\",\n" +
            "    \"price\": 201,\n" +
            "    \"dimension\": 16,\n" +
            "    \"productCode\": 12346\n" +
            "}";

    String subscriber = "{\n" +
            "    \"name\": \"firma\",\n" +
            "    \"firstName\": \"Tomas\",\n" +
            "    \"lastName\": \"Chocholaty\",\n" +
            "    \"address\": \"adresa\",\n" +
            "    \"ico\": 2000,\n" +
            "    \"accountNumber\": 2134325\n" +
            "}";

    String requisition = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"none\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 5\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 10\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    String changeStateOfGoods = "{\n" +
            "    \"goodsIds\": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],\n" +
            "    \"state\": \"dispatched\"\n" +
            "}\n";

    String invoice = "{\n" +
            "    \"exposeDate\": \"January 2, 2010\",\n" +
            "    \"maturityDate\": \"January 3, 2010\",\n" +
            "    \"variableSymbol\": 12345,\n" +
            "    \"goods\": [\n" +
            "        1,2,3,4,5\n" +
            "    ]\n" +
            "}";

    String salesForPeriod = "{\n" +
            "    \"from\": \"January 1, 2006\",\n" +
            "    \"to\": \"January 1, 2015\"\n" +
            "}";

    String getFinancesExpectedPattern = "[ {\r\n" +
            "  \"id\" : 1,\r\n" +
            "  \"supplier\" : \"firma\",\r\n" +
            "  \"exposeDate\" : 1262386800000,\r\n" +
            "  \"maturityDate\" : 1262473200000,\r\n" +
            "  \"variableSymbol\" : 12345,\r\n" +
            "  \"price\" : 1000.0,\r\n" +
            "  \"goods\" : [ {\r\n" +
            "    \"serialNumber\" : 1,\r\n" +
            "    \"state\" : \"dispatched\",\r\n" +
            "    \"fabricationDate\" : null\r\n" +
            "  }, {\r\n" +
            "    \"serialNumber\" : 2,\r\n" +
            "    \"state\" : \"dispatched\",\r\n" +
            "    \"fabricationDate\" : null\r\n" +
            "  }, {\r\n" +
            "    \"serialNumber\" : 3,\r\n" +
            "    \"state\" : \"dispatched\",\r\n" +
            "    \"fabricationDate\" : null\r\n" +
            "  }, {\r\n" +
            "    \"serialNumber\" : 4,\r\n" +
            "    \"state\" : \"dispatched\",\r\n" +
            "    \"fabricationDate\" : null\r\n" +
            "  }, {\r\n" +
            "    \"serialNumber\" : 5,\r\n" +
            "    \"state\" : \"dispatched\",\r\n" +
            "    \"fabricationDate\" : null\r\n" +
            "  } ],\r\n" +
            "  \"subscriberDocument\" : {\r\n" +
            "    \"idSubscriber\" : 1,\r\n" +
            "    \"name\" : \"firma\",\r\n" +
            "    \"firstName\" : \"Tomas\",\r\n" +
            "    \"lastName\" : \"Chocholaty\",\r\n" +
            "    \"address\" : \"adresa\",\r\n" +
            "    \"ico\" : 2000,\r\n" +
            "    \"accountNumber\" : \"2134325\"\r\n" +
            "  },\r\n" +
            "  \"version\" : null\r\n" +
            "} ]";

    @Test
    public void test_addInvoice() {
        subscriberCommandService.addNewSubscriber(subscriber);
        productCommandService.addProduct(product);
        productCommandService.addProduct(product2);
        requisitionCommandService.addRequisition(1, requisition);
        goodsCommandService.changeStateForGoods(changeStateOfGoods);
        invoiceCommandService.addNewInvoice(1, invoice);

        int createdInvoiceId = invoiceCommandRepository.findAll().get(0).getIdInvoice();
        Optional<Invoice> invoiceFromSql = invoiceCommandRepository.findById(createdInvoiceId);
        Optional<FinanceCollection> invoiceFromNoSql = financeQueryRepository.findById(createdInvoiceId);

        assertTrue(invoiceFromSql.isPresent());
        assertTrue(invoiceFromNoSql.isPresent());

        assertEquals(createdInvoiceId,invoiceFromSql.get().getIdInvoice());
        assertEquals("2010-01-02 00:00:00.0",invoiceFromSql.get().getExposeDate().toString());
        assertEquals("2010-01-03 00:00:00.0",invoiceFromSql.get().getMaturityDate().toString());
        assertEquals(12345,invoiceFromSql.get().getVariableSymbol());
        assertEquals(1000, invoiceFromSql.get().getPrice(), 0.0);
        assertEquals(1,invoiceFromSql.get().getSubscriberReference().getIdSubscriber());

        assertEquals(createdInvoiceId,invoiceFromNoSql.get().getId());
        assertEquals("Sat Jan 02 00:00:00 CET 2010",invoiceFromNoSql.get().getExposeDate().toString());
        assertEquals("Sun Jan 03 00:00:00 CET 2010",invoiceFromNoSql.get().getMaturityDate().toString());
        assertEquals(12345,invoiceFromNoSql.get().getVariableSymbol(), 0.0);
        assertEquals(1000, invoiceFromNoSql.get().getPrice(), 0.0);
    }

    @Test
    public void test_salesForPeriod() {
        subscriberCommandService.addNewSubscriber(subscriber);
        productCommandService.addProduct(product);
        productCommandService.addProduct(product2);
        requisitionCommandService.addRequisition(1, requisition);
        goodsCommandService.changeStateForGoods(changeStateOfGoods);
        invoiceCommandService.addNewInvoice(1, invoice);

        ResponseHolder salesForPeriodResponse = financeQueryService.getSalesForPeriod(salesForPeriod);


        String response = "";
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            response = ow.writeValueAsString(salesForPeriodResponse.getResponseBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assertEquals(200, salesForPeriodResponse.getReturnCode());
        assertEquals("1000.0", response);

    }

    @Test
    public void test_getFinances() {
        subscriberCommandService.addNewSubscriber(subscriber);
        productCommandService.addProduct(product);
        productCommandService.addProduct(product2);
        requisitionCommandService.addRequisition(1, requisition);
        goodsCommandService.changeStateForGoods(changeStateOfGoods);
        invoiceCommandService.addNewInvoice(1, invoice);

        ResponseHolder finances = financeQueryService.getFinances();

        String response = "";
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            response = ow.writeValueAsString(finances.getResponseBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assertEquals(200, finances.getReturnCode());
        assertEquals(getFinancesExpectedPattern, response);
    }
}
