package com.pdb.pdbprojekt.tests;

import com.pdb.pdbprojekt.commands.services.ProductCommandService;
import com.pdb.pdbprojekt.commands.services.RequisitionCommandService;
import com.pdb.pdbprojekt.commands.services.StorageCommandService;
import com.pdb.pdbprojekt.commands.services.SubscriberCommandService;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.nosql.documents.GoodsDocument;
import com.pdb.pdbprojekt.models.nosql.documents.RequisitionDocument;
import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.models.sql.Requisition;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.RequisitionCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.SubscriberCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class RequisitionServiceTest {


    @Autowired
    StorageCommandService storageCommandService;

    @Autowired
    ProductCommandService productCommandService;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @Autowired
    SubscriberCommandRepository subscriberCommandRepository;

    @Autowired
    RequisitionCommandRepository requisitionCommandRepository;

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    @Autowired
    GoodsCommandRepository goodsCommandRepository;

    String storage = "{\n" +
            "    \"capacity\": 200,\n" +
            "    \"name\": \"PrvniSklad\"\n" +
            "}";

    String product1 = "{\n" +
            "    \"name\": \"produkt1\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String product2 = "{\n" +
            "    \"name\": \"produkt2\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String subscriber = "{\n" +
            "    \"name\": \"firma\",\n" +
            "    \"firstName\": \"Tomas\",\n" +
            "    \"lastName\": \"Chocholaty\",\n" +
            "    \"address\": \"adresa\",\n" +
            "    \"ico\": 2000,\n" +
            "    \"accountNumber\": 2134325\n" +
            "}";

    String requisition = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"none\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 5\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 10\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    @Test
    public void test_createRequisition() {
        storageCommandService.addNewStorage(storage);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        subscriberCommandService.addNewSubscriber(subscriber);
        int subscriberId = subscriberCommandRepository.findAll().get(0).getIdSubscriber();
        requisitionCommandService.addRequisition(subscriberId, requisition);

        int requisitionId = requisitionCommandRepository.findAll().get(0).getIdRequisition();
        Optional<Requisition> requisitionSql = requisitionCommandRepository.findById(requisitionId);
        Optional<RequisitionSystemCollection> requisitionNoSql = Optional.ofNullable(requisitionSystemQueryRepository.findById(requisitionId));

        RequisitionDocument requisitionDocument = requisitionNoSql.get().getRequisitions().get(0);


        assertTrue(requisitionSql.isPresent());
        assertTrue(requisitionNoSql.isPresent());

        assertEquals(requisitionId, requisitionSql.get().getIdRequisition());
        assertEquals("inProgress", requisitionSql.get().getRequisitionState());
        assertEquals("none", requisitionSql.get().getNote());


        assertEquals(requisitionId, requisitionDocument.getId());
        assertEquals("inProgress", requisitionDocument.getRequisitionState());
        assertEquals("none", requisitionDocument.getNote());


        for (int i = 1; i < 16; i++) {
            Optional<Goods> goods = goodsCommandRepository.findById(i);
            assertTrue(goods.isPresent());
            assertEquals(i, goods.get().getSerialNumber());
            assertEquals("inProgress", goods.get().getState());
        }

        List<GoodsDocument> goodsList = requisitionDocument.getGoods();
        int i = 1;
        for (GoodsDocument actualGoodsDocument : goodsList) {
            assertEquals(i, actualGoodsDocument.getSerialNumber());
            assertEquals("inProgress", actualGoodsDocument.getState());
            i++;
        }
    }

    @Test
    public void test_removeRequisition() {
        storageCommandService.addNewStorage(storage);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        subscriberCommandService.addNewSubscriber(subscriber);
        int subscriberId = subscriberCommandRepository.findAll().get(0).getIdSubscriber();
        requisitionCommandService.addRequisition(subscriberId, requisition);

        int requisitionId = requisitionCommandRepository.findAll().get(0).getIdRequisition();

        requisitionCommandService.removeRequisition(requisitionId);

        Optional<Requisition> requisitionSql = requisitionCommandRepository.findById(requisitionId);
        Optional<RequisitionSystemCollection> requisitionNoSql = Optional.ofNullable(requisitionSystemQueryRepository.findById(requisitionId));
        List<RequisitionDocument> requisitionDocument = requisitionNoSql.get().getRequisitions();

        assertFalse(requisitionSql.isPresent());
        assertEquals(0, requisitionDocument.size());
    }

    @Test
    public void test_getRequisitions() {
        storageCommandService.addNewStorage(storage);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        subscriberCommandService.addNewSubscriber(subscriber);
        requisitionCommandService.addRequisition(1, requisition);

        List<Requisition> requisitionList = requisitionCommandRepository.findAll();
        List<RequisitionSystemCollection> requisitionSystemCollectionList = requisitionSystemQueryRepository.findAll();

        assertEquals(1, requisitionList.size());
        assertEquals(1, requisitionSystemCollectionList.size());

    }
}
