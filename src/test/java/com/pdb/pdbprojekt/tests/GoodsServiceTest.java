package com.pdb.pdbprojekt.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pdb.pdbprojekt.commands.services.*;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.quieries.services.GoodsQueryService;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class GoodsServiceTest {

    @Autowired
    StorageCommandService storageCommandService;

    @Autowired
    ProductCommandService productCommandService;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @Autowired
    GoodsCommandService goodsCommandService;

    @Autowired
    RegalCommandService regalCommandService;

    @Autowired
    GoodsQueryService goodsQueryService;

    @Autowired
    GoodsCommandRepository goodsCommandRepository;

    @Autowired
    GoodsQueryRepository goodsQueryRepository;

    String storage = "{\n" +
            "    \"capacity\": 200,\n" +
            "    \"name\": \"PrvniSklad\"\n" +
            "}";

    String storage2 = "{\n" +
            "    \"capacity\": 2020,\n" +
            "    \"name\": \"Hall A\"\n" +
            "}";

    String product1 = "{\n" +
            "    \"name\": \"produkt1\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String product2 = "{\n" +
            "    \"name\": \"produkt2\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String regal = "{\n" +
            "    \"section\": 2,\n" +
            "    \"sector\": 50,\n" +
            "    \"position\": 100\n" +
            "}";

    String regal2 = "{\n" +
            "    \"section\": 29,\n" +
            "    \"sector\": 509,\n" +
            "    \"position\": 1009\n" +
            "}";

    String productToRegal = "{\n" +
            "    \"product\": 1\n" +
            "}";

    String goodsToRegal = "{\n" +
            "    \"goodsIds\": [1,2]\n" +
            "}";

    String goodsToRegal2 = "{\n" +
            "    \"goodsIds\": [3,4,5]\n" +
            "}";

    String subscriber = "{\n" +
            "    \"name\": \"firma\",\n" +
            "    \"firstName\": \"Tomas\",\n" +
            "    \"lastName\": \"Chocholaty\",\n" +
            "    \"address\": \"adresa\",\n" +
            "    \"ico\": 2000,\n" +
            "    \"accountNumber\": 2134325\n" +
            "}";

    String requisition = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"none\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 5\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 10\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    String requisition2 = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"NoteOne\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 2\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 3\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    String changeStateOfGoods = "{\n" +
            "    \"goodsIds\": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],\n" +
            "    \"state\": \"dispatched\"\n" +
            "}\n";


    String locationOfGoodsExpectedPattern = "{\r\n" +
            "  \"idStorage\" : 1,\r\n" +
            "  \"regalResponseList\" : [ {\r\n" +
            "    \"idRegal\" : 1,\r\n" +
            "    \"section\" : 2,\r\n" +
            "    \"sector\" : 50,\r\n" +
            "    \"position\" : 100\r\n" +
            "  } ]\r\n" +
            "}";

    String goodsWithoutRequisitionExpectedPattern = "[ {\r\n" +
            "  \"id\" : 1,\r\n" +
            "  \"state\" : \"inProgress\",\r\n" +
            "  \"fabricationDate\" : null,\r\n" +
            "  \"productReference\" : {\r\n" +
            "    \"id\" : 1,\r\n" +
            "    \"name\" : \"produkt1\",\r\n" +
            "    \"price\" : 200.0,\r\n" +
            "    \"dimension\" : 15,\r\n" +
            "    \"productCode\" : 12345,\r\n" +
            "    \"version\" : null\r\n" +
            "  },\r\n" +
            "  \"version\" : null\r\n" +
            "}, {\r\n" +
            "  \"id\" : 2,\r\n" +
            "  \"state\" : \"inProgress\",\r\n" +
            "  \"fabricationDate\" : null,\r\n" +
            "  \"productReference\" : {\r\n" +
            "    \"id\" : 1,\r\n" +
            "    \"name\" : \"produkt1\",\r\n" +
            "    \"price\" : 200.0,\r\n" +
            "    \"dimension\" : 15,\r\n" +
            "    \"productCode\" : 12345,\r\n" +
            "    \"version\" : null\r\n" +
            "  },\r\n" +
            "  \"version\" : null\r\n" +
            "}, {\r\n" +
            "  \"id\" : 3,\r\n" +
            "  \"state\" : \"inProgress\",\r\n" +
            "  \"fabricationDate\" : null,\r\n" +
            "  \"productReference\" : {\r\n" +
            "    \"id\" : 2,\r\n" +
            "    \"name\" : \"produkt2\",\r\n" +
            "    \"price\" : 200.0,\r\n" +
            "    \"dimension\" : 15,\r\n" +
            "    \"productCode\" : 12345,\r\n" +
            "    \"version\" : null\r\n" +
            "  },\r\n" +
            "  \"version\" : null\r\n" +
            "}, {\r\n" +
            "  \"id\" : 4,\r\n" +
            "  \"state\" : \"inProgress\",\r\n" +
            "  \"fabricationDate\" : null,\r\n" +
            "  \"productReference\" : {\r\n" +
            "    \"id\" : 2,\r\n" +
            "    \"name\" : \"produkt2\",\r\n" +
            "    \"price\" : 200.0,\r\n" +
            "    \"dimension\" : 15,\r\n" +
            "    \"productCode\" : 12345,\r\n" +
            "    \"version\" : null\r\n" +
            "  },\r\n" +
            "  \"version\" : null\r\n" +
            "}, {\r\n" +
            "  \"id\" : 5,\r\n" +
            "  \"state\" : \"inProgress\",\r\n" +
            "  \"fabricationDate\" : null,\r\n" +
            "  \"productReference\" : {\r\n" +
            "    \"id\" : 2,\r\n" +
            "    \"name\" : \"produkt2\",\r\n" +
            "    \"price\" : 200.0,\r\n" +
            "    \"dimension\" : 15,\r\n" +
            "    \"productCode\" : 12345,\r\n" +
            "    \"version\" : null\r\n" +
            "  },\r\n" +
            "  \"version\" : null\r\n" +
            "} ]";

    @Test
    public void test_changeStateForGoods() {
        subscriberCommandService.addNewSubscriber(subscriber);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        requisitionCommandService.addRequisition(1, requisition);
        goodsCommandService.changeStateForGoods(changeStateOfGoods);

        List<Goods> goodsList = goodsCommandRepository.findAll();
        List<GoodsCollection> goodsCollectionList = goodsQueryRepository.findAll();

        for (Goods goods : goodsList) {
            assertEquals("dispatched", goods.getState());
        }

        for (GoodsCollection goodsCollection : goodsCollectionList) {
            assertEquals("dispatched", goodsCollection.getState());
        }

    }

    @Test
    public void test_getGoodsWithoutRequisition() {
        subscriberCommandService.addNewSubscriber(subscriber);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        requisitionCommandService.addRequisition(1, requisition2);
        requisitionCommandService.removeRequisition(1);

       ResponseHolder goodsWithoutRequisition = goodsQueryService.getGoodsWithoutRequisition();

        String response = "";
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            response = ow.writeValueAsString(goodsWithoutRequisition.getResponseBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assertEquals(200, goodsWithoutRequisition.getReturnCode());
        assertEquals(goodsWithoutRequisitionExpectedPattern, response);
    }

    @Test
    public void test_getPositionOfGoods() {
        storageCommandService.addNewStorage(storage);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        regalCommandService.addNewRegal(1, regal);
        subscriberCommandService.addNewSubscriber(subscriber);
        requisitionCommandService.addRequisition(1, requisition);
        regalCommandService.addNewProductsToRegal(1, productToRegal);
        regalCommandService.addNewGoodsToRegal(1, goodsToRegal);


        ResponseHolder locationOfProductResponse = goodsQueryService.getPositionOfGoods(2);

        String response = "";
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            response = ow.writeValueAsString(locationOfProductResponse.getResponseBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assertEquals(200, locationOfProductResponse.getReturnCode());
        assertEquals(locationOfGoodsExpectedPattern, response);
    }

    @Test
    public void test_getGoods() {
        storageCommandService.addNewStorage(storage);
        productCommandService.addProduct(product1);
        productCommandService.addProduct(product2);
        subscriberCommandService.addNewSubscriber(subscriber);
        requisitionCommandService.addRequisition(1, requisition);

        List<GoodsCollection> goodsCollectionList = goodsQueryRepository.findAll();
        assertEquals(15, goodsCollectionList.size());

        requisitionCommandService.addRequisition(1, requisition2);
        List<GoodsCollection> goodsCollectionList1 = goodsQueryRepository.findAll();

        assertEquals(20, goodsCollectionList1.size());
    }
}
