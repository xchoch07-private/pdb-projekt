package com.pdb.pdbprojekt.tests;

import com.pdb.pdbprojekt.commands.services.*;
import com.pdb.pdbprojekt.models.nosql.GoodsCollection;
import com.pdb.pdbprojekt.models.nosql.RegalCollection;
import com.pdb.pdbprojekt.models.sql.Goods;
import com.pdb.pdbprojekt.models.sql.Regal;
import com.pdb.pdbprojekt.repositories.nosql.GoodsQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.StorageQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.GoodsCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.RegalCommandRepository;
import com.pdb.pdbprojekt.repositories.sql.StorageCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class RegalServiceTest {

    @Autowired
    StorageCommandService storageCommandService;

    @Autowired
    RegalCommandService regalCommandService;

    @Autowired
    ProductCommandService productCommandService;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @Autowired
    GoodsCommandService goodsCommandService;

    @Autowired
    StorageCommandRepository storageCommandRepository;

    @Autowired
    StorageQueryRepository storageQueryRepository;

    @Autowired
    RegalCommandRepository regalCommandRepository;

    @Autowired
    RegalQueryRepository regalQueryRepository;

    @Autowired
    GoodsCommandRepository goodsCommandRepository;

    @Autowired
    GoodsQueryRepository goodsQueryRepository;


    String storage = "{\n" +
            "    \"capacity\": 200,\n" +
            "    \"name\": \"PrvniSklad\"\n" +
            "}";

    String regal = "{\n" +
            "    \"section\": 2,\n" +
            "    \"sector\": 50,\n" +
            "    \"position\": 100\n" +
            "}";

    String regalUpdated = "{\n" +
            "    \"section\": 9,\n" +
            "    \"sector\": 50,\n" +
            "    \"position\": 999\n" +
            "}";

    String regalUpdated2 = "{\n" +
            "    \"section\": 123,\n" +
            "    \"sector\": 50,\n" +
            "    \"position\": 999\n" +
            "}";

    String product = "{\n" +
            "    \"name\": \"Product1\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String product2 = "{\n" +
            "    \"name\": \"Product2\",\n" +
            "    \"price\": 2000,\n" +
            "    \"dimension\": 150,\n" +
            "    \"productCode\": 123453\n" +
            "}";

    String productToRegal = "{\n" +
            "    \"product\": 1\n" +
            "}";

    String goodsToRegal = "{\n" +
            "    \"goodsIds\": [1,2,3,5]\n" +
            "}";

    String subscriber = "{\n" +
            "    \"name\": \"testName\",\n" +
            "    \"firstName\": \"testFirstName\",\n" +
            "    \"lastName\": \"testLastName\",\n" +
            "    \"address\": \"testAddress\",\n" +
            "    \"ico\": 2000,\n" +
            "    \"accountNumber\": 123\n" +
            "}";

    String requisition = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"none\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 5\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 10\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    @Test
    public void test_createRegal() {
        storageCommandService.addNewStorage(storage);
        int idStorage = storageCommandRepository.findAll().get(0).getIdStorage();

        regalCommandService.addNewRegal(idStorage, regal);

        int idCreatedRegal = regalCommandRepository.findAll().get(0).getIdRegal();
        Optional<Regal> regalFromSql = regalCommandRepository.findById(idCreatedRegal);
        Optional<RegalCollection> regalFromNoSql = regalQueryRepository.findById(idCreatedRegal);
        assertTrue(regalFromSql.isPresent());
        assertTrue(regalFromNoSql.isPresent());
        assertEquals(idCreatedRegal, regalFromSql.get().getIdRegal());
        assertEquals(2, regalFromSql.get().getSection());
        assertEquals(50, regalFromSql.get().getSector());
        assertEquals(100, regalFromSql.get().getPosition());
        assertEquals(idStorage, regalFromSql.get().getStorageReference().getIdStorage());
        assertEquals("PrvniSklad", regalFromSql.get().getStorageReference().getName());
        assertEquals(regalFromNoSql.get(), new RegalCollection(idCreatedRegal, regalFromSql.get().getSection(),
                regalFromSql.get().getSector(), regalFromSql.get().getPosition(),
                storageQueryRepository.findStorageCollectionById(idStorage), null, null));

    }

    @Test
    public void test_updateRegal() {
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(storageCommandRepository.findAll().get(0).getIdStorage(), regal);

        int idCreatedRegal = regalCommandRepository.findAll().get(0).getIdRegal();
        regalCommandService.updateRegal(idCreatedRegal, regalUpdated);
        Optional<Regal> regalFromSql = regalCommandRepository.findById(idCreatedRegal);
        Optional<RegalCollection> regalFromNoSql = regalQueryRepository.findById(idCreatedRegal);
        assertTrue(regalFromSql.isPresent());
        assertTrue(regalFromNoSql.isPresent());
        assertEquals(idCreatedRegal, regalFromSql.get().getIdRegal());
        assertEquals(9, regalFromSql.get().getSection());
        assertEquals(50, regalFromSql.get().getSector());
        assertEquals(999, regalFromSql.get().getPosition());
        assertNotEquals(regalFromNoSql.get(), new RegalCollection(idCreatedRegal, regalFromSql.get().getSection(),
                regalFromSql.get().getSector(), regalFromSql.get().getPosition(),
                storageQueryRepository.findStorageCollectionById(storageCommandRepository.findAll().get(0).getIdStorage()),
                null, null));
    }

    @Test
    public void test_checkVersionUpdatedRegalCollection() {
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(storageCommandRepository.findAll().get(0).getIdStorage(), regal);

        int idCreatedRegal = regalCommandRepository.findAll().get(0).getIdRegal();
        regalCommandService.updateRegal(idCreatedRegal, regalUpdated);
        Optional<RegalCollection> regalFromNoSql = regalQueryRepository.findById(idCreatedRegal);
        assertTrue(regalFromNoSql.isPresent());
        assertEquals(Integer.valueOf(2), regalFromNoSql.get().getVersion());

        regalCommandService.updateRegal(idCreatedRegal, regalUpdated2);
        assertEquals(Integer.valueOf(3), regalQueryRepository.findById(idCreatedRegal).get().getVersion());
    }

    @Test
    public void test_deleteRegal() {
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(storageCommandRepository.findAll().get(0).getIdStorage(), regal);
        int idCreatedRegal = regalCommandRepository.findAll().get(0).getIdRegal();
        regalCommandService.deleteRegal(idCreatedRegal);

        Optional<Regal> regalFromSql = regalCommandRepository.findById(idCreatedRegal);
        Optional<RegalCollection> regalFromNoSql = regalQueryRepository.findById(idCreatedRegal);
        assertFalse(regalFromSql.isPresent());
        assertFalse(regalFromNoSql.isPresent());
    }

    @Test
    public void test_addProductToRegal() {
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(storageCommandRepository.findAll().get(0).getIdStorage(), regal);
        int idCreatedRegal = regalCommandRepository.findAll().get(0).getIdRegal();
        productCommandService.addProduct(product);
        regalCommandService.addNewProductsToRegal(idCreatedRegal, productToRegal);

        Optional<Regal> regal = regalCommandRepository.findById(idCreatedRegal);
        Optional<RegalCollection> regalCollection = regalQueryRepository.findById(idCreatedRegal);

        assertTrue(regal.isPresent());
        assertTrue(regalCollection.isPresent());

        assertEquals(1, regal.get().getProductReference().getIdProduct());
        assertEquals("Product1", regal.get().getProductReference().getName());
        assertEquals(200.0, regal.get().getProductReference().getPrice(), 0.0);
        assertEquals(15, regal.get().getProductReference().getDimension());
        assertEquals(12345, regal.get().getProductReference().getProductCode());

        assertEquals(1, regalCollection.get().getProductReference().getIdProduct());
        assertEquals("Product1", regalCollection.get().getProductReference().getName());
        assertEquals(200.0, regalCollection.get().getProductReference().getPrice(), 0.0);
        assertEquals(15, regalCollection.get().getProductReference().getDimension());
        assertEquals(12345, regalCollection.get().getProductReference().getProductCode());
        assertEquals(Integer.valueOf(2), regalCollection.get().getVersion());
    }

    @Test
    public void test_addGoodsToRegal() {
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(1, regal);
        productCommandService.addProduct(product);
        productCommandService.addProduct(product2);
        subscriberCommandService.addNewSubscriber(subscriber);
        requisitionCommandService.addRequisition(1, requisition);
        regalCommandService.addNewProductsToRegal(1, productToRegal);
        regalCommandService.addNewGoodsToRegal(1, goodsToRegal);

        int countOfGoodsInRegalCollection = regalQueryRepository.findRegalCollectionById(1).getGoods().size();
        int countOfGoodsInRegal = regalCommandRepository.findRegalByIdRegal(1).getGoods().size();

        List<Goods> goodsList = goodsCommandRepository.findAll();
        int goodsWithStateInRegal = 0;
        for (Goods goods : goodsList) {
            if (goods.getState().equals("in-regal")) {
                goodsWithStateInRegal++;
            }
        }
        List<GoodsCollection> goodsCollectionList = goodsQueryRepository.findAll();
        int goodsCollectionStateInRegal = 0;
        for (GoodsCollection goodsCollection : goodsCollectionList) {
            if (goodsCollection.getState().equals("in-regal")) {
                goodsCollectionStateInRegal++;
            }
        }

        assertEquals(4, countOfGoodsInRegal);
        assertEquals(4, countOfGoodsInRegalCollection);

        assertFalse(goodsList.isEmpty());
        assertFalse(goodsCollectionList.isEmpty());
        assertEquals(countOfGoodsInRegal, goodsWithStateInRegal);
        assertEquals(countOfGoodsInRegalCollection, goodsCollectionStateInRegal);
        assertEquals(goodsWithStateInRegal, goodsCollectionStateInRegal);
    }

    @Test
    public void test_getRegals() {
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(storageCommandRepository.findAll().get(0).getIdStorage(), regal);
        regalCommandService.addNewRegal(storageCommandRepository.findAll().get(0).getIdStorage(), regalUpdated);
        regalCommandService.addNewRegal(storageCommandRepository.findAll().get(0).getIdStorage(), regalUpdated2);

        List<Regal> regalList = regalCommandRepository.findAll();
        List<RegalCollection> regalCollectionList = regalQueryRepository.findAll();

        assertEquals(3, regalList.size());
        assertEquals(3, regalCollectionList.size());
    }
}
