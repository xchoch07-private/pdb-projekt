package com.pdb.pdbprojekt.tests;

import com.pdb.pdbprojekt.commands.services.SubscriberCommandService;
import com.pdb.pdbprojekt.models.nosql.RequisitionSystemCollection;
import com.pdb.pdbprojekt.models.sql.Subscriber;
import com.pdb.pdbprojekt.repositories.nosql.RequisitionSystemQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.SubscriberCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SubscriberServiceTest {

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    SubscriberCommandRepository subscriberCommandRepository;

    @Autowired
    RequisitionSystemQueryRepository requisitionSystemQueryRepository;

    String subscriberString = "{\n" +
            "    \"name\": \"testName\",\n" +
            "    \"firstName\": \"testFirstName\",\n" +
            "    \"lastName\": \"testLastName\",\n" +
            "    \"address\": \"testAddress\",\n" +
            "    \"ico\": 2000,\n" +
            "    \"accountNumber\": 123\n" +
            "}";

    String subscriberString2 = "{\n" +
            "    \"name\": \"testName2\",\n" +
            "    \"firstName\": \"testFirstName2\",\n" +
            "    \"lastName\": \"testLastName2\",\n" +
            "    \"address\": \"testAddress2\",\n" +
            "    \"ico\": 2001,\n" +
            "    \"accountNumber\": 124\n" +
            "}";

    @Test
    public void test_createSubscriber() {
        subscriberCommandService.addNewSubscriber(subscriberString);
        int createdSubscriberId = subscriberCommandRepository.findAll().get(0).getIdSubscriber();
        Optional<Subscriber> subscriberFromSql = subscriberCommandRepository.findById(createdSubscriberId);
        Optional<RequisitionSystemCollection> subscriberFromNoSql = Optional.ofNullable(requisitionSystemQueryRepository.findById(createdSubscriberId));

        assertTrue(subscriberFromSql.isPresent());
        assertTrue(subscriberFromNoSql.isPresent());

        assertEquals(createdSubscriberId, subscriberFromSql.get().getIdSubscriber());
        assertEquals("testName", subscriberFromSql.get().getName());
        assertEquals("testFirstName", subscriberFromSql.get().getFirstName());
        assertEquals("testLastName", subscriberFromSql.get().getLastName());
        assertEquals("testAddress", subscriberFromSql.get().getAddress());
        assertEquals(2000, subscriberFromSql.get().getIco());
        assertEquals("123", subscriberFromSql.get().getAccountNumber());

        assertEquals(subscriberFromNoSql.get(), new RequisitionSystemCollection(1, "testName", "testFirstName", "testLastName", "testAddress", 2000, "123"));
    }

    @Test
    public void test_updateSubscriber() {
        subscriberCommandService.addNewSubscriber(subscriberString);
        subscriberCommandService.updateSubscriber(1, subscriberString2);

        subscriberCommandService.addNewSubscriber(subscriberString);
        int createdSubscriberId = subscriberCommandRepository.findAll().get(0).getIdSubscriber();
        Optional<Subscriber> subscriberFromSql = subscriberCommandRepository.findById(createdSubscriberId);
        Optional<RequisitionSystemCollection> subscriberFromNoSql = Optional.ofNullable(requisitionSystemQueryRepository.findById(createdSubscriberId));

        assertTrue(subscriberFromSql.isPresent());
        assertTrue(subscriberFromNoSql.isPresent());

        assertEquals(createdSubscriberId, subscriberFromSql.get().getIdSubscriber());
        assertEquals("testName2", subscriberFromSql.get().getName());
        assertEquals("testFirstName2", subscriberFromSql.get().getFirstName());
        assertEquals("testLastName2", subscriberFromSql.get().getLastName());
        assertEquals("testAddress2", subscriberFromSql.get().getAddress());
        assertEquals(2001, subscriberFromSql.get().getIco());
        assertEquals("124", subscriberFromSql.get().getAccountNumber());

        assertEquals(subscriberFromNoSql.get(), new RequisitionSystemCollection(1, "testName2", "testFirstName2", "testLastName2", "testAddress2", 2001, "124", 2));

    }

    @Test
    public void test_deleteSubscriber() {
        subscriberCommandService.addNewSubscriber(subscriberString);

        subscriberCommandService.addNewSubscriber(subscriberString);
        int createdSubscriberId = subscriberCommandRepository.findAll().get(0).getIdSubscriber();
        subscriberCommandService.deleteSubscriber(createdSubscriberId);

        Optional<Subscriber> subscriberFromSql = subscriberCommandRepository.findById(createdSubscriberId);
        Optional<RequisitionSystemCollection> subscriberFromNoSql = Optional.ofNullable(requisitionSystemQueryRepository.findById(createdSubscriberId));

        assertFalse(subscriberFromSql.isPresent());
        assertFalse(subscriberFromNoSql.isPresent());

    }
}
