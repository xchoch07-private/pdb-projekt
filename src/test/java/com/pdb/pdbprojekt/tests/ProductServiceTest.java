package com.pdb.pdbprojekt.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.pdb.pdbprojekt.commands.services.*;
import com.pdb.pdbprojekt.common.ResponseHolder;
import com.pdb.pdbprojekt.models.nosql.ProductCollection;
import com.pdb.pdbprojekt.models.nosql.documents.ProductDocument;
import com.pdb.pdbprojekt.models.sql.Product;
import com.pdb.pdbprojekt.quieries.services.ProductQueryService;
import com.pdb.pdbprojekt.repositories.nosql.ProductQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.RegalQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.ProductCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductServiceTest {

    @Autowired
    ProductCommandService productCommandService;

    @Autowired
    RegalCommandService regalCommandService;

    @Autowired
    StorageCommandService storageCommandService;

    @Autowired
    SubscriberCommandService subscriberCommandService;

    @Autowired
    RequisitionCommandService requisitionCommandService;

    @Autowired
    GoodsCommandService goodsCommandService;

    @Autowired
    InvoiceCommandService invoiceCommandService;

    @Autowired
    ProductQueryService productQueryService;

    @Autowired
    RegalQueryRepository regalQueryRepository;

    @Autowired
    ProductQueryRepository productQueryRepository;

    @Autowired
    ProductCommandRepository productCommandRepository;

    String product = "{\n" +
            "    \"name\": \"Product1\",\n" +
            "    \"price\": 200,\n" +
            "    \"dimension\": 15,\n" +
            "    \"productCode\": 12345\n" +
            "}";

    String product2 = "{\n" +
            "    \"name\": \"Product2\",\n" +
            "    \"price\": 201,\n" +
            "    \"dimension\": 16,\n" +
            "    \"productCode\": 12346\n" +
            "}";

    String storage = "{\n" +
            "    \"capacity\": 200,\n" +
            "    \"name\": \"DruhejSklad\"\n" +
            "}0";

    String storage2 = "{\n" +
            "    \"capacity\": 2020,\n" +
            "    \"name\": \"Hall A\"\n" +
            "}";

    String regal = "{\n" +
            "    \"section\": 2,\n" +
            "    \"sector\": 50,\n" +
            "    \"position\": 100\n" +
            "}";

    String regal2 = "{\n" +
            "    \"section\": 29,\n" +
            "    \"sector\": 509,\n" +
            "    \"position\": 1009\n" +
            "}";

    String productToRegal = "{\n" +
            "    \"product\": 1\n" +
            "}";

    String subscriber = "{\n" +
            "    \"name\": \"firma\",\n" +
            "    \"firstName\": \"Tomas\",\n" +
            "    \"lastName\": \"Chocholaty\",\n" +
            "    \"address\": \"adresa\",\n" +
            "    \"ico\": 2000,\n" +
            "    \"accountNumber\": 2134325\n" +
            "}";

    String requisition = "{\n" +
            "    \"requisitionState\": \"inProgress\",\n" +
            "    \"note\": \"none\",\n" +
            "    \"orderCreationDate\": \"January 2, 2010\",\n" +
            "    \"goods\": [\n" +
            "        {\n" +
            "            \"idProduct\": 1,\n" +
            "            \"amount\": 5\n" +
            "        },\n" +
            "        {\n" +
            "             \"idProduct\": 2,\n" +
            "            \"amount\": 10\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    String changeStateOfGoods = "{\n" +
            "    \"goodsIds\": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],\n" +
            "    \"state\": \"dispatched\"\n" +
            "}\n";

    String invoice = "{\n" +
            "    \"exposeDate\": \"January 2, 2010\",\n" +
            "    \"maturityDate\": \"January 3, 2010\",\n" +
            "    \"variableSymbol\": 12345,\n" +
            "    \"goods\": [\n" +
            "        1,2,3,4,5\n" +
            "    ]\n" +
            "}";

    String sellProductInPeriod = "{\n" +
            "    \"from\": \"January 1, 2006\",\n" +
            "    \"to\": \"January 1, 2015\"\n" +
            "}";


    String positionOfProductInStoragesExpectedPattern = "[ {\r\n" +
            "  \"idStorage\" : 1,\r\n" +
            "  \"regalResponseList\" : [ {\r\n" +
            "    \"idRegal\" : 1,\r\n" +
            "    \"section\" : 2,\r\n" +
            "    \"sector\" : 50,\r\n" +
            "    \"position\" : 100\r\n" +
            "  } ]\r\n" +
            "}, {\r\n" +
            "  \"idStorage\" : 2,\r\n" +
            "  \"regalResponseList\" : [ {\r\n" +
            "    \"idRegal\" : 2,\r\n" +
            "    \"section\" : 29,\r\n" +
            "    \"sector\" : 509,\r\n" +
            "    \"position\" : 1009\r\n" +
            "  } ]\r\n" +
            "} ]";

    @Test
    public void test_createProduct() {
        productCommandService.addProduct(product);

        int createdProductId = productCommandRepository.findAll().get(0).getIdProduct();
        Optional<Product> productFromSql = productCommandRepository.findById(createdProductId);
        Optional<ProductCollection> productFromNoSql = Optional.ofNullable(productQueryRepository.findById(createdProductId));
        assertTrue(productFromSql.isPresent());
        assertTrue(productFromNoSql.isPresent());
        assertEquals(createdProductId, productFromSql.get().getIdProduct());
        assertEquals("Product1", productFromSql.get().getName());
        assertEquals(200.0, productFromSql.get().getPrice(), 0.0);
        assertEquals(15, productFromSql.get().getDimension());
        assertEquals(12345, productFromSql.get().getProductCode());
        assertEquals(productFromNoSql.get(), new ProductCollection(createdProductId, productFromSql.get().getName(),
                productFromSql.get().getPrice(), productFromSql.get().getDimension(), productFromSql.get().getProductCode()));
    }

    @Test
    public void test_updateProduct() {
        productCommandService.addProduct(product);
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(1, regal);
        regalCommandService.addNewProductsToRegal(1, "{\n" +
                "\t\"product\": 1\n" +
                "}");

        int createdProductId = productCommandRepository.findAll().get(0).getIdProduct();
        productCommandService.updateProduct(createdProductId, product2);

        Optional<Product> productFromSql = productCommandRepository.findById(createdProductId);
        Optional<ProductCollection> productFromNoSql = Optional.ofNullable(productQueryRepository.findById(createdProductId));
        assertTrue(productFromSql.isPresent());
        assertTrue(productFromNoSql.isPresent());
        assertEquals(createdProductId, productFromSql.get().getIdProduct());
        assertEquals("Product2", productFromSql.get().getName());
        assertEquals(201.0, productFromSql.get().getPrice(), 0.0);
        assertEquals(16, productFromSql.get().getDimension());
        assertEquals(12346, productFromSql.get().getProductCode());
        assertEquals(productFromNoSql.get(), new ProductCollection(createdProductId, productFromSql.get().getName(),
                productFromSql.get().getPrice(), productFromSql.get().getDimension(), productFromSql.get().getProductCode(), 2));


        ProductDocument productDocument = regalQueryRepository.findRegalCollectionById(1).getProductReference();
        assertEquals(createdProductId, productDocument.getIdProduct());
        assertEquals("Product2", productDocument.getName());
        assertEquals(201.0, productDocument.getPrice(), 0.0);
        assertEquals(16, productDocument.getDimension());

    }

    @Test
    public void test_deleteProduct() {
        productCommandService.addProduct(product);
        storageCommandService.addNewStorage(storage);
        regalCommandService.addNewRegal(1, regal);
        regalCommandService.addNewProductsToRegal(1, "{\n" +
                "\t\"product\": 1\n" +
                "}");

        int createdProductId = productCommandRepository.findAll().get(0).getIdProduct();
        productCommandService.deleteProduct(createdProductId);

        Optional<Product> productFromSql = productCommandRepository.findById(createdProductId);
        Optional<ProductCollection> productFromNoSql = Optional.ofNullable(productQueryRepository.findById(createdProductId));
        ProductDocument productDocument = regalQueryRepository.findRegalCollectionById(1).getProductReference();

        assertFalse(productFromSql.isPresent());
        assertFalse(productFromNoSql.isPresent());
        assertNull(productDocument);
    }

    @Test
    public void test_getPositionOfProductInStorages() {
        storageCommandService.addNewStorage(storage);
        storageCommandService.addNewStorage(storage2);
        productCommandService.addProduct(product);
        productCommandService.addProduct(product2);
        regalCommandService.addNewRegal(1, regal);
        regalCommandService.addNewRegal(2, regal2);
        subscriberCommandService.addNewSubscriber(subscriber);
        requisitionCommandService.addRequisition(1, requisition);
        regalCommandService.addNewProductsToRegal(1, productToRegal);
        regalCommandService.addNewProductsToRegal(2, productToRegal);

        ResponseHolder locationOfProductResponse = productQueryService.getPositionOfProductInStorage(1);

        String response = "";
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            response = ow.writeValueAsString(locationOfProductResponse.getResponseBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assertEquals(200, locationOfProductResponse.getReturnCode());
        assertEquals(positionOfProductInStoragesExpectedPattern, response);
    }

    @Test
    public void test_getSellProductInPeriod() {
        subscriberCommandService.addNewSubscriber(subscriber);
        productCommandService.addProduct(product);
        productCommandService.addProduct(product2);
        requisitionCommandService.addRequisition(1, requisition);
        goodsCommandService.changeStateForGoods(changeStateOfGoods);
        invoiceCommandService.addNewInvoice(1, invoice);

       ResponseHolder sellProductInPeriodResponse = productQueryService.getSellProductInPeriod(1, sellProductInPeriod);

        String response = "";
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            response = ow.writeValueAsString(sellProductInPeriodResponse.getResponseBody());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        assertEquals(200, sellProductInPeriodResponse.getReturnCode());
        assertEquals("5", response);
    }

    @Test
    public void test_getProducts() {
        productCommandService.addProduct(product);
        productCommandService.addProduct(product2);

        List<Product> productList = productCommandRepository.findAll();
        List<ProductCollection> productCollectionList = productQueryRepository.findAll();

        assertEquals(2, productList.size());
        assertEquals(2, productCollectionList.size());
    }
}
