package com.pdb.pdbprojekt.tests;

import com.pdb.pdbprojekt.commands.services.RegalCommandService;
import com.pdb.pdbprojekt.commands.services.StorageCommandService;
import com.pdb.pdbprojekt.models.nosql.StorageCollection;
import com.pdb.pdbprojekt.models.nosql.sequences.SequenceGeneratorService;
import com.pdb.pdbprojekt.models.sql.Storage;
import com.pdb.pdbprojekt.repositories.nosql.SequenceQueryRepository;
import com.pdb.pdbprojekt.repositories.nosql.StorageQueryRepository;
import com.pdb.pdbprojekt.repositories.sql.StorageCommandRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class StorageServiceTest {

    @Autowired
    StorageCommandService storageCommandService;

    @Autowired
    StorageCommandRepository storageCommandRepository;

    @Autowired
    StorageQueryRepository storageQueryRepository;

    @Autowired
    SequenceQueryRepository sequenceQueryRepository;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    RegalCommandService regalCommandService;

    String storage = "{\n" +
            "    \"capacity\": 200,\n" +
            "    \"name\": \"PrvniSklad\"\n" +
            "}";

    String storage2 = "{\n" +
            "    \"capacity\": 2020,\n" +
            "    \"name\": \"Hall A\"\n" +
            "}";

    String storageString = "{\n" +
            "    \"capacity\": 159,\n" +
            "    \"name\": \"Hala F\"\n" +
            "}";

    String storageUpdated = "{\n" +
            "    \"capacity\": 2020,\n" +
            "    \"name\": \"Hall A\"\n" +
            "}";

    String regal = "{\n" +
            "    \"section\": 2,\n" +
            "    \"sector\": 50,\n" +
            "    \"position\": 100\n" +
            "}";

    @Test
    public void test_createStorage() {
        storageCommandService.addNewStorage(storage);

        int createdStorageId = storageCommandRepository.findAll().get(0).getIdStorage();
        Optional<Storage> storageFromSql = storageCommandRepository.findById(createdStorageId);
        Optional<StorageCollection> storageFromNoSql = storageQueryRepository.findById(createdStorageId);
        assertTrue(storageFromSql.isPresent());
        assertTrue(storageFromNoSql.isPresent());
        assertEquals(createdStorageId, storageFromNoSql.get().getId());
        assertEquals(200, storageFromSql.get().getCapacity());
        assertEquals("PrvniSklad", storageFromSql.get().getName());
        assertEquals(storageFromNoSql.get(), new StorageCollection(createdStorageId, storageFromSql.get().getCapacity(), storageFromSql.get().getName()));
    }

    @Test
    public void test_updateStorage() {
        storageCommandService.addNewStorage(storage);
        int createdStorageId = storageCommandRepository.findAll().get(0).getIdStorage();
        storageCommandService.updateStorage(createdStorageId, storageUpdated);

        Optional<Storage> storageFromSql = storageCommandRepository.findById(createdStorageId);
        Optional<StorageCollection> storageFromNoSql = storageQueryRepository.findById(createdStorageId);

        assertTrue(storageFromSql.isPresent());
        assertTrue(storageFromNoSql.isPresent());
        assertEquals(createdStorageId, storageFromSql.get().getIdStorage());
        assertEquals(2020, storageFromSql.get().getCapacity());
        assertEquals("Hall A", storageFromSql.get().getName());
        assertEquals(storageFromNoSql.get(), new StorageCollection(createdStorageId, storageFromSql.get().getCapacity(), storageFromSql.get().getName(), 2));
    }

    @Test
    public void test_getStorageFromNoSql() {
        StorageCollection storageCollection = new StorageCollection(1, 159, "Hala F");

        storageCommandService.addNewStorage(storageString);
        int id = storageCommandRepository.findAll().get(0).getIdStorage();
        StorageCollection storageCollectionFromDB = storageQueryRepository.findStorageCollectionById(id);

        assertEquals(storageCollection, storageCollectionFromDB);

    }

    @Test
    public void delete_storage() {
        storageCommandService.addNewStorage(storage);
        int createdStorageId = storageCommandRepository.findAll().get(0).getIdStorage();
        storageCommandService.deleteStorage(createdStorageId);
        Optional<Storage> storageFromSql = storageCommandRepository.findById(createdStorageId);
        Optional<StorageCollection> storageFromNoSql = storageQueryRepository.findById(createdStorageId);

        assertFalse(storageFromSql.isPresent());
        assertFalse(storageFromNoSql.isPresent());

        storageCommandService.addNewStorage(storage);
        int createdStorageId1 = storageCommandRepository.findAll().get(0).getIdStorage();       
        regalCommandService.addNewRegal(createdStorageId1, regal);
        storageCommandService.deleteStorage(createdStorageId1);
        Optional<Storage> storageFromSql1 = storageCommandRepository.findById(createdStorageId1);
        Optional<StorageCollection> storageFromNoSql1 = storageQueryRepository.findById(createdStorageId1);

        assertTrue(storageFromSql1.isPresent());
        assertTrue(storageFromNoSql1.isPresent());
    }

    @Test
    public void test_getAllStorages() {
        storageCommandService.addNewStorage(storage);
        storageCommandService.addNewStorage(storage2);

        List<Storage> storageList = storageCommandRepository.findAll();
        List<StorageCollection> storageCollectionList = storageQueryRepository.findAll();

        assertEquals(2, storageList.size());
        assertEquals(2, storageCollectionList.size());
    }
}
