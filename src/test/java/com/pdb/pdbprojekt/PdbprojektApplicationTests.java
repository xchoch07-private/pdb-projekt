package com.pdb.pdbprojekt;

import com.pdb.pdbprojekt.tests.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({GoodsServiceTest.class, InvoiceServiceTest.class,
                     ProductServiceTest.class, RegalServiceTest.class,
                     RequisitionServiceTest.class, StorageServiceTest.class,
                     SubscriberServiceTest.class, TruckServiceTest.class})
public class PdbprojektApplicationTests {

    @Test
    public void contextLoads() {

    }
}
