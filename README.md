## PDB Projekt

Semester school project to PDB course implementing API for shipping company.

#### Authors:

- **Tomáš Chocholatý** - xchoch07
- **Ondřej Novák** - xnovak2b

### Prerequisites:

- Install `Java JDK 11`,  `Maven` and some SQL database (implemented and tested on Oracle SQL database) and MongoDB database.

- In the root of the project repository run `$ mvn clean install -DskipTests`, which will build the app.
- The last thing is to fill database configuration. In the file `src/main/resources/application.properties` fill the following keys:
  - `spring.datasource.url` - URL to database, e.g.  **jdbc:oracle:thin:@//gort.fit.vutbr.cz:1521/orclpdb**
  - `spring.datasource.username` - username of database user
  - `spring.datasource.password` - password of database user
  - `spring.data.mongodb.uri=mongodb` - Uri for MongoDB
  - `spring.data.mongodb.database` - name for NoSql databased


### Running the server

- Application can be started running `$ mvn spring-boot:run`